/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <math.h>
#include "constants.h"
#include "errorhandler.h"
#include "point.h"

Point::Point()
    : Point(0.0, 0.0, 0.0)
{
}

Point::Point(Point* p)
{
    m_x = p->x();
    m_y = p->y();
    m_z = p->z();
    m_invalid = p->m_invalid;
    m_truncated = p->m_truncated;
    m_roughness = p->m_roughness;
    m_id = p->getId();
}

Point::Point(const double x, const double y, const double z)
{
    m_x = x;
    m_y = y;
    m_z = z;
    m_invalid = false;
    m_truncated = false;
    m_roughness = DEFAULT_ROUGHNESS;
    m_id = CPID::NONE;
}

Point::~Point()
{
}

QString Point::idToString(const CPID id)
{
    switch (id) {
    case MAAIVELD_BUITENWAARTS:
        return "Maaiveld binnenwaarts";
    case TEEN_GEUL:
        return "Teen geul";
    case INSTEEK_GEUL:
        return "Insteek geul";
    case TEEN_DIJK_BUITENWAARTS:
        return "Teen Dijk Buitenwaarts";
    case KRUIN_BUITENBERM:
        return "Kruin buitenberm";
    case INSTEEK_BUITENBERM:
        return "Insteek buitenberm";
    case KRUIN_BUITENTALUD:
        return "Kruin buitentalud";
    case RAND_VERKEERSBELASTING_BUITENWAARTS:
        return "Rand verkeersbelasting buitenwaarts";
    case RAND_VERKEERSBELASTING_BINNENWAARTS:
        return "Rand verkeersbelasting binnenwaarts";
    case KRUIN_BINNENTALUD:
        return "Kruin binnentalud";
    case INSTEEK_BINNENBERM:
        return "Insteek binnenberm";
    case KRUIN_BINNENBERM:
        return "Kruin binnenberm";
    case TEEN_DIJK_BINNENWAARTS:
        return "Teen dijk binnenwaarts";
    case INSTEEK_SLOOT_DIJKZIJDE:
        return "Insteek sloot dijkzijde";
    case SLOOTBODEM_DIJKZIJDE:
         return "Slootbodem dijkzijde";
    case SLOOTBODEM_POLDERZIJDE:
         return "Slootbodem polderzijde";
    case INSTEEK_SLOOT_POLDERZIJDE:
        return "Insteek sloot polderzijde";
    case MAAIVELD_BINNENWAARTS:
        return "Maaiveld binnenwaarts";
    case IMAGINAIR_KRUINPUNT:
        return "Imaginair kruinpunt";
    case IMAGINAIR_TEENPUNT:
        return "Imaginair teenpunt";
    default:
        return "";
    }
}

Point::CPID Point::stringToId(QString s)
{
    if (s == "Maaiveld binnenwaarts") return MAAIVELD_BUITENWAARTS;
    if (s == "Teen geul") return TEEN_GEUL;
    if (s == "Insteek geul") return INSTEEK_GEUL;
    if (s == "Teen Dijk Buitenwaarts") return TEEN_DIJK_BUITENWAARTS;
    if (s == "Kruin buitenberm") return KRUIN_BUITENBERM;
    if (s == "Insteek buitenberm") return INSTEEK_BUITENBERM;
    if (s == "Kruin buitentalud") return KRUIN_BUITENTALUD;
    if (s == "Rand verkeersbelasting buitenwaarts") return RAND_VERKEERSBELASTING_BUITENWAARTS;
    if (s == "Rand verkeersbelasting binnenwaarts") return RAND_VERKEERSBELASTING_BINNENWAARTS;
    if (s == "Kruin binnentalud") return KRUIN_BINNENTALUD;
    if (s == "Insteek binnenberm") return INSTEEK_BINNENBERM;
    if (s == "Kruin binnenberm") return KRUIN_BINNENBERM;
    if (s == "Teen dijk binnenwaarts") return TEEN_DIJK_BINNENWAARTS;
    if (s == "Insteek sloot dijkzijde") return INSTEEK_SLOOT_DIJKZIJDE;
    if (s == "Slootbodem dijkzijde") return SLOOTBODEM_DIJKZIJDE;
    if (s == "Slootbodem polderzijde") return SLOOTBODEM_POLDERZIJDE;
    if (s == "Insteek sloot polderzijde") return INSTEEK_SLOOT_POLDERZIJDE;
    if (s == "Maaiveld binnenwaarts") return MAAIVELD_BINNENWAARTS;
    if (s == "Imaginair kruinpunt") return IMAGINAIR_KRUINPUNT;
    return NONE;
}

double Point::l(const Point *base) const
{
    // Return the horizontal distance to a point. Ignore z.
    double dx = m_x - base->x();
    double dy = m_y - base->y();
    return sqrt(dx*dx + dy*dy);
}

long Point::lmm(const Point *base) const
{
    return toLong(l(base));
}

bool Point::isSameLocation(const Point *p) const
{
    double d = l(p);
    if (d < -0.001) return false;
    if (d >  0.001) return false;
    return true;
}

bool Point::truncated() const
{
    return m_truncated;
}

void Point::setTruncated(bool truncated)
{
    m_truncated = truncated;
}

bool Point::invalid() const
{
    return m_invalid;
}

void Point::setInvalid(bool invalid)
{
    m_invalid = invalid;
}

bool Point::read(const QJsonObject &json)
{
    if (json.contains("x") && json.contains("y") && json.contains("z") && json.contains("r") &&
            json.contains("truncated") && json.contains("invalid") && json.contains("id")) {
        m_x = json["x"].toDouble();
        m_y = json["y"].toDouble();
        m_z = json["z"].toDouble();
        m_roughness = json["r"].toDouble();
        m_truncated = json["truncated"].toBool();
        m_invalid = json["invalid"].toBool();
        m_id = static_cast<CPID>(json["id"].toInt());
        return true;
    } else {
        ErrorHandler::info("Bestand bevat één of meer ongeldige punten.");
        return false;
    }
}

bool Point::write(QJsonObject &json) const
{
    json["x"] = m_x;
    json["y"] = m_y;
    json["z"] = m_z;
    json["r"] = m_roughness;
    json["truncated"] = m_truncated;
    json["invalid"] = m_invalid;
    json["id"] = static_cast<int>(m_id);
    return true;
}
