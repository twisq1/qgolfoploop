/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef RDPPARSER_H
#define RDPPARSER_H

#include <QString>
#include "processor.h"
#include "point.h"

#define GLOBAL_EPSILON_KEY "rdp_epsilon"

class RDPparser : public Processor
{
    static constexpr double DEFAULT_EPSILON = 0.1;

public:
    RDPparser(Crosssection* crs);

    // Processor interface
    virtual QString getName() const override;

    // These two functions can be used to read and write the global value to persistent store.
    static void setGlobalEpsilon(double epsilon);
    static double getGlobalEpsilon();

private:    
    virtual void doProcess() override;
    //void step(const QVector<Point*> &pointList, double epsilon, QVector<Point*> &out);
    void apply(const QList<Point*> &pointList, double epsilon);
};

#endif // RDPPARSER_H
