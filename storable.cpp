/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QFile>
#include <QJsonDocument>
#include "errorhandler.h"
#include "qgoexception.h"
#include "storable.h"

Storable::Storable()
{
    m_filename.clear();
}

bool Storable::load(const QString filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        QGOException ex(QGOException::INFO, "Leesfout", "Het bestand " + filename + " kan niet gelezen worden.");
        ErrorHandler::exception(ex);
        return false;
    }

    QJsonDocument json = QJsonDocument().fromJson(file.readAll());
    return read(json.object());
}

bool Storable::save(const QString filename) const
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        QGOException ex(QGOException::INFO, "Schrijffout", "Het bestand " + filename + " kan niet worden geschreven.");
        ErrorHandler::exception(ex);
        return false;
    }

    QJsonObject json;
    if (write(json)) {

        QJsonDocument doc(json);
        return (file.write(doc.toJson(QJsonDocument::Compact)) > 0);
    }
    return false;
}

bool Storable::save() const
{
    return save(m_filename);
}
