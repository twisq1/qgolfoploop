/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "constants.h"
#include "point.h"
#include "crosssection.h"
#include "truncator.h"

Truncator::Truncator(Crosssection* crs)
    : Processor(crs)
{
}

QString Truncator::getName() const
{
    return PROCESSOR_TRUNC;
}

void Truncator::doProcess()
{
    /*
     * Remove all points before teendijk.l and after binnenkruin.l
     *
     * UPDATE; save the leftmost point, this is `voorland' until the TEEN_DIJK_BUITENWAARTS
     */

    Point* teendijk = crs()->getCharacteristicPoint(Point::CPID::IMAGINAIR_TEENPUNT);
    if (!teendijk) teendijk = crs()->getCharacteristicPoint(Point::CPID::TEEN_DIJK_BUITENWAARTS);
    if (!teendijk) {
        error("Teen dijk buitenwaarts niet bekend.");
        return;
    }

    Point* binnenkruin = crs()->getCharacteristicPoint(Point::CPID::KRUIN_BINNENTALUD);
    if (!binnenkruin) {
        error("Kruin binnentalud niet bekend.");
        return;
    }

    Point* base = crs()->getReferencePoint();
    if (!base) {
        error("Referentiepunt niet bekend.");
        return;
    }

    double l_min = teendijk->l(base) - 0.0005;       // Subtract a half mm to avoid rounding errors.
    double l_max = 0.;
    Point* imaginair_kruinpunt = crs()->getCharacteristicPoint(Point::CPID::IMAGINAIR_KRUINPUNT);
    if (imaginair_kruinpunt && (imaginair_kruinpunt->l(base) > binnenkruin->l(base))) {
        l_max = imaginair_kruinpunt->l(base) + 0.0005; // Add a half mm to avoid rounding errors.
    } else {
        l_max = binnenkruin->l(base) + 0.0005;  // Add a half mm to avoid rounding errors.
    }

    foreach (Point* p, crs()->getSurfacePoints()) {
        if(p != crs()->getSurfacePoints().at(0)){ //skip the leftmost point, this area is voorland
            if ((p->l(base) < l_min) || (p->l(base) > l_max)) {
                p->setTruncated();
            }
        }
    }


    // remove all points after the imaginary point if this is available
    Point *cp = crs()->getCharacteristicPoint(Point::IMAGINAIR_KRUINPUNT);
    if(cp!=nullptr) {
        l_max = cp->l(base) + 0.0005;   // Add a half mm again to avoid rounding errors.
        foreach(Point *p, crs()->getSurfacePoints()){
            if(!p->truncated()){
                p->setTruncated(p->l(base) > l_max);
            }
        }
    }
    crs()->setTruncated(true);
}
