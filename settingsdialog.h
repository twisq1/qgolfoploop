/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "crosssection.h"
#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = nullptr, QMap<QString,int> *pm = nullptr);
    ~SettingsDialog();

private slots:
    void on_buttonBox_accepted();
    void on_pbMoveUp_clicked();
    void on_pbMoveDown_clicked();
    void on_pbReset_clicked();

private:
    Ui::SettingsDialog *ui;
    Crosssection* crs;  // We need a cross section because it contains a list of processors.
    QMap<QString, int> *m_processor_order;

    void updateProcessorListbox();
};

#endif // SETTINGSDIALOG_H
