/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QPushButton>
#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>
#include <QProgressDialog>
#include <QSettings>
#include <QXmlStreamWriter>
#include <QModelIndex>
#include "errorhandler.h"
#include "ui_mainwindow.h"
#include "constants.h"
#include "simpleQtLogger.h"
#include "settingsdialog.h"
#include "exportdialog.h"
#include "shapefile.h"
#include "mainwindow.h"

void MainWindow::loadInitialFolder()
{
    if (QCoreApplication::arguments().count() > 1) {
        m_project.readFromPath(QCoreApplication::arguments().at(1));
        m_project.setCurrent_crosssection_index(0);
        initProfileList();
        showActiveCrossSection();
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // Load settings of the processor order
    loadProcessorOrderSettings();

    //setup UI
    ui->setupUi(this);

    ui->sliderXScale->setMaximum(MAX_ZOOMFACTOR_X);
    ui->sliderYScale->setMaximum(MAX_ZOOMFACTOR_Y);
    ui->sliderXScale->setValue(DEFAULT_ZOOMFACTOR_X);
    ui->sliderYScale->setValue(DEFAULT_ZOOMFACTOR_Y);
    ui->sliderXScale->setMinimum(MIN_ZOOMFACTOR_X);
    ui->sliderYScale->setMinimum(MIN_ZOOMFACTOR_Y);

    //add status bar information
    m_lbl_mouse_location = new QLabel(this);
    m_lbl_mouse_location->setAlignment(Qt::AlignRight);
    m_lbl_cross_section = new QLabel(this);
    ui->statusBar->addWidget(m_lbl_cross_section);
    ui->statusBar->addPermanentWidget(m_lbl_mouse_location);

    //connect the sliders to the projectview and changes from the projectview to the sliders
    connect(ui->sliderXScale, SIGNAL(valueChanged(int)), ui->wProjectView, SLOT(setZoomX(int)));
    connect(ui->sliderYScale, SIGNAL(valueChanged(int)), ui->wProjectView, SLOT(setZoomY(int)));
    connect(ui->wProjectView, SIGNAL(zoomFactorXChanged(int)), ui->sliderXScale, SLOT(setValue(int)));
    connect(ui->wProjectView, SIGNAL(zoomFactorYChanged(int)), ui->sliderYScale, SLOT(setValue(int)));

    //connect the mouse position from the projectview to the status bar information
    connect(ui->wProjectView, SIGNAL(mousePositionChanged(QPointF)), this, SLOT(on_mouseLocationChanged(QPointF)));
    connect(ui->wProjectView, SIGNAL(updateBottom()), this, SLOT(updateRules()));    
    connect(ui->wProjectView, SIGNAL(crosssectionChanged()), this, SLOT(updateRules()));
    connect(ui->wProjectView, SIGNAL(scaleFactorChanged(QPointF)), this, SLOT(on_projectviewScaleChanged(QPointF)));

    connect(ui->tableCrosssection, SIGNAL(clicked(QModelIndex)), ui->wProjectView, SLOT(setActiveLine(QModelIndex)));
    connect(ui->actionBoxZoom, SIGNAL(triggered(bool)), ui->wProjectView, SLOT(setBoxZoom(bool)));

    ui->actionShowPoints->setChecked(false);
    ui->actionShowGrid->setChecked(true);
    ui->actionShowLines->setChecked(true);
    ui->actionactionShowCrosssectionInfo->setChecked(true);
    ui->tableCrosssection->setItemDelegate(&m_dspbox_delegate);
    ui->actionBoxZoom->setChecked(false);

    ui->wProjectView->setProject(&m_project);

    // Give folder to load as command line parameter.
    // in Qt: Projects -> Build & Run -> Run -> Command line parameters.
    loadInitialFolder();



    updating_rules = false;
}

MainWindow::~MainWindow()
{
    writeProcessorOrderSettings();
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
}

void MainWindow::gotoNextCrosssection()
{
    if (m_project.getCurrent_crosssection_index() < (m_project.numCrosssections() - 1)) {
        m_project.setCurrent_crosssection_index(m_project.getCurrent_crosssection_index() + 1);
        showActiveCrossSection();
    }
}

void MainWindow::updateRules()
{
    updating_rules = true;
    ui->processorsListWidget->clear();
    ui->teWarning->clear();
    Crosssection* crs = m_project.getCurrentCrosssection();
    bool crs_ok = true;
    if (crs) {
        QListWidgetItem *item = new QListWidgetItem("Alle processoren", ui->processorsListWidget);
        item->setData(Qt::UserRole, 0LL);
        item->setFlags((item->flags() | Qt::ItemIsUserTristate) & (~Qt::ItemIsSelectable));
        item->setCheckState(crs->getAllProcessorState());
        ui->processorsListWidget->addItem(item);
        for (QString name : m_project.getOrderedProcessorNames())
        {
            Processor* p =  nullptr;
            for (Processor* pit : crs->getProcessors()) {
                if (pit->getName() == name) p = pit;
            }

            if (p) {
                QListWidgetItem *item = new QListWidgetItem(p->getName(), ui->processorsListWidget);
                item->setData(Qt::UserRole, reinterpret_cast<qlonglong>(p));
                item->setFlags((item->flags() | Qt::ItemIsUserTristate) & (~Qt::ItemIsSelectable));
                switch (p->getLocalEnable()) {
                case Processor::Tristate::ON:
                    item->setCheckState(Qt::Checked);
                    break;
                case Processor::Tristate::OFF:
                    item->setCheckState(Qt::Unchecked);
                    break;
                case Processor::Tristate::GLOBAL:
                    item->setCheckState(Qt::PartiallyChecked);
                    break;
                }
                if (p->getHasrun()) {
                    if (p->getOk()) {
                        item->setForeground(Qt::darkGreen);
                    } else {
                        item->setForeground(Qt::red);
                        ui->teWarning->append(p->getStatus());
                        crs_ok = false;
                    }
                } else {
                    if (!p->isEnabled()) item->setForeground(Qt::gray);
                }
                ui->processorsListWidget->addItem(item);
            }
        }

        if (ui->processorsListWidget->currentItem()) {
            if(crs_ok){
                ui->profileListWidget->currentItem()->setForeground(Qt::black);
            }else{
                ui->profileListWidget->currentItem()->setForeground(Qt::red);
            }
        }
    }
    updating_rules = false;
}

void MainWindow::showActiveCrossSection()
{
    Crosssection *crs = m_project.getCurrentCrosssection();

    if(crs==nullptr) return;
    ui->wProjectView->setCurrentCrosssectionIndex(m_project.getCurrent_crosssection_index());

    // Update profile list
    updateProfileList();

    //update table
    ui->tableCrosssection->setModel(crs);


    //connect crosssection changes to the qprojectview updates
    if(crs!=nullptr){
        connect(crs, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), ui->wProjectView, SLOT(update()));
        connect(crs, SIGNAL(validationStateChanged(QString)), this, SLOT(updateRules()));        
    }

    //connect qprojectview crosssections changes to table update
    connect(ui->wProjectView, SIGNAL(crosssectionChanged()), this, SLOT(updateTable()));

    //update rules
    updateRules();

    //update UI
    QString s = QString("Profiel: %1 / %2").arg(m_project.getCurrent_crosssection_index() + 1).arg(m_project.numCrosssections());
    m_lbl_cross_section->setText(s);

    if(crs!=nullptr){
        ui->teRemark->setText(crs->getRemark());
    }
}

void MainWindow::updateProfileList()
{
    int selectedIndex = m_project.getCurrent_crosssection_index();
    if (ui->profileListWidget->count() > selectedIndex) {
        ui->profileListWidget->item(selectedIndex)->setSelected(true);
        ui->profileListWidget->setCurrentRow(selectedIndex);
    }
}


void MainWindow::initProfileList()
{
    ui->profileListWidget->clear();
    int n = m_project.numCrosssections();
    Crosssection* p;
    for (int i = 0; i < n; i++) {
        p = m_project.getCrosssection(i);
        if (p){
            ui->profileListWidget->addItem(p->getName());
            if(p->isInvalid()){
                ui->profileListWidget->item(i)->setForeground(Qt::red);
            } else {
                ui->profileListWidget->item(i)->setForeground(Qt::black);
            }

        }
    }
    //updateProfileList();
}

void MainWindow::loadProcessorOrderSettings()
{
    QSettings settings;

    QMap<int, QString> inverseMap;
    inverseMap.insertMulti(settings.value("processors/ikp", 99).toInt(), PROCESSOR_IKP);
    inverseMap.insertMulti(settings.value("processors/itp", 99).toInt(), PROCESSOR_ITP);
    inverseMap.insertMulti(settings.value("processors/trunc", 99).toInt(), PROCESSOR_TRUNC);
    inverseMap.insertMulti(settings.value("processors/rdp", 99).toInt(), PROCESSOR_RDP);
    inverseMap.insertMulti(settings.value("processors/bekleding", 99).toInt(), PROCESSOR_BEKLEDING);
    inverseMap.insertMulti(settings.value("processors/breuksteen", 99).toInt(), PROCESSOR_BREUKSTEEN);
    inverseMap.insertMulti(settings.value("processors/zetting", 99).toInt(), PROCESSOR_ZETTING);
    inverseMap.insertMulti(settings.value("processors/cor_neg", 99).toInt(), PROCESSOR_COR_NEG);
    inverseMap.insertMulti(settings.value("processors/cor_gentleslope", 99).toInt(), PROCESSOR_COR_GENTLESLOPE);
    inverseMap.insertMulti(settings.value("processors/cor_steep", 99).toInt(), PROCESSOR_COR_STEEP);
    inverseMap.insertMulti(settings.value("processors/validator", 99).toInt(), PROCESSOR_VALIDATOR);
    inverseMap.insertMulti(settings.value("processors/riskeer", 99).toInt(), PROCESSOR_RISKEER);
    int i=0;
    for (int key : inverseMap.keys()) {
        for (QString value : inverseMap.values(key)) {
            m_project.addProcessorMapping(value, i++);
        }
    }
}

void MainWindow::writeProcessorOrderSettings()
{
    QSettings settings;
    settings.setValue("processors/ikp", m_project.getProcessorOrder(PROCESSOR_IKP));
    settings.setValue("processors/itp", m_project.getProcessorOrder(PROCESSOR_ITP));
    settings.setValue("processors/trunc", m_project.getProcessorOrder(PROCESSOR_TRUNC));
    settings.setValue("processors/rdp", m_project.getProcessorOrder(PROCESSOR_RDP));
    settings.setValue("processors/bekleding", m_project.getProcessorOrder(PROCESSOR_BEKLEDING));
    settings.setValue("processors/breuksteen", m_project.getProcessorOrder(PROCESSOR_BREUKSTEEN));
    settings.setValue("processors/zetting", m_project.getProcessorOrder(PROCESSOR_ZETTING));
    settings.setValue("processors/cor_neg", m_project.getProcessorOrder(PROCESSOR_COR_NEG));
    settings.setValue("processors/cor_gentleslope", m_project.getProcessorOrder(PROCESSOR_COR_GENTLESLOPE));
    settings.setValue("processors/cor_steep", m_project.getProcessorOrder(PROCESSOR_COR_STEEP));
    settings.setValue("processors/validator", m_project.getProcessorOrder(PROCESSOR_VALIDATOR));
    settings.setValue("processors/riskeer", m_project.getProcessorOrder(PROCESSOR_RISKEER));
}

void MainWindow::on_actionInfo_triggered()
{
    QString version = QCoreApplication::applicationVersion();
    QString html = "<html><body><h1>qGolfoploop</h1><p>Deze software kan worden gebruikt voor de invoer van de golfoploop toetsing.</p>";
    html += "<p><table>";
    html += "<tr><td>Versie:</td><td>" + version + "</td></tr>";
    html += "<tr><td>Copyright:</td><td>TWISQ</td></tr>";
    html += "<tr><td>Opdrachtgevers:</td><td>Hoogheemraadschap Hollands Noorderkwartier en Wetterskip Fryslân</td></tr>";
    html += "<tr><td>Iconen:</td><td><a href='http://www.famfamfam.com/lab/icons/silk/'>Fam Fam Fam</a></td></tr>";
    html += "<tr><td>Help:</td><td><a href='https://www.twisq.nl/qgolfoploop/handleiding_v1_5.pdf'>https://www.twisq.nl/qgolfoploop/handleiding_v1_5.pdf</a></td></tr>";
    html += "</table></p>";
    html += "<p>Deze software is open source en uitgebracht onder de GNU GPLv3 licentie. Dat betekent dat je vrij bent om de software te gebruiken, copieren, aanpassen en distribueren, mits je de oorspronkelijke auteur vermeldt en geen aanvullende restricties oplegt. Een volledige beschrijving van GNU GPLv3 kan je <a href='https://www.gnu.org/licenses/gpl.html'>hier</a> lezen.</p>";
    html += "<p>qGolfoploop is geschreven in Qt versie 5.12.</p>";
    html += "</body></html>";
    QMessageBox::about(this, "qGolfoploop", html);
}


void MainWindow::on_actionOpen_triggered()
{
    if (m_project.isDirty()) {
        if (QMessageBox::question(this, "Bevestiging", "Het project bevat nog niet opgeslagen wijzigingen. Weet je zeker dat je een nieuw bestand wilt openen?", QMessageBox::Yes|QMessageBox::No) ==
                QMessageBox::StandardButton::No) return;
    }

    QSettings settings;
    QString basedir = settings.value("save_dir").toString();
    QString filename = QFileDialog::getOpenFileName(this, tr("Kies bestand"),basedir, "*.json");
    if(filename != "") {
        QFileInfo fileinfo(filename);
        settings.setValue("save_dir", fileinfo.absolutePath());
        if (m_project.load(filename)) {
            m_project.setCurrent_crosssection_index(0);
            initProfileList();
            showActiveCrossSection();
        }
    }
}


void MainWindow::on_actionImport_triggered()
{
    if (m_project.isDirty()) {
        if (QMessageBox::question(this, "Bevestiging", "Het project bevat nog niet opgeslagen wijzigingen. Weet je zeker dat je een nieuw bestand wilt openen?", QMessageBox::Yes|QMessageBox::No) ==
                QMessageBox::StandardButton::No) return;
    }

    QSettings settings;
    QString basedir = settings.value("base_dir").toString();
    QString dir = QFileDialog::getExistingDirectory(this, tr("Kies folder met bestanden"),basedir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(dir != "") {
        QDir d(dir);
        d.cdUp();
        settings.setValue("base_dir", d.absolutePath());
        m_project.readFromPath(dir);
        m_project.setCurrent_crosssection_index(0);
        initProfileList();
        showActiveCrossSection();
    }
}


void MainWindow::on_actionSave_triggered()
{
    QSettings settings;
    QString basedir = settings.value("save_dir").toString();
    QString filename = QFileDialog::getSaveFileName(this, tr("Kies bestand"),basedir, "*.json");
    if(filename != "") {
        QFileInfo fileinfo(filename);
        settings.setValue("save_dir", fileinfo.absolutePath());
        if (m_project.save(filename)) {
            QMessageBox::information(this, "Ok", "Het bestand is opgeslagen.");
        }
    }
}

void MainWindow::on_actionExport_triggered()
{
    ExportDialog dlg;
    dlg.exec();
    switch (dlg.getExportWhat()) {
    case ExportDialog::ExportWhat::SINGLE_PROFILE:
        exportSingleProfile();
        break;
    case ExportDialog::ExportWhat::ALL_PROFILES:
        exportAllProfiles();
        break;
    case ExportDialog::ExportWhat::SINGLE_IMAGE:
        exportSingleImage();
        break;
    case ExportDialog::ExportWhat::ALL_IMAGES:
        exportAllImages();
        break;
    case ExportDialog::ExportWhat::SHAPEFILE:
        exportShapefile();
        break;
    case ExportDialog::ExportWhat::XML_FILE:
        exportXmlFile();
        break;
    default:
        //L_DEBUG("Nothing to export.");
        break;
    }
}

void MainWindow::exportSingleProfile()
{
    if (m_project.getCurrentCrosssection()) {
        QSettings settings;
        QString startdir = settings.value("prfl_dir", "").toString();
        QString filename  = QString("%1.prfl").arg(m_project.getCurrentCrosssection()->getName());
        QFileInfo preferredfile(QDir(startdir), filename);
        filename = QFileDialog::getSaveFileName(this, "Kies bestandsnaam voor profiel.", preferredfile.absoluteFilePath(), "*.prfl");
        if (!filename.isEmpty()) {
            QFileInfo fileinfo(filename);
            settings.setValue("prfl_dir", fileinfo.absolutePath());

            m_project.exportSingleProfile(filename);
            QMessageBox::information(this, "Profiel opgeslagen", "Het profiel is opgeslagen");
        }
    }
}

void MainWindow::exportAllProfiles()
{
    QSettings settings;
    QString prfldir = settings.value("prfl_dir").toString();
    QString dir = QFileDialog::getExistingDirectory(this, tr("Kies folder waarin PRFL bestanden kunnen worden opgeslagen"),prfldir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(dir != "") {
        QDir d(dir);
        settings.setValue("prfl_dir", d.absolutePath());
        m_project.exportAllProfiles(dir);
    }
}

void MainWindow::exportSingleImage()
{
    Crosssection* crs = m_project.getCurrentCrosssection();

    if(crs){
        QSettings settings;
        QString startdir = settings.value("images_directory", "").toString();
        QString filename = QString("%1.png").arg(crs->getName());
        QFileInfo preferredfile(QDir(startdir), filename);
        filename = QFileDialog::getSaveFileName(this, "Kies bestandsnaam voor afbeelding.", preferredfile.absoluteFilePath(), "*.png");
        if (!filename.isEmpty()) {
            QFileInfo fileinfo(filename);
            settings.setValue("images_directory", fileinfo.absolutePath());

            //maak de cursor even onzichtbaar en doe de export
            //ui->wProjectView->setCrosslinesVisible(false)
            ui->wProjectView->setCursorVisible(false);
            ui->wProjectView->grab().save(filename);
            //en weer zichtbaar
            ui->wProjectView->setCursorVisible(true);
            QMessageBox::information( this, "Image save", QString("De afbeelding is opgeslagen als %1").arg(filename), QMessageBox::Ok, 0 );
        }
    }
}

void MainWindow::exportAllImages()
{
    //is er uberhaupt iets te doen?
    if(m_project.numCrosssections()==0) return;

    //bewaar de huidige selectie
    int ccrsid = m_project.getCurrent_crosssection_index();
    //selecteer het uitvoerpad
    QSettings settings;
    QString startdir = settings.value("images_directory", "").toString();

    QString exportdir = QFileDialog::getExistingDirectory(this, tr("Selecteer uitvoerpad"),
                                                 startdir,
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    //cancel opvangen
    if(exportdir.length()==0) return;

    // Store path
    settings.setValue("images_directory", exportdir);

    QCoreApplication::processEvents(); //soms werkt dit om te voorkomen dat de dialoog niet verschijnt

    //maak een nieuwe projectview en gebruik dezelfde functionaliteit van een enkele screenshot
    QProjectView *view = new QProjectView();
    view->resize(800,600);
    view->setShowGrid(false);
    view->setShowHelperLines(true);
    view->setShowPoints(true);
    view->setProject(&m_project);
    view->setCursorVisible(false);

    QProgressDialog dlg("Bezig met het exporteren van de dwarsprofiel afbeeldingen", "Onderbreken", 0, m_project.numCrosssections(), this);
    dlg.setWindowModality(Qt::WindowModal);

    for (int i=0; i<m_project.numCrosssections()-1; i++) {
        dlg.setValue(i);
        if(dlg.wasCanceled()) break;
        Crosssection *crs = m_project.getCrosssection(i);
        QString filename = QString("%1.png").arg(crs->getName());
        QFileInfo fileinfo(QDir(exportdir), filename);
        view->setCurrentCrosssectionIndex(i);
        view->grab().save(fileinfo.absoluteFilePath());
    }
    dlg.setValue(m_project.numCrosssections());
    delete view;

    //reset het actuele dwarsprofiel
    m_project.setCurrent_crosssection_index(ccrsid);

    QMessageBox::information(this, "Afbeeldingen export", QString("De afbeeldingen zijn opgeslagen in %1").arg(exportdir), QMessageBox::Ok, 0 );
}

void MainWindow::exportShapefile()
{
    QSettings settings;
    QString startdir = settings.value("shape_directory", "").toString();
    QString filename = QString("qgolfoploop.shp");
    QFileInfo preferredfile(QDir(startdir), filename);
    filename = QFileDialog::getSaveFileName(this, "Kies bestandsnaam voor shapefile.", preferredfile.absoluteFilePath(), "*.shp");
    if (!filename.isEmpty()) {
        QFileInfo fileinfo(filename);
        settings.setValue("shape_directory", fileinfo.absolutePath());

        Shapefile shape;
        shape.open(fileinfo.absoluteFilePath());
        for (int i = 0; i < m_project.numCrosssections(); i++) {
            Crosssection* crs = m_project.getCrosssection(i);
            shape.addPoint(crs->getReferencePoint(), crs->getName());
        }
        if (shape.close()) {
            QMessageBox::information( this, "Export", QString("De shapefile is opgeslagen als %1").arg(filename), QMessageBox::Ok, 0 );
        }
    }
}

void MainWindow::exportXmlFile()
{
    QSettings settings;
    QString startdir = settings.value("xml_directory", "").toString();
    QString filename = QString("qgolfoploop.xml");
    QFileInfo preferredfile(QDir(startdir), filename);
    filename = QFileDialog::getSaveFileName(this, "Kies bestandsnaam voor XML file.", preferredfile.absoluteFilePath(), "*.xml");
    if (!filename.isEmpty()) {
        QFileInfo fileinfo(filename);
        settings.setValue("xml_directory", fileinfo.absolutePath());

        QFile file(fileinfo.absoluteFilePath());
        if (file.open(QIODevice::WriteOnly)) {
            QXmlStreamWriter stream(&file);
            stream.setAutoFormatting(true);
            stream.writeStartDocument();
            stream.writeStartElement("golfoploopProfielen");
            stream.writeAttribute("xmlns", "http://WBI/golfoploopProfielen/1");

            for (int i = 0; i < m_project.numCrosssections(); i++) {
                Crosssection* crs = m_project.getCrosssection(i);

                stream.writeStartElement("golfoploopProfiel");
                stream.writeAttribute("versie", "4.0");
                stream.writeStartElement("id");
                stream.writeCharacters(crs->getName());
                stream.writeEndElement();

                stream.writeStartElement("richting");
                stream.writeCharacters(QString::number(crs->getDirection(), 'f', 2));
                stream.writeEndElement();

                stream.writeStartElement("dam");
                stream.writeStartElement("damType");
                stream.writeCharacters(QString::number(crs->getDam()->type()));
                stream.writeEndElement();
                stream.writeStartElement("damhoogte");
                stream.writeCharacters(QString::number(crs->getDam()->height(), 'f', 3));
                stream.writeEndElement();
                stream.writeEndElement();

                stream.writeStartElement("illustratiePunt");
                stream.writeStartElement("x_illustratiePunt");
                stream.writeCharacters(QString::number(crs->getReferencePoint()->x(), 'f', 3));
                stream.writeEndElement();
                stream.writeStartElement("y_illustratiePunt");
                stream.writeCharacters(QString::number(crs->getReferencePoint()->y(), 'f', 3));
                stream.writeEndElement();
                stream.writeEndElement();

                // Bepaal waar de overgang is tussen voorland en dijk.
                long teenpos = 0;
                Point* teen = crs->getCharacteristicPoint(Point::CPID::TEEN_DIJK_BUITENWAARTS);
                if (teen) {
                    teenpos = teen->lmm(crs->getReferencePoint());
                } else {
                    L_WARN("Profiel bevat geen teen");
                }

                QList<Point*> voorland;
                QList<Point*> dijk;
                for (auto p : crs->getSurfacePoints()) {
                    if (p->lmm(crs->getReferencePoint()) <= teenpos) {
                        voorland.append(p);
                    }
                    if (p->lmm(crs->getReferencePoint()) >= teenpos) {
                        dijk.append(p);
                    }
                }

                stream.writeStartElement("voorland");
                stream.writeStartElement("aantalVoorlandPunten");
                stream.writeCharacters(QString::number(voorland.length()));
                stream.writeEndElement();
                stream.writeStartElement("voorlandPunten");
                for (auto p : voorland) {
                    stream.writeStartElement("voorlandPunt");
                    stream.writeStartElement("x_voorlandPunt");
                    stream.writeCharacters(QString::number(p->x(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeStartElement("y_voorlandPunt");
                    stream.writeCharacters(QString::number(p->y(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeStartElement("z_voorlandPunt");
                    stream.writeCharacters(QString::number(p->z(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeStartElement("ruwheid_voorlandPunt");
                    stream.writeCharacters(QString::number(p->roughness(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeEndElement();
                }
                stream.writeEndElement();
                stream.writeEndElement();

                stream.writeStartElement("damwandType");
                stream.writeCharacters(QString::number(crs->getDamwand()->type()));
                stream.writeEndElement();

                double kruinhoogte = 0.0;
                Point* kruin = crs->getCharacteristicPoint(Point::CPID::KRUIN_BINNENTALUD);
                if (kruin) {
                    kruinhoogte = kruin->z();
                } else {
                    kruinhoogte = crs->getZMax();
                }

                stream.writeStartElement("profiel");
                stream.writeStartElement("kruinhoogte");
                stream.writeCharacters(QString::number(kruinhoogte, 'f', 3));
                stream.writeEndElement();
                stream.writeStartElement("aantalDijkPunten");
                stream.writeCharacters(QString::number(dijk.length()));
                stream.writeEndElement();
                stream.writeStartElement("dijkPunten");
                for (auto p : dijk) {
                    stream.writeStartElement("dijkPunt");
                    stream.writeStartElement("x_dijkPunt");
                    stream.writeCharacters(QString::number(p->x(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeStartElement("y_dijkPunt");
                    stream.writeCharacters(QString::number(p->y(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeStartElement("z_dijkPunt");
                    stream.writeCharacters(QString::number(p->z(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeStartElement("ruwheid_dijkPunt");
                    stream.writeCharacters(QString::number(p->roughness(), 'f', 3));
                    stream.writeEndElement();
                    stream.writeEndElement();
                }
                stream.writeEndElement();
                stream.writeEndElement();

                stream.writeStartElement("memo");
                stream.writeCharacters(crs->getRemark());
                stream.writeEndElement();

                stream.writeStartElement("sturingsparametersGolfoploopProfiel");
                if (crs->hasRDPepsilon()) {
                    stream.writeStartElement("peuckerEpsilon");
                    stream.writeCharacters(QString::number(crs->getRDPepsilon(), 'f', 3));
                    stream.writeEndElement();
                }
                if (crs->hasIKPnumskip()) {
                    stream.writeStartElement("segmentenOverslaanInLineaireregressieLijn");
                    stream.writeCharacters(QString::number(crs->getIKPnumskip()));
                    stream.writeEndElement();
                }
                if (crs->hasIKPnumregression()) {
                    stream.writeStartElement("segmentenInLineaireregressieLijn");
                    stream.writeCharacters(QString::number(crs->getIKPnumregression()));
                    stream.writeEndElement();
                }
                stream.writeEndElement();
                stream.writeEndElement();
            }
            stream.writeEndElement();
            stream.writeEndDocument();
            QMessageBox::information( this, "Export", QString("De xml file is opgeslagen als %1").arg(filename), QMessageBox::Ok, 0 );
        } else {
            QMessageBox::warning( this, "Export", QString("Fout bij opslaan van xml file %1").arg(filename), QMessageBox::Ok, 0 );
        }
    }
}

void MainWindow::on_actionToStart_triggered()
{
    if (m_project.numCrosssections() > 0) {
        m_project.setCurrent_crosssection_index(0);
        showActiveCrossSection();
    }
}

void MainWindow::on_actionBackward_triggered()
{
    if (m_project.getCurrent_crosssection_index() > 0) {
        m_project.setCurrent_crosssection_index(m_project.getCurrent_crosssection_index() - 1);
        showActiveCrossSection();
    }
}

void MainWindow::on_actionForward_triggered()
{
    gotoNextCrosssection();
}

void MainWindow::on_actionToEnd_triggered()
{
    if (m_project.numCrosssections() > 0) {
        m_project.setCurrent_crosssection_index(m_project.numCrosssections() - 1);
        showActiveCrossSection();
    }
}

void MainWindow::updateProcessorState(Qt::CheckState state, Processor* p)
{
    switch(state) {
    case Qt::Checked:
        p->setLocalEnable(Processor::Tristate::ON);
        break;
    case Qt::Unchecked:
        p->setLocalEnable(Processor::Tristate::OFF);
        break;
    case Qt::PartiallyChecked:
        p->setLocalEnable(Processor::Tristate::GLOBAL);
        break;
    }
}

void MainWindow::on_processorsListWidget_itemChanged(QListWidgetItem *item)
{
    if (!updating_rules) {
        Processor* p = reinterpret_cast<Processor*>(item->data(Qt::UserRole).toLongLong());
        if (p) {
            updateProcessorState(item->checkState(), p);
        } else {
            m_project.getCurrentCrosssection()->setAllProcessorState(item->checkState());
            for (Processor* p : m_project.getCurrentCrosssection()->getProcessors()) {
                updateProcessorState(item->checkState(), p);
            }
        }
        updateRules();
    }
}


void MainWindow::on_processorsListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    Processor* p = reinterpret_cast<Processor*>(item->data(Qt::UserRole).toLongLong());
    if (p) {
        QString html = "<html><body><h1>Processorstatus</h1>";
        html += "<p><table>";
        html += "<tr><td>Profiel:</td><td>" + p->getCrs()->getName() + "</td></tr>";
        html += "<tr><td>Processor:</td><td>" + p->getName() + "</td></tr>";
        html += "<tr><td>Status:</td><td>" + p->getStatus() + "</td></tr>";
        html += "</table></p>";
        html += "</body></html>";
        QMessageBox::information(this, "Processorstatus", html);
    }
}


void MainWindow::on_actionGo_triggered()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_project.runProcessorsOnAllCrosssections();

    ui->wProjectView->saveZoomState();
    initProfileList();

    showActiveCrossSection();

    QApplication::restoreOverrideCursor();
    ui->wProjectView->resetView();
    ui->wProjectView->resetZoomState();
}

void MainWindow::on_actionOne_triggered()
{
    if(m_project.getCurrentCrosssection()==nullptr) return;

    ui->wProjectView->saveZoomState();
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_project.runProcessorsOnCurrentCrosssection();
    initProfileList();
    showActiveCrossSection();
    QApplication::restoreOverrideCursor();
    ui->wProjectView->resetView();
    ui->wProjectView->resetZoomState();
}

void MainWindow::on_actionShowPoints_toggled(bool arg1)
{
    ui->wProjectView->setShowPoints(arg1);
}

void MainWindow::on_actionEdit_toggled(bool arg1)
{
    if (arg1) {
        ui->actionAdd->setChecked(false);
        ui->actionDelete->setChecked(false);
        ui->wProjectView->setClickmode(QProjectView::ClickMode::EDIT);
    } else {
        ui->wProjectView->setClickmode(QProjectView::ClickMode::NONE);
    }
}

void MainWindow::on_actionAdd_toggled(bool arg1)
{
    if (arg1) {
        ui->actionEdit->setChecked(false);
        ui->actionDelete->setChecked(false);
        ui->wProjectView->setClickmode(QProjectView::ClickMode::ADD);
    } else {
        ui->wProjectView->setClickmode(QProjectView::ClickMode::NONE);
    }
}

void MainWindow::on_actionDelete_toggled(bool arg1)
{
    if (arg1) {
        ui->actionAdd->setChecked(false);
        ui->actionEdit->setChecked(false);
        ui->wProjectView->setClickmode(QProjectView::ClickMode::DELETE);
    } else {
        ui->wProjectView->setClickmode(QProjectView::ClickMode::NONE);
    }
}

void MainWindow::on_actionReset_zoom_triggered()
{
    ui->actionBoxZoom->setChecked(false);
    ui->sliderXScale->setValue(DEFAULT_ZOOMFACTOR_X);
    ui->sliderYScale->setValue(DEFAULT_ZOOMFACTOR_Y);
    ui->wProjectView->resetView();
}


void MainWindow::on_actionShowLines_toggled(bool arg1)
{
    ui->wProjectView->setShowHelperLines(arg1);
}

void MainWindow::on_actionShowGrid_toggled(bool arg1)
{
    ui->wProjectView->setShowGrid(arg1);
}

void MainWindow::on_mouseLocationChanged(QPointF p)
{
    if(m_project.numCrosssections()>0) {
        m_lbl_mouse_location->setText(QString("L = %1, Z = %2").arg(p.x(), 5, 'f', 2).arg(p.y(), 5, 'f', 2));
    }
}

void MainWindow::on_actionSettings_triggered()
{
    SettingsDialog dlg(nullptr, m_project.getProcessorMapping());
    if (dlg.exec() == QDialog::Accepted) updateRules();
}

void MainWindow::on_profileListWidget_currentRowChanged(int currentRow)
{
    if ((currentRow >= 0) && (currentRow < m_project.numCrosssections())) {
        m_project.setCurrent_crosssection_index(currentRow);
        showActiveCrossSection();
    }
}

void MainWindow::on_teRemark_textChanged()
{
    Crosssection* crs = m_project.getCurrentCrosssection();

    if(crs!=nullptr){
        crs->setRemark(ui->teRemark->toPlainText());
    }
}

void MainWindow::updateTable()
{
    ui->tableCrosssection->model()->layoutChanged();
}

void MainWindow::on_actionReset_profiel_triggered()
{
    int currentrow = ui->profileListWidget->currentRow();
    QMessageBox msgBox;
    msgBox.setText("Deze actie maakt alle bewerkingen ongedaan. Wilt u hiermee doorgaan?");
    QAbstractButton* pButtonYesForOne = static_cast<QAbstractButton*>(msgBox.addButton(tr("Ja, alleen dit profiel"), QMessageBox::YesRole));
    QAbstractButton* pButtonYesForAll = static_cast<QAbstractButton*>(msgBox.addButton(tr("Ja, alle profielen"), QMessageBox::YesRole));
    msgBox.addButton("Nee", QMessageBox::NoRole);

    msgBox.exec();
    QAbstractButton* action = msgBox.clickedButton();
    if (action == pButtonYesForAll) {
        for (int i = 0; i < m_project.numCrosssections(); i++) {
            Crosssection* crs = m_project.getCrosssection(i);
            crs->reset();
        }
        ui->wProjectView->resetView();
        updateRules();
        initProfileList();
        ui->profileListWidget->setCurrentRow(currentrow);
        updateTable();
        ui->sliderXScale->setValue(DEFAULT_ZOOMFACTOR_X);
        ui->sliderYScale->setValue(DEFAULT_ZOOMFACTOR_Y);
    } else if (action == pButtonYesForOne) {
        ui->wProjectView->saveZoomState();
        m_project.getCurrentCrosssection()->reset();
        ui->wProjectView->resetView();
        updateRules();
        initProfileList();
        ui->profileListWidget->setCurrentRow(currentrow);
        updateTable();
        ui->wProjectView->resetZoomState();
    }
}

void MainWindow::on_actionactionShowCrosssectionInfo_triggered(bool checked)
{
    ui->wProjectView->setShowCrosssectionInfo(checked);
}

void MainWindow::on_projectviewScaleChanged(const QPointF new_scale)
{
    int nx = int(ui->sliderXScale->value() * new_scale.x());
    int ny = int(ui->sliderYScale->value() * new_scale.y());
    if(nx > MAX_ZOOMFACTOR_X) nx = MAX_ZOOMFACTOR_X;
    if(nx < MIN_ZOOMFACTOR_X) nx = MIN_ZOOMFACTOR_X;
    if(ny > MAX_ZOOMFACTOR_Y) ny = MAX_ZOOMFACTOR_Y;
    if(ny < MIN_ZOOMFACTOR_Y) ny = MIN_ZOOMFACTOR_Y;

    ui->sliderXScale->setValue(nx);
    ui->sliderYScale->setValue(ny);
}
