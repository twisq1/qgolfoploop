/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef POINT_H
#define POINT_H

#include "storable.h"

// A point with X,Y,Z coordinates.
class Point : Storable
{
public:
    enum CPID {
        NONE = 0,
        MAAIVELD_BUITENWAARTS = 100,
        TEEN_GEUL = 110,
        INSTEEK_GEUL = 120,
        TEEN_DIJK_BUITENWAARTS = 130,
        KRUIN_BUITENBERM = 140,
        INSTEEK_BUITENBERM = 150,
        KRUIN_BUITENTALUD = 160,
        RAND_VERKEERSBELASTING_BUITENWAARTS = 170,
        RAND_VERKEERSBELASTING_BINNENWAARTS = 180,
        KRUIN_BINNENTALUD = 190,
        INSTEEK_BINNENBERM = 200,
        KRUIN_BINNENBERM = 210,
        TEEN_DIJK_BINNENWAARTS = 220,
        INSTEEK_SLOOT_DIJKZIJDE = 230,
        SLOOTBODEM_DIJKZIJDE = 240,
        SLOOTBODEM_POLDERZIJDE = 250,
        INSTEEK_SLOOT_POLDERZIJDE = 260,
        MAAIVELD_BINNENWAARTS = 270,
        IMAGINAIR_KRUINPUNT = 280,
        IMAGINAIR_TEENPUNT = 290,
        OVERGANG_RUWHEID = 300
    };

    // Helper functions to translate between id and name.
    static QString idToString(const CPID id);
    static CPID stringToId(QString s);
    static double toDouble(const long mm) { return (((double)mm) / 1000.0); }
    static long toLong(const double m) { return (long) (m * 1000.0); }

    Point();
    Point(Point* p);
    Point(const double x, const double y, const double z);

    virtual ~Point();

    double x() const {return m_x;}
    double y() const {return m_y;}
    double z() const {return m_z;}
    double roughness() const {return m_roughness;}
    CPID getId() const {return m_id;}


    void setX(const double x) {m_x = x;}
    void setY(const double y) {m_y = y;}
    void setZ(const double z) {m_z = z;}
    void setRoughness(const double r) {m_roughness = r;}
    void setId(const CPID id) {m_id = id;}

    // Calculate the horizontal distance to a reference point. Ignore z.
    double l(const Point* base) const;
    long lmm(const Point* base) const;  // return distance as long in millimeters.

    bool isSameLocation(const Point* p) const;

    bool truncated() const;
    void setTruncated(bool truncated = true);
    bool invalid() const;
    void setInvalid(bool invalid);

    virtual bool read(const QJsonObject &json) override;
    virtual bool write(QJsonObject &json) const override;

protected:
    double m_x;
    double m_y;
    double m_z;
    bool   m_truncated;
    bool   m_invalid;
    double m_roughness;
    CPID m_id;
};


#endif // POINT_H
