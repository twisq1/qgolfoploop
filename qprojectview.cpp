/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "qprojectview.h"
#include "mainwindow.h"
#include "math.h"

#include <QPainter>
#include <QMouseEvent>
#include <QStyleOption>
#include <QStyle>
#include <QTextDocument>
#include <QAbstractTextDocumentLayout>
#include <QFontMetrics>

#include "simpleQtLogger.h"

#include "constants.h"

QProjectView::QProjectView(QWidget *parent) : QWidget(parent)
{
    m_project = nullptr;
    m_clickmode = ClickMode::NONE;
    m_zoomfactor_x = DEFAULT_ZOOMFACTOR_X;
    m_zoomfactor_y = DEFAULT_ZOOMFACTOR_Y;
    m_lmin = 0.0;
    m_lmax = 1.0;
    m_zmin = 0.0;
    m_zmax = 1.0;
    m_offset_x = 0;
    m_offset_y = 0;
    m_show_grid = true;
    m_show_cursor = true;
    m_show_points = false;
    m_show_crosssection_info = true;
    m_snap_to_points = true; //zie header voor betekenis
    m_grid_spacing_x = 1.0;
    m_grid_spacing_y = 1.0;
    m_mouse_down_pos.setX(-1);
    m_closestPoint = nullptr;

    setStyleSheet("background-color:white;");
    setMouseTracking(true);
}

QProjectView::~QProjectView()
{
}

void QProjectView::setProject(Project *project)
{
    m_project = project;
}

void QProjectView::setZoomX(const int zoomfactor)
{
    m_zoomfactor_x = zoomfactor;
    update();
}

void QProjectView::setZoomY(const int zoomfactor)
{
    m_zoomfactor_y = zoomfactor;
    update();
}

void QProjectView::setActiveLine(const QModelIndex &index)
{
    m_highlighted_line_index = index.row() + 1;
    update();
}

void QProjectView::setBoxZoom(const bool boxzoom)
{
    if(boxzoom){
        m_clickmode = ClickMode::BOXZOOM;
    }else{
        m_clickmode = ClickMode::NONE;
    }
    update();
}

void QProjectView::setClickmode(const ClickMode &clickmode)
{    
    m_clickmode = clickmode;
}

void QProjectView::setCurrentCrosssectionIndex(const int index)
{
    m_project->setCurrent_crosssection_index(index);
    resetView();
}

void QProjectView::setShowGrid(const bool value)
{
    m_show_grid = value;
    update();
}

void QProjectView::setShowHelperLines(const bool value)
{
    m_show_helperlines = value;
    update();
}

void QProjectView::setShowPoints(const bool value)
{
    m_show_points = value;
    update();
}

void QProjectView::setShowCrosssectionInfo(const bool value)
{
    m_show_crosssection_info = value;
    update();
}

void QProjectView::setHelperLineOffset(const double offset)
{
    m_helper_line_offset = offset;
    update();
}

void QProjectView::setCursorVisible(const bool value)
{
    m_show_cursor = value;
    update();
}

void QProjectView::resetView(const bool keep_zoom_state)
{
    Crosssection *crs = m_project->getCurrentCrosssection();
    m_highlighted_line_index = -1;


    if (crs != nullptr){
        //L_DEBUG(crs->getName());
        if(!keep_zoom_state){
            m_lmin = crs->lmin();
            m_lmax = crs->lmax();
            m_zmin = crs->zmin();
            m_zmax = crs->zmax();

            //calculate scale from given input
            m_sx = (m_lmax - m_lmin) / (width() - 2 * MARGIN);
            m_sy = (m_zmax - m_zmin) / (height() - 2 * MARGIN);

            //calculate the middle of the camera in world coordinats
            m_cmx = m_lmin + (m_lmax - m_lmin) / 2.0;
            m_cmy = m_zmax - (m_zmax - m_zmin) / 2.0;

            m_offset_x = 0;
            m_offset_y = 0;
        }

        update();
    }
}

void QProjectView::saveZoomState()
{
    m_old_zoomfactor_x = m_zoomfactor_x;
    m_old_zoomfactor_y = m_zoomfactor_y;
    m_old_offset_x = m_offset_x;
    m_old_offset_y = m_offset_y;
    m_old_sx = m_sx;
    m_old_sy = m_sy;
    m_old_cmx = m_cmx;
    m_old_cmy = m_cmy;
}

void QProjectView::resetZoomState()
{
    m_zoomfactor_x = m_old_zoomfactor_x;
    m_zoomfactor_y = m_old_zoomfactor_y;
    m_offset_x = m_old_offset_x;
    m_offset_y = m_old_offset_y;
    m_sx = m_old_sx;
    m_sy = m_old_sy;
    m_cmx = m_old_cmx;
    m_cmy = m_old_cmy;
    resetView(true);
}

void QProjectView::resizeEvent(QResizeEvent *evt)
{
    Q_UNUSED(evt)

    m_sx = (m_lmax - m_lmin) / (width() - 2 * MARGIN);
    m_sy = (m_zmax - m_zmin) / (height() - 2 * MARGIN);
    update();
}

void QProjectView::paintEvent(QPaintEvent *evt)
{
    Q_UNUSED(evt)

    //this code is needed for Qt to use a subclasses widget and attach the widget style as specified in the constructor
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    //cursor
    /*if(m_show_cursor){
        painter.drawLine(m_mouse_pos.x(), 0, m_mouse_pos.x(), height());
        painter.drawLine(0, m_mouse_pos.y(), width(), m_mouse_pos.y());
    }*/

    Crosssection *crs = m_project->getCurrentCrosssection();
    if (crs) drawCrosssection(crs);
    if (m_clickmode==ClickMode::BOXZOOM){
        drawBoxZoom();
    }
}

void QProjectView::drawCrosssection(Crosssection *crs)
{
    QPainter painter(this);
    //bewaar de oude pen
    QPen oldpen = painter.pen();
    QFontMetrics fm = painter.fontMetrics();

    if (m_show_grid) {
        QPointF topleft = screenToWorld(QPoint(0,0));
        QPointF bottomright = screenToWorld(QPoint(width(),height()));
        QPointF ptopleft;
        QPointF pbottomright;

        double xt = topleft.x();
        double yt = topleft.y();

        //de volgende code start het grid vanuit het nulpunt
        double l = copysign(ceil(fabs(xt)/m_grid_spacing_x)*m_grid_spacing_x, xt);
        double z = copysign(ceil(fabs(yt)/m_grid_spacing_y)*m_grid_spacing_y, yt);

        ptopleft.setX(l);
        ptopleft.setY(z);

        pbottomright.setX(ceil(bottomright.x()));
        pbottomright.setY(floor(bottomright.y()));

        double x = ptopleft.x();
        while(x<=pbottomright.x()){
            double y = ptopleft.y();
            while(y>=pbottomright.y()){
                QPoint sp = worldToScreen(x, y);
                y -= m_grid_spacing_y;
                painter.drawPoint(sp);
            }
            x += m_grid_spacing_x;
        }
    }

    if(m_show_helperlines){
        //de nul lijn
        Point p(0.0, 0.0, 0.0);
        QPoint ps1 = worldToScreen(p);

        QPen hlpen(Qt::black);
        hlpen.setStyle(Qt::DashLine);
        painter.setPen(hlpen);
        painter.drawLine(0, ps1.y(), width(), ps1.y());
        painter.drawText(5, ps1.y(), "0.0");

        //de offset lijn
        p.setZ(m_helper_line_offset);
        QPoint ps2 = worldToScreen(p);
        hlpen.setColor(Qt::gray);
        painter.setPen(hlpen);
        painter.drawLine(0, ps2.y(), width(), ps2.y());
        painter.drawText(5, ps2.y(), QString::number(p.z(), 'f', 1));
        painter.setPen(oldpen);
    }

    // toon overgang voorland

    if (crs->voorland()) {
        QPen voorlandpen(COLOR_VOORLAND);
        QPoint pvl = worldToScreen(Point::toDouble(crs->voorland()), 0.0);
        painter.setPen(voorlandpen);
        painter.drawLine(pvl.x(), 0, pvl.x(), height());
        QString txt("Voorland");
        QFontMetrics fm = painter.fontMetrics();
        painter.drawText(pvl.x() - fm.horizontalAdvance(txt) - 5, fm.height(), txt);
    }

    // Now draw the geometic profile, with all the points

    QPen geopointpen(COLOR_GEOMETRIC_PROFILE);
    geopointpen.setCapStyle(Qt::RoundCap);
    geopointpen.setWidth(POINT_GEOMETRIC_PROFILE);
    QPen geolinepen(COLOR_GEOMETRIC_PROFILE);
    geolinepen.setCapStyle(Qt::RoundCap);
    geolinepen.setWidth(WIDHT_GEOMETRIC_PROFILE);

    QPoint ps1;
    bool ps1ok = false;    
    foreach (Point* p2, crs->getOriginalSurfacePoints()) {
        QPoint ps2 = worldToScreen(*p2);

        if (m_show_points) {
            painter.setPen(geopointpen);
            painter.drawPoint(ps2);
        }
        if (ps1ok) {
            painter.setPen(geolinepen);
            painter.drawLine(ps1, ps2);
        }
        ps1 = ps2;
        ps1ok = true;        
    }

    // Now draw the golfoploop profile
    QPen goppointpen(COLOR_GOLFOPLOOP_PROFILE);
    goppointpen.setCapStyle(Qt::RoundCap);
    goppointpen.setWidth(POINT_GOLFOPLOOP_PROFILE);
    QPen goplinepen(COLOR_GOLFOPLOOP_PROFILE);
    goplinepen.setCapStyle(Qt::RoundCap);
    goplinepen.setWidth(WIDHT_GOLFOPLOOP_PROFILE);
    QPen invalidpointpen(COLOR_INVALID_PROFILE);
    invalidpointpen.setCapStyle(Qt::RoundCap);
    invalidpointpen.setWidth(POINT_INVALID_PROFILE);
    QPen invalidlinepen(COLOR_INVALID_PROFILE);
    invalidlinepen.setCapStyle(Qt::RoundCap);
    invalidlinepen.setWidth(WIDHT_INVALID_PROFILE);

    if (crs->voorland()) {
        goplinepen.setColor(COLOR_VOORLAND); //Als er een voorland is, beginnen we met de voorland kleur.
    } else {
        goplinepen.setColor(COLOR_GOLFOPLOOP_PROFILE);
    }
    ps1ok = false;
    Point* ref = crs->getReferencePoint();
    double roughness = ref->roughness();
    int index=0;
    foreach (Point* p2, crs->getSurfacePoints()) {
        QPoint ps2 = worldToScreen(*p2);
        painter.setPen(p2->invalid() ? invalidpointpen : goppointpen);

        painter.drawPoint(ps2);
        if (ps1ok) {
            if(crs->isTrunacted() && index==m_highlighted_line_index){
                QString txt = QString("%1").arg(roughness);
                int xmid = (ps2.x() + ps1.x()) / 2;
                int ymid = (ps2.y() + ps1.y()) / 2;
                painter.drawText(QPointF(xmid - fm.horizontalAdvance(txt), ymid - 5), txt);
                QPen pen = p2->invalid() ? invalidlinepen : goplinepen;
                pen.setStyle(Qt::DotLine);
                painter.setPen(pen);
            }else{
                painter.setPen(p2->invalid() ? invalidlinepen : goplinepen);
                roughness = p2->roughness();
            }
            painter.drawLine(ps1, ps2);
        }
        ps1 = ps2;
        ps1ok = true;

        if (p2->lmm(ref) >= crs->voorland()) {
            goplinepen.setColor(COLOR_GOLFOPLOOP_PROFILE); //verander pen kleur als we voorbij de opgegeven l zijn
        }
        index++;
    }

    QPen kppen(Qt::red);
    kppen.setCapStyle(Qt::RoundCap);
    kppen.setWidth(2);
    painter.setPen(kppen);
    foreach (Point *cp, crs->getCharacteristicPoints()) {
        QPoint ps1 = worldToScreen(cp);
        painter.drawEllipse(ps1, 5, 5);
    }
    painter.setPen(oldpen);

    // Now highlight the active point
    if (crs->getActivePoint()) {
        QPen indentpen(COLOR_ACTIVE);
        indentpen.setCapStyle(Qt::RoundCap);
        indentpen.setWidth(POINT_ACTIVE);
        painter.setPen(indentpen);
        painter.drawPoint(worldToScreen(crs->getActivePoint()));
    }

    // And highlight the closest point
    if (m_closestPoint){
        QPen indentpen(COLOR_CLOSEST_POINT);
        indentpen.setCapStyle(Qt::RoundCap);
        indentpen.setWidth(POINT_ACTIVE);
        painter.setPen(indentpen);
        painter.drawPoint(worldToScreen(*m_closestPoint));
    }

    // draw bekledingsovergangen
    QPen bvpen(COLOR_BEKLEDINGSVLAK);
    bvpen.setStyle(Qt::DashLine);
    bvpen.setWidth(1);
    painter.setPen(bvpen);
    double rn = -1.0;
    foreach (Point* p, crs->getSurfacePoints()) {
        if ((rn > -1.0) && ((fabs(rn - p->roughness())) > 0.0005)) {
            QPoint pw = worldToScreen(p->l(ref), p->z());
            QString name = m_project->getRoughnessName(p->roughness());
            painter.drawLine(0, pw.y(), width(), pw.y());
            painter.drawText(OFFSET_X_BEKLEDINGSNAAM, pw.y() + OFFSET_Y_BEKLEDINGSNAAM, name);
        }
        rn = p->roughness();
    }

    if(m_show_crosssection_info){
        //draw crossection information
        QTextDocument td;
        //painter.translate(QPoint(10,10));
        QString html;
        html.append(QString("<h4>Dwarsprofiel %1</h4").arg(crs->getName()));
        html.append(QString("<ul>"));
        if(crs->hasZetting())
            html.append(QString("<li>zetting: %1</li>").arg(crs->getZetting()));
        if(crs->getDam()->isValid())
            html.append(QString("<li>dam hoogte: %1</li>").arg(crs->getDam()->height()));
        if(crs->getDamwand()->isValid())
            html.append(QString("<li>damwand hoogte: %1</li>").arg(crs->getDamwand()->height()));
        html.append(QString("</ul>"));
        html.append(QString("<p><i>%1</i></p>").arg(crs->getRemark().replace("\n", "<br>")));
        td.setHtml(html);
        QAbstractTextDocumentLayout::PaintContext ctx;
        ctx.clip = QRectF(10,10,300,300);
        td.documentLayout()->draw(&painter, ctx);
        //painter.translate(QPoint(0,0));
    }

    painter.setPen(oldpen);


}

void QProjectView::drawBoxZoom()
{
    QPainter painter(this);
    //bewaar de oude pen
    QPen oldpen = painter.pen();

    if(m_mouse_down_pos.x()!=-1){
        int dx = m_mouse_pos.x() - m_mouse_down_pos.x();
        int dy = m_mouse_pos.y() - m_mouse_down_pos.y();
        painter.drawRect(QRect(m_mouse_down_pos.x(), m_mouse_down_pos.y(), dx, dy));
    }

    painter.setPen(oldpen);
}

Point* QProjectView::findNearestPoint(double l)
{
    Crosssection *crs = m_project->getCurrentCrosssection();
    if (!crs) return nullptr;

    double mindistance = 10; // Only look for points within 10 meters.
    double distance;
    Point *np = nullptr;
    foreach (Point* p, crs->getSurfacePoints()) {
        if (p->lmm(crs->getReferencePoint()) > 0) {     // Don't replace the base point.
            distance = fabs(l - p->l(crs->getReferencePoint()));
            if (distance < mindistance) {
                mindistance = distance;
                np = p;
            }
        }
    }
    return np;
}

void QProjectView::mouseMoveEvent(QMouseEvent *evt)
{
    if(evt->buttons() == Qt::RightButton){
        int dx = evt->x() - m_mouse_pos.x();
        int dy = evt->y() - m_mouse_pos.y();
        m_offset_x += dx;
        m_offset_y += dy;
    }

    if(m_clickmode == ClickMode::EDIT || m_clickmode == ClickMode::ADD || m_clickmode==ClickMode::DELETE){
        QPointF lz = screenToWorld(evt->pos());
        m_closestPoint = findNearestPoint(lz.x());
    }else{
        m_closestPoint = nullptr;
    }

    Crosssection* crs = m_project->getCurrentCrosssection();
    if (crs) {
        QPointF lz = screenToWorld(evt->pos());
        emit mousePositionChanged(lz);

        if (crs->getActivePoint() && (lz.x() > 0.002)) { // > 0.002 to prevent the reference point from being deleted.

            crs->setActivePoint(crs->moveSurfacePoint(crs->getActivePoint(), lz.x(), lz.y()));
            crs->setDirty(true);
            // emit crosssectionChanged();
        }
    }

    m_mouse_pos = evt->pos();
    update();
}

void QProjectView::mousePressEvent(QMouseEvent *evt)
{

    Crosssection *crs = m_project->getCurrentCrosssection();

    if(crs == nullptr) return;

    if(evt->button() == Qt::LeftButton) {

        QPointF click{screenToWorld(evt->pos())};

        switch (m_clickmode) {

        case ClickMode::EDIT:
            crs->setActivePoint(findNearestPoint(click.x()));
            if (crs->getActivePoint()) {
                crs->validate();
                emit updateBottom();
                update();
                crs->setDirty(true);
                emit crosssectionChanged();
            }
            break;

         case ClickMode::DELETE:
            crs->setActivePoint(findNearestPoint(click.x()));
            if (crs->getActivePoint()) {
                crs->removeSurfacePoint(crs->getActivePoint()->lmm(crs->getReferencePoint()));
                crs->setActivePoint(nullptr);
                crs->validate();
                emit updateBottom();
                update();
                crs->setDirty(true);
                emit crosssectionChanged();
            }
            break;

         case ClickMode::ADD:
            crs->setActivePoint(new Point(crs->LZtoRDW(click)));
            crs->addSurfacePoint(crs->getActivePoint());
            crs->setActivePoint(nullptr);
            emit updateBottom();
            update();
            crs->setDirty(true);
            emit crosssectionChanged();
            break;

         case ClickMode::BOXZOOM:
            m_mouse_down_pos = evt->pos();
            break;

         case ClickMode::NONE:
            break;
        }
    }
}

void QProjectView::mouseReleaseEvent(QMouseEvent *evt)
{    
    Crosssection *crs = m_project->getCurrentCrosssection();
    if (!crs) return;

    if(m_clickmode==ClickMode::BOXZOOM && evt->button() == Qt::LeftButton){
        int xmid = int((m_mouse_down_pos.x() + m_mouse_pos.x())/2);
        int ymid = int((m_mouse_down_pos.y() + m_mouse_pos.y())/2);

        QPointF wmid = screenToWorld(QPoint(xmid, ymid));
        m_cmx = wmid.x();
        m_cmy = wmid.y();
        m_offset_x = 0;
        m_offset_y = 0;

        qreal sx = width() / qreal(qAbs(m_mouse_down_pos.x() - m_mouse_pos.x()));
        qreal sy = height() / qreal(qAbs(m_mouse_down_pos.y() - m_mouse_pos.y()));

        emit scaleFactorChanged(QPointF(sx, sy));

        update();
    }

    if (crs->getActivePoint() && (evt->button() == Qt::LeftButton)) {
        crs->setActivePoint(nullptr);
        crs->validate();
        emit updateBottom();
        crs->setDirty(true);
        emit crosssectionChanged();
        update();
    }
    m_mouse_down_pos.setX(-1);
}

void QProjectView::wheelEvent(QWheelEvent *evt)
{
    if(evt->modifiers() & Qt::ShiftModifier){
        m_zoomfactor_x += evt->delta() / 60;
        if(m_zoomfactor_x > MAX_ZOOMFACTOR_X) m_zoomfactor_x = MAX_ZOOMFACTOR_X;
        if(m_zoomfactor_x < MIN_ZOOMFACTOR_X) m_zoomfactor_x = MIN_ZOOMFACTOR_X;
        emit zoomFactorXChanged(m_zoomfactor_x);
        update();
    }else{
        m_zoomfactor_y += evt->delta() / 60;
        if(m_zoomfactor_y > MAX_ZOOMFACTOR_Y) m_zoomfactor_y = MAX_ZOOMFACTOR_Y;
        if(m_zoomfactor_y < MIN_ZOOMFACTOR_Y) m_zoomfactor_y = MIN_ZOOMFACTOR_Y;
        emit zoomFactorYChanged(m_zoomfactor_y);
        update();
    }
}

QPoint QProjectView::worldToScreen(const Point& p) const
{
    Crosssection *crs = m_project->getCurrentCrosssection();
    if (crs && crs->getReferencePoint()) {
        return worldToScreen(p.l(crs->getReferencePoint()), p.z());
    }
    return QPoint(0, 0);
}

QPoint QProjectView::worldToScreen(const double x, const double y) const
{
    double dx = x - m_cmx;
    double dy = y - m_cmy;
    int dxpx = int(dx / m_sx * m_zoomfactor_x / 100.0);
    int dypx = int(dy / m_sy * m_zoomfactor_y / 100.0);
    return QPoint(width()/2 + dxpx + m_offset_x, height()/2 - dypx + m_offset_y);
}

QPointF QProjectView::screenToWorld(const QPoint &p)
{
    QPointF result;
    int dx = p.x() - width() / 2 - m_offset_x;
    int dy = p.y() - height() / 2 - m_offset_y;
    double l = m_cmx + dx * m_sx / (m_zoomfactor_x / 100.0);
    double z = m_cmy - dy * m_sy / (m_zoomfactor_y / 100.0);
    result.setX(l);
    result.setY(z);
    return result;
}
