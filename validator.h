/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef VALIDATOR_H
#define VALIDATOR_H

#include "corrector.h"

#define GLOBAL_CHECK_START      "val_check_start"
#define GLOBAL_CHECK_END        "val_check_end"
#define GLOBAL_CHECK_BERM       "val_check_maxberm"
#define GLOBAL_MAX_BERM         "val_max_berm"

#define DEFAULT_CHECK_START     true
#define DEFAULT_CHECK_END       true
#define DEFAULT_CHECK_BERM      true
#define DEFAULT_MAX_BERM        2

class Validator : public Corrector
{
public:
    Validator(Crosssection* crs);

    static void setGlobalCheckStart(const bool check);
    static void setGlobalCheckEnd(const bool check);
    static void setGlobalCheckBerm(const bool check);
    static void setGlobalMaxBerm(const int x);
    static bool getGlobalCheckStart();
    static bool getGlobalCheckEnd();
    static bool getGlobalCheckBerm();
    static int  getGlobalMaxBerm();

    // Processor interface
public:
    virtual QString getName() const override;

private:
    virtual void doProcess() override;
};

#endif // VALIDATOR_H
