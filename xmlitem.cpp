/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "simpleQtLogger.h"
#include "xmlitem.h"

XMLItem::XMLItem(QDomNode node) :
    m_node(node)
{
}

QList<XMLItem> XMLItem::getChildren()
{
    QList<XMLItem> list;
    for (int i = 0; i < m_node.childNodes().size(); i++) {
        QDomNode node = m_node.childNodes().at(i);
        list.append(node);
    }
    return list;
}

bool XMLItem::getItemById(const QString id, XMLItem *item)
{
    for (XMLItem x : getChildren()){
        if(x.getName() == id){
            *item = x;
            return true;
        }
    }
    return false;
}

QString XMLItem::getName() const
{
    return m_node.nodeName();
}

bool XMLItem::getValueAsString(QString name, QString *value)
{
    for (int i = 0; i < m_node.childNodes().size(); i++) {
        QDomNode node = m_node.childNodes().at(i);
        if (name == node.nodeName()) {
            *value = node.firstChild().nodeValue();
            return true;
        }
    }
    return false;
}

bool XMLItem::getValueAsInt(QString name, int *value)
{
    QString s;
    if (getValueAsString(name, &s)) {
        bool ok;
        *value = s.toInt(&ok);
        return ok;
    }
    return false;
}

bool XMLItem::getValueAsDouble(QString name, double *value)
{
    QString s;
    if (getValueAsString(name, &s)) {
        bool ok;
        *value = s.toDouble(&ok);
        return ok;
    }
    return false;
}

