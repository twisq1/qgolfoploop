/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "mainwindow.h"
#include <QApplication>
#include <QSettings>
#include <QDir>
#include "simpleQtLogger.h"

int main(int argc, char *argv[])
{
    // Gloabl settings
    QCoreApplication::setOrganizationName("TwisQ");
    QCoreApplication::setApplicationVersion("1.5.3");
    QCoreApplication::setOrganizationDomain("twisq.nl");
    QCoreApplication::setApplicationName("qGolfoploop");

    // Initialize logger
    Q_ASSERT_X(SQTL_VERSION >= SQTL_VERSION_CHECK(1, 2, 0), "main", "SimpleQtLogger version");

    // enable sinks
    simpleqtlogger::ENABLE_LOG_SINK_FILE = true;
    simpleqtlogger::ENABLE_LOG_SINK_CONSOLE = true;
    simpleqtlogger::ENABLE_LOG_SINK_QDEBUG = true;
    simpleqtlogger::ENABLE_LOG_SINK_SIGNAL = false;
    // set log-features
    simpleqtlogger::ENABLE_FUNCTION_STACK_TRACE = true;
    simpleqtlogger::ENABLE_CONSOLE_COLOR = true;
    simpleqtlogger::ENABLE_CONSOLE_TRIMMED = true;
    // set log-levels (global; all enabled)
    simpleqtlogger::ENABLE_LOG_LEVELS.logLevel_DEBUG = true;
    simpleqtlogger::ENABLE_LOG_LEVELS.logLevel_FUNCTION = true;
    // set log-levels (specific)
    simpleqtlogger::EnableLogLevels enableLogLevels_file = simpleqtlogger::ENABLE_LOG_LEVELS;
    simpleqtlogger::EnableLogLevels enableLogLevels_console = simpleqtlogger::ENABLE_LOG_LEVELS;
    simpleqtlogger::EnableLogLevels enableLogLevels_qDebug = simpleqtlogger::ENABLE_LOG_LEVELS;
    simpleqtlogger::EnableLogLevels enableLogLevels_signal = simpleqtlogger::ENABLE_LOG_LEVELS;
    enableLogLevels_console.logLevel_FUNCTION = false;

    // Get location and name of logfile.
    QSettings settings;
    QString logfile = settings.value("logfile").toString();
    if (logfile.isEmpty()) {
        logfile = QDir::home().filePath("qgolfoploop.log");
        settings.setValue("logfile", logfile);
    }

    // initialize SimpleQtLogger (step 1/2)
    simpleqtlogger::SimpleQtLogger::createInstance(qApp)->setLogFormat_file("<TS> [<LL>] <TEXT> (<FUNC>@<FILE>:<LINE>)", "<TS> [<LL>] <TEXT>");
    simpleqtlogger::SimpleQtLogger::getInstance()->setLogFileName(logfile, 10*1000, 10);
    simpleqtlogger::SimpleQtLogger::getInstance()->setLogLevels_file(enableLogLevels_file);
    simpleqtlogger::SimpleQtLogger::getInstance()->setLogLevels_console(enableLogLevels_console);
    simpleqtlogger::SimpleQtLogger::getInstance()->setLogLevels_qDebug(enableLogLevels_qDebug);
    simpleqtlogger::SimpleQtLogger::getInstance()->setLogLevels_signal(enableLogLevels_signal);

    //L_INFO("QGolfOploop starting");
    //L_DEBUG("Logfile = " + logfile);

    QApplication a(argc, argv);
    MainWindow w;

    w.show();
    a.setWindowIcon(QIcon(":/icons/qgolfoploop.png"));
    int r = a.exec();

    return r;
}
