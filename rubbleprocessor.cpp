/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "rubbleprocessor.h"
#include "constants.h"
#include <QDebug>
#include <QList>
#include <QSettings>
#include "crosssection.h"
#include "math.h"

RubbleProcessor::RubbleProcessor(Crosssection* crs) : Processor(crs)
{
}

QString RubbleProcessor::getName() const
{
    return PROCESSOR_BREUKSTEEN;
}

void RubbleProcessor::setGlobalRubbleIgnore(const int ignore)
{
    QSettings settings;
    settings.setValue(GLOBAL_RUBBLE_IGNORE_KEY, ignore);
}

void RubbleProcessor::setGlobalRubbleRegression(const int regression)
{
    QSettings settings;
    settings.setValue(GLOBAL_RUBBLE_REGRESSION_KEY, regression);
}

void RubbleProcessor::setGlobalRubbleRoughness(const double roughness)
{
    QSettings settings;
    settings.setValue(GLOBAL_RUBBLE_ROUGHNESS_KEY, roughness);
}

void RubbleProcessor::setGlobalRubbleKeepRoughness(const bool keep)
{
    QSettings settings;
    settings.setValue(GLOBAL_RUBBLE_KEEP_ROUGHNESS_KEY, keep);
}

int RubbleProcessor::getGlobalRubbleIgnore()
{
    QSettings settings;
    return settings.value(GLOBAL_RUBBLE_IGNORE_KEY, DEFAULT_RUBBLE_IGNORE).toInt();
}

int RubbleProcessor::getGlobalRubbleRegression()
{
    QSettings settings;
    return settings.value(GLOBAL_RUBBLE_REGRESSION_KEY, DEFAULT_RUBBLE_REGRESSION).toInt();
}

double RubbleProcessor::getGlobalRubbleRoughness()
{
    QSettings settings;
    return settings.value(GLOBAL_RUBBLE_ROUGHNESS_KEY, DEFAULT_RUBBLE_ROUGHNESS).toDouble();
}

bool RubbleProcessor::getGlobalRubbleKeepRoughness()
{
    QSettings settings;
    return settings.value(GLOBAL_RUBBLE_KEEP_ROUGHNESS_KEY, DEFAULT_RUBBLE_KEEP_ROUGHNESS).toBool();
}

void RubbleProcessor::doProcess()
{
    bool changeRoughness = !getGlobalRubbleKeepRoughness();
    double r = getGlobalRubbleRoughness();
    bool rubble = false;
    double lastr = 1.0;
    Point* teen = nullptr;
    foreach (Point* p, crs()->getSurfacePoints()) {
        if(p != crs()->getSurfacePoints().at(0)){ //skip the leftmost point, this area is voorland
            if (fabs(p->roughness() - r) < 0.0001) {
                if (rubble) {
                    if (p->getId() != Point::CPID::TEEN_DIJK_BUITENWAARTS) {
                        p->setTruncated();
                    } else {
                        teen = p;
                    }

                } else {
                    rubble = true;
                    if (changeRoughness) p->setRoughness(lastr);
                }
            } else {
                rubble = false;
                lastr = p->roughness();
                if (teen) {
                    if (changeRoughness) teen->setRoughness(lastr);
                    teen = nullptr;
                }
            }
        }
    }
}
