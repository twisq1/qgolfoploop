/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "simpleQtLogger.h"
#include "crosssection.h"
#include "constants.h"
#include "tooshallowcorrector.h"

TooShallowCorrector::TooShallowCorrector(Crosssection *crs)
    : Corrector(crs)
{
}

QString TooShallowCorrector::getName() const
{
    return PROCESSOR_COR_GENTLESLOPE;
}

void TooShallowCorrector::doProcess()
{
    bool correction = false;

    // Check voorland.
    Point* ref = crs()->getReferencePoint();
    long voorland_lmm = crs()->voorland();
    if (!voorland_lmm) {
        L_WARN("Profiel heeft geen voorland.");
        setOk(false);
        setStatus("Profiel heeft geen voorland.");
    }

    // Get parameters.
    double max_slope_bank = 1.0 / getGlobalMaxBank();
    double min_slope_slope = 1.0 / getGlobalMinDam();

    Point* p1 = nullptr;
    double shift = 0.0;
    QList<Point*> pointsBefore = crs()->getSurfacePoints();

    // Walk through profile, segment by segment. Each time the segment between p1 and p2 is processed.
    for (int n = 0; n < pointsBefore.length(); n++) {
        Point* p2 = pointsBefore.at(n);
        if (p1) {

            // check only dam part.
            if (p2->lmm(ref) > voorland_lmm) {

                // Shift point if shift <> 0. This could be because a correction of a too steep or to shallow dam segment.
                if ((shift > 0.0005) || (shift < -0.0005)) {
                    p2 = crs()->moveSurfacePoint(p2, p2->l(ref) + shift, p2->z());
                }

                // Calculate slope of current segment and do the checks.
                double slope = crs()->getZSlope(p1, p2);

                if ((slope > max_slope_bank) && (slope < min_slope_slope)) {

                    // Deel is te steil voor een berm en te flauw voor een talud, maak steiler door profiel naar links op de schuiven.
                    correction = true;
                    double addshift = correctSlope(&p1, &p2, shift, min_slope_slope * OVERSHOOT);
                    L_DEBUG("Te flauw. Schuif profiel horizontaal op: " + QString::number(addshift, 'f', 3));

                }
            }
        }
        p1 = p2;
    }
    crs()->addRemark("Profiel automatisch aangepast: Te flauw bermdeel steiler gemaakt.");
}
