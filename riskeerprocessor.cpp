/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QRegularExpression>
#include <QList>
#include "simpleQtLogger.h"
#include "crosssection.h"
#include "riskeerprocessor.h"
#include "constants.h"

RiskeerProcessor::RiskeerProcessor(Crosssection *crs)
    : Processor(crs)
{

}

QString RiskeerProcessor::getName() const
{
    return PROCESSOR_RISKEER;
}

void RiskeerProcessor::doProcess()
{
    reset();

    // First remove special characters from profile name.
    QString name = crs()->getName();
    name.replace(QRegularExpression("[^a-zA-Z0-9]"),"R");
    crs()->setName(name);

    // Now round all points to centimeter scale.
    QList<Point*> cmPoints;
    for (Point* pmm : crs()->getSurfacePoints(true)) {
        Point* pcm = new Point(pmm);
        pcm->setX(qRound(pmm->x() / PRECISION) * PRECISION);
        pcm->setY(qRound(pmm->y() / PRECISION) * PRECISION);
        pcm->setZ(qRound(pmm->z() / PRECISION) * PRECISION);
        cmPoints.append(pcm);
    }

    // Remove old surface points
    crs()->deleteAllSurfacePoints();

    // Add new surfacepoints and avoid duplicates.
    Point* base = crs()->getReferencePoint();
    long lbefore = -1;
    Point* pbefore = nullptr;
    for (Point* p : cmPoints) {
        long l = qRound(p->l(base) / PRECISION);
        if (pbefore && (l == lbefore)) {
            // Duplicate detected. Merge points.
            pbefore->setTruncated(pbefore->truncated() && p->truncated());
            pbefore->setInvalid(pbefore->invalid() && p->invalid());
            if ((pbefore->getId() != Point::NONE) && (p->getId() != Point::NONE) && (pbefore->getId() != p->getId())) {
                setOk(false);
                setStatus("Door aanpassing t.b.v. Riskeer is er informatie over karakteristieke punten verdwenen.");
            }
            pbefore->setId(qMax(pbefore->getId(), p->getId()));
            delete p;
        } else {
            // No duplicate. Add it.
            crs()->addSurfacePoint(p);
            pbefore = p;
        }
        lbefore = l;
    }
}
