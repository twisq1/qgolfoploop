/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef CORRECTOR_H
#define CORRECTOR_H

#include <QPointF>
#include <QPair>
#include "processor.h"
#include "point.h"

#define GLOBAL_CHECK_NEGATIVE_KEY       "cor_check_negative"
#define GLOBAL_CHECK_TOO_FLAT_KEY       "cor_check_too_flat"
#define GLOBAL_CHECK_TOO_STEEP_KEY      "cor_check_too_steep"
#define GLOBAL_ALMOST_HORIZONTAL        "cor_slope_almost_horizontal"
#define GLOBAL_MAX_BANK                 "cor_max_bank"
#define GLOBAL_MIN_DAM                  "cor_min_dam"
#define GLOBAL_MAX_DAM                  "cor_max_dam"

#define DEFAULT_ALMOST_HORIZONTAL 100.0
#define DEFAULT_MAX_BANK          15.0
#define DEFAULT_MIN_DAM           8.0
#define DEFAULT_MAX_DAM           1.0

// Correct a little bit more to avoid rounding errors.
#define OVERSHOOT                 1.0001

// A line has two parameters, its slope, and it intersection with the y-axis.
typedef QPair<double, double> Line;

class Corrector : public Processor
{

public:
    Corrector(Crosssection* crs);

    static void setGlobalCheckNegativeKey(const bool check);
    static void setGlobalCheckTooFlat(const bool check);
    static void setGlobalCheckTooSteep(const bool check);
    static void setGlobalAlmostHorizontal(const double x);
    static void setGlobalMaxBank(const double x);
    static void setGlobalMinDam(const double x);
    static void setGlobalMaxDam(const double x);
    static bool getGlobalCheckNegativeKey();
    static bool getGlobalCheckTooFlat();
    static bool getGlobalCheckTooSteep();
    static double getGlobalAlmostHorizontal();
    static double getGlobalMaxBank();
    static double getGlobalMinDam();
    static double getGlobalMaxDam();

protected:
    double correctSlope(Point** pp1, Point** pp2, double& shift, double newslope);
    QPointF getIntersection(QPointF q1, QPointF q2, Line secant, bool* hit);
    QPointF getIntersection(Line line1, Line line2, bool *hit);
    Line getLine(QPointF p, double rc);
};

#endif // CORRECTOR_H
