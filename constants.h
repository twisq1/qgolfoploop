/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2017 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>

const int MAX_ZOOMFACTOR_X = 10000;
const int MAX_ZOOMFACTOR_Y = 10000;
const int MIN_ZOOMFACTOR_X = 10;
const int MIN_ZOOMFACTOR_Y = 10;
const int DEFAULT_ZOOMFACTOR_X = 100;
const int DEFAULT_ZOOMFACTOR_Y = 100;
const double DEFAULT_GRID_SPACING_X = 5.0;
const double DEFAULT_GRID_SPACING_Y = 0.5;
const double DEFAULT_HELPER_LINE_OFFSET = 1.0;
const bool DEFAULT_SHOW_GRID = true;
const bool DEFAULT_SHOW_HELPERLINES = true;
const bool DEFAULT_SHOW_POINTS = true;
const int NUM_MANDATORY_FILES = 7;

const int COLOR_VOORLAND = 0x0000ff; //full blue
const int COLOR_VOORLAND_TABLE = 0xcceeff;      // light blue
const int COLOR_CHARACTERISTIC_POINT = 0xff0000; // red
const int COLOR_SELECTED_POINT = 0xff8080;      // light red
const int COLOR_GEOMETRIC_PROFILE = 0x99ccff;   // ~light blue
const int POINT_GEOMETRIC_PROFILE = 4;
const int WIDHT_GEOMETRIC_PROFILE = 1;
const int COLOR_GOLFOPLOOP_PROFILE = 0xccaa00;  // ~orange
const int POINT_GOLFOPLOOP_PROFILE = 6;
const int WIDHT_GOLFOPLOOP_PROFILE = 2;
const int COLOR_INVALID_PROFILE = 0xff0000;     // red
const int POINT_INVALID_PROFILE = 6;
const int WIDHT_INVALID_PROFILE = 2;
const int COLOR_ACTIVE = 0x80ff00;              // ~green
const int POINT_ACTIVE = 8;
const int COLOR_BEKLEDINGSVLAK = 0x585858; //dark greyish
const int COLOR_TEXT = 0x000000; //black
const int COLOR_CLOSEST_POINT = 0xFF0000; //red

const int COLOR_IMAGINARY_POINT = 0x000000;     // black
const int POINT_IMAGINARY_POINT = 6;

const int OFFSET_X_BEKLEDINGSNAAM = 5;
const int OFFSET_Y_BEKLEDINGSNAAM = -5;

const double DEFAULT_LMIN = -1000.;
const double DEFAULT_LMAX = 1000.;

const double DEFAULT_ROUGHNESS = 1.0;

const int INDEX_COLUMN_L = 2;

const QString PROCESSOR_IKP = "Imaginair Kruinpunt";
const QString PROCESSOR_ITP = "Imaginair Teenpunt";
const QString PROCESSOR_TRUNC = "Truncator";
const QString PROCESSOR_RDP = "Ramer-Douglas-Peucker";
const QString PROCESSOR_BEKLEDING = "Bekleding overnemen uit XML";
const QString PROCESSOR_BREUKSTEEN = "Breuksteen verwijderen";
const QString PROCESSOR_ZETTING = "Zetting";
const QString PROCESSOR_COR_NEG = "Corrigeer negatieve helling";
const QString PROCESSOR_COR_GENTLESLOPE = "Corrigeer te flauw taluddeel";
const QString PROCESSOR_COR_STEEP = "Corrigeer te steil taluddeel";
const QString PROCESSOR_VALIDATOR = "Validator";
const QString PROCESSOR_RISKEER = "Aanpassen voor Riskeer";

enum PRFLColumnSeperator {ColumnSpaces=0, ColumnTabs=1};



#endif // CONSTANTS_H
