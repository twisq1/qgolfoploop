/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef SHAPEFILE_H
#define SHAPEFILE_H

#include <QString>
#include <QList>
#include "point.h"

#define DBF_RECORD_SIZE 10

class Shapefile
{
public:

    Shapefile();

    void open(const QString &filename);
    void addPoint(const Point* p, QString name);
    bool close();

private:

    uint32_t big_endian(uint32_t x);

#pragma pack(push, 1)           // Disable padding in structures. Without this directive the compiler would allign structure members to eight byte boundaries.
    struct {
        uint32_t file_code;     // Big endian
        uint32_t reserved[5] = {0};
        uint32_t file_length;   // Big endian
        uint32_t version;
        uint32_t shape_type;
        double xmin;
        double ymin;
        double xmax;
        double ymax;
        double zmin;
        double zmax;
        double mmin;
        double mmax;
    } shp_header;

    struct shp_point_type {
        uint32_t record_nr;     // Big endian
        uint32_t record_length; // Big endian
        uint32_t shape_type = 1;
        double x;
        double y;
    } shp_point;

    struct {
        uint32_t record_offset; // Big endian
        uint32_t record_length; // Big endian
    } shx_record;

    struct {
        uint8_t flags = 0x03;
        uint8_t year;
        uint8_t month;
        uint8_t day;
        uint32_t nrof_records;
        uint16_t header_size = sizeof(dbf_header);
        uint16_t record_size = DBF_RECORD_SIZE + 1;
        uint8_t reserved1[2] = {0};
        uint8_t incomplete = 0;
        uint8_t encryption = 0;
        uint8_t reserved2[12] = {0};
        uint8_t mdx_file = 0;
        uint8_t language = 0;
        uint8_t reserved3[2] = {0};
        char field_name[11] = {'I','D',0,0,0,0,0,0,0,0,0};
        char field_type = 'C';
        uint32_t reserved4 = 0;
        uint8_t field_length = DBF_RECORD_SIZE;
        uint8_t field_decimals = 0;
        uint16_t work_area = 0;
        uint8_t example = 0;
        char reserved5[10] = {0};
        uint8_t mdx_field = 0;
        uint8_t field_terminator = 0x0d;
    } dbf_header;

    struct {
        char deleted = ' ';
        char data[DBF_RECORD_SIZE] = {' '};
    } dbf_record;

#pragma pack(pop)

    QList<const Point*> m_points;
    QList<QString> m_names;
    QString m_filename;
};

#endif // SHAPEFILE_H
