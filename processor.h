/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QString>
#include "storable.h"
#include "point.h"

class Crosssection;

/****************************************************************************************************
 * This is the base class for all processors. You can subclass this class to make your own processor.
 * Every modification to the data should be done with a processor.
 * 2018-03-26 PT
 */

#define KEY_PREFIX  "GlobalEnable_"

class Processor : public Storable
{
public:
    struct SlopeLine{
        double x;
        double y;
        double slope;

        SlopeLine(const double _x, const double _y, const double _slope){
            x = _x;
            y = _y;
            slope = _slope;
        }
    };

    enum class Tristate {
        OFF,
        ON,
        GLOBAL
    };

    Processor(Crosssection* crs);
    virtual ~Processor();

    // This virtual function returns the name of the processor. The name is to be displayed to the user.
    virtual QString getName() const = 0;

    // Call this function to start the processor. The actual work will be done by the doProcess function of the subclass.
    void run();

    void reset();

    // Getter and setter functions
    Tristate getLocalEnable() const;
    void setLocalEnable(const Tristate enabled);
    bool isEnabled() const;
    bool getHasrun() const;
    bool getOk() const;
    QString getStatus() const;
    Crosssection *getCrs() const;

    // These are the getters and setters of the global enable flag
    bool getGlobalEnable() const;
    void setGlobalEnable(bool enable);

    virtual bool read(const QJsonObject &json);
    virtual bool write(QJsonObject &json) const;

    SlopeLine slope(const QList<Point *>& points);

protected:
    // These private setters are to be used by a subclass only.
    void setOk(bool ok);
    void setStatus(const QString &status);


    // Error sets status message and sets ok to false;
    void error(const QString &status);
    Crosssection* crs() const { return m_crs; }

private:
    // This virtual function has to be implemented by the subclass. The actual work is done in this function.
    virtual void doProcess() = 0;

    // Member variables
    Tristate m_enabled;
    bool m_hasrun;
    bool m_ok;
    QString m_status;
    Crosssection* m_crs;
};

#endif // PROCESSOR_H
