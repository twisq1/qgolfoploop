#-------------------------------------------------
#
# Project created by QtCreator 2018-03-06T10:23:08
#
#-------------------------------------------------

QT       += core gui xml
QMAKE_MAC_SDK = macosx10.15

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qgolfoploop
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    project.cpp \
    point.cpp \
    qprojectview.cpp \
    crosssection.cpp \
    processor.cpp \
    truncator.cpp \
    rdpparser.cpp \
    simpleQtLogger.cpp \
    errorhandler.cpp \
    qgoexception.cpp \
    ipointparser.cpp \
    validator.cpp \
    settingsdialog.cpp \
    doublespinitemdelegate.cpp \
    xmlitem.cpp \
    damobject.cpp \
    corrector.cpp \
    negativeslopecorrector.cpp \
    tooshallowcorrector.cpp \
    toosteepcorrector.cpp \
    exportdialog.cpp \
    storable.cpp \
    shapefile.cpp \
    riskeerprocessor.cpp \
    zettingparser.cpp \
    rubbleprocessor.cpp \
    itoeparser.cpp

HEADERS += \
        mainwindow.h \
    project.h \
    point.h \
    qprojectview.h \
    crosssection.h \
    constants.h \
    processor.h \
    truncator.h \
    rdpparser.h \
    simpleQtLogger.h \
    errorhandler.h \
    qgoexception.h \
    ipointparser.h \
    validator.h \
    settingsdialog.h \
    doublespinitemdelegate.h \
    xmlitem.h \
    damobject.h \
    corrector.h \
    negativeslopecorrector.h \
    tooshallowcorrector.h \
    toosteepcorrector.h \
    exportdialog.h \
    storable.h \
    shapefile.h \
    riskeerprocessor.h \
    zettingparser.h \
    rubbleprocessor.h \
    itoeparser.h


FORMS += \
        mainwindow.ui \
    settingsdialog.ui \
    exportdialog.ui

RESOURCES += \
    resources.qrc
