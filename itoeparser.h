/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef ITOEPARSER_H
#define ITOEPARSER_H

#include "processor.h"

#define GLOBAL_ITP_IGNORE_KEY       "itp_ignore"
#define GLOBAL_ITP_REGRESSION_KEY   "itp_regression"
#define GLOBAL_ITP_HEIGHT_KEY       "itp_height"
#define GLOBAL_ITP_TRUNCATE_KEY     "itp_truncate"
#define GLOBAL_ITP_FIXED_KEY        "itp_fixed"

class IToeParser : public Processor
{
    static const int DEFAULT_ITP_IGNORE = 2;
    static const int DEFAULT_ITP_REGRESSION = 4;
    static constexpr double DEFAULT_ITP_HEIGHT = 0.0;
    static const bool DEFAULT_ITP_TRUNCATE = false;
    static const bool DEFAULT_ITP_FIXED = false;

public:
    IToeParser(Crosssection* crs);

    virtual QString getName() const override;

    static void setGlobalIgnore(const int ignore);
    static void setGlobalRegression(const int regression);
    static void setGlobalTruncate(const bool truncate);
    static void setGlobalHeight(const double toe);
    static void setGlobalFixed(const bool fixed);
    static int getGlobalIgnore();
    static int getGlobalRegression();
    static double getGlobalHeight();
    static bool getGlobalTruncate();
    static bool getGlobalFixed();

private:
    virtual void doProcess() override;
};

#endif // ITOEPARSER_H
