/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef IPOINTPARSER_H
#define IPOINTPARSER_H

#include "processor.h"

#define GLOBAL_NUM_IGNORE_KEY       "ikb_num_ignore"
#define GLOBAL_NUM_REGRESSION_KEY   "ikb_num_regression"
#define GLOBAL_IKB_TRUNCATE         "ikb_truncate"

class IPointParser : public Processor
{
    static const int DEFAULT_NUM_IGNORE = 0;
    static const int DEFAULT_NUM_REGRESSION = 2;
    static const bool DEFAULT_IKB_TRUNCATE = false;

public:
    IPointParser(Crosssection* crs);

    virtual QString getName() const override;

    static void setGlobalNumIgnore(const int ignore);
    static void setGlobalNumRegression(const int regression);
    static void setGobalIkbTruncate(const bool truncate);
    static int getGlobalNumIgnore();
    static int getGlobalNumRegression();
    static bool getGlobalIkbTruncate();

private:
    virtual void doProcess() override;
};

#endif // IPOINTPARSER_H
