#include <QDate>
#include <QDataStream>
#include <cstring>
#include "errorhandler.h"
#include "simpleQtLogger.h"
#include "shapefile.h"
#include <QDataStream>


Shapefile::Shapefile()
{
    shp_header.file_code = big_endian(9994);
    shp_header.version = 1000;
    shp_header.shape_type = 1;
    shp_header.zmin = 0.0;
    shp_header.zmax = 0.0;
    shp_header.mmin = 0.0;
    shp_header.mmax = 0.0;
    shp_point.record_length = big_endian((sizeof(shp_point) - 8) / 2);
}

void Shapefile::open(const QString &filename)
{
    m_filename = filename;
}

void Shapefile::addPoint(const Point *p, QString name)
{
    if (m_points.length()) {
        if (p->x() < shp_header.xmin) shp_header.xmin = p->x();
        if (p->x() > shp_header.xmax) shp_header.xmax = p->x();
        if (p->y() < shp_header.ymin) shp_header.ymin = p->y();
        if (p->y() > shp_header.ymax) shp_header.ymax = p->y();
    } else {
        shp_header.xmin = shp_header.xmax = p->x();
        shp_header.ymin = shp_header.ymax = p->y();
    }
    m_points.append(p);
    m_names.append(name);
}

bool Shapefile::close()
{
    shp_header.file_length = big_endian((sizeof(shp_header) + m_points.length() * sizeof(shp_point)) / 2);

    QFile file(m_filename);
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream stream(&file);
        stream.writeRawData(reinterpret_cast<const char*>(&shp_header), sizeof(shp_header));

        int i = 1;
        for (const Point* p : m_points) {
            shp_point.record_nr = big_endian(i++);
            shp_point.x = p->x();
            shp_point.y = p->y();
            stream.writeRawData(reinterpret_cast<const char*>(&shp_point), sizeof(shp_point));
        }

    } else {
        ErrorHandler::warn("Kan shp bestand niet aanmaken.");
        return false;
    }

    // Now write shx file.
    QString shx_filename = m_filename.left(m_filename.length() - 3) + "shx";
    QFile shx_file(shx_filename);
    if (shx_file.open(QIODevice::WriteOnly)) {
        QDataStream shx_stream(&shx_file);
        shx_stream.writeRawData(reinterpret_cast<const char*>(&shp_header), sizeof(shp_header));

        shx_record.record_length = big_endian(10);
        uint32_t offset = 0x00000032;
        for (int i = 0; i < m_points.length(); i++) {
            shx_record.record_offset = big_endian(offset);
            offset += 14;
            shx_stream.writeRawData(reinterpret_cast<const char*>(&shx_record), sizeof(shx_record));
        }

    } else {
        ErrorHandler::warn("Kan shx bestand niet aanmaken.");
        return false;
    }

    // Now write dbf file.
    dbf_header.year = QDate::currentDate().year() - 1900;
    dbf_header.month = QDate::currentDate().month();
    dbf_header.day = QDate::currentDate().day();
    dbf_header.nrof_records = m_points.length();

    QString dbf_filename = m_filename.left(m_filename.length() - 3) + "dbf";
    QFile dbf_file(dbf_filename);
    if (dbf_file.open(QIODevice::WriteOnly)) {
        QDataStream dbf_stream(&dbf_file);
        dbf_stream.writeRawData(reinterpret_cast<const char*>(&dbf_header), sizeof(dbf_header));

        for (QString name : m_names) {
            // Convert string to dbf record;

            QByteArray bytes = name.toLatin1();
            if (bytes.length() < DBF_RECORD_SIZE) {
                bytes.append(DBF_RECORD_SIZE - bytes.length(), ' ');
            } else if (bytes.length() > DBF_RECORD_SIZE) {
                bytes = bytes.left(DBF_RECORD_SIZE);
            }
            std::strncpy(dbf_record.data, bytes.data(), DBF_RECORD_SIZE);
            dbf_stream.writeRawData(reinterpret_cast<char*>(&dbf_record), sizeof(dbf_record));
        }
        char terminator = 0x1A;
        dbf_stream.writeRawData(&terminator, 1);

    } else {
        ErrorHandler::warn("Kan dbf bestand niet aanmaken.");
        return false;
    }
    return true;
}

// Change order of bytes
uint32_t Shapefile::big_endian(uint32_t x)
{
    uint32_t b1 = x & 0xff000000;
    uint32_t b2 = x & 0x00ff0000;
    uint32_t b3 = x & 0x0000ff00;
    uint32_t b4 = x & 0x000000ff;
    return (b1 >> 24) | (b2 >> 8) | (b3 << 8) | (b4 << 24);
}
