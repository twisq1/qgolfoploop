/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef PROJECT_H
#define PROJECT_H

#include <QString>
#include <QTextStream>
#include <QDomDocument>
#include "crosssection.h"
#include "storable.h"
#include "constants.h"

class Project : public Storable
{
public:
    Project();
    virtual ~Project();

    // Override serialization functions
    virtual bool read(const QJsonObject &json) override;
    virtual bool write(QJsonObject &json) const override;

    bool isDirty() const;

    bool readFromPath(const QString &path);

    bool readSurfacelines(const QString &filename);
    bool readAlternativeSurfacelines(QTextStream& stream);
    bool readCharacteristicPoints(const QString &filename);
    bool readXmlFile(const QString &filename);    

    // Write all profiles to *.prfl files.
    void exportAllProfiles(const QString& path);

    Crosssection* getCurrentCrosssection() const;
    int getCurrent_crosssection_index() const;
    void setCurrent_crosssection_index(int current_crosssection_index);
    int numCrosssections() {return m_crosssections.count();}

    void runProcessorsOnAllCrosssections();
    void runProcessorsOnCurrentCrosssection();

    QString getRoughnessName(const double r);

    Crosssection *getCrosssection(const int index);
    Crosssection *getCrosssection(const QString label);

    bool containsDirtyCrosssections();
    void exportSingleProfile(const QString filename, Crosssection *crs = nullptr);

    void addProcessorMapping(const QString key, const int value);
    int getProcessorOrder(const QString key);
    QMap<QString, int> *getProcessorMapping() {return &m_processormapping;}
    QList<QString> getOrderedProcessorNames();


private:
    static bool show_processor_warning;

    QList<Crosssection*> m_crosssections;
    int m_current_crosssection_index;
    QMap<int, QString> m_roughnessmap;
    QMap<QString, int> m_processormapping;

    void clearData();
    Crosssection* getCrosssectionByProfileCode(const QString &profilecode);
    void writePrlf(QTextStream& stream, Crosssection* crs);
    void parseXmlSturingsParameters(QDomDocument& doc);
    void parseXmlRuwheden(QDomDocument& doc);
    void parseXmlDam(QDomDocument& doc);
    void parseXmlDamwand(QDomDocument& doc);
    void parseXmlZetting(QDomDocument &doc);
};

#endif // PROJECT_H
