/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef QEXCEPTION_H
#define QEXCEPTION_H

#include <QException>
#include <QString>

class QGOException : public QException
{
public:

    enum ExceptionType {
        FATAL,
        ERROR,
        WARNING,
        INFO
    };

    QGOException();
    QGOException(ExceptionType type, QString message, QString description);

    ExceptionType getType() const;
    void setType(const ExceptionType &value);

    QString getMessage() const;
    void setMessage(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);

private:
    ExceptionType type;
    QString message;
    QString description;
};

#endif // QEXCEPTION_H
