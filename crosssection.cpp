/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QtMath>
#include <QColor>
#include <QDebug>
#include <math.h>
#include "errorhandler.h"
#include "truncator.h"
#include "rdpparser.h"
#include "ipointparser.h"
#include "itoeparser.h"
#include "rubbleprocessor.h"
#include "zettingparser.h"
#include "validator.h"
#include "negativeslopecorrector.h"
#include "tooshallowcorrector.h"
#include "toosteepcorrector.h"
#include "riskeerprocessor.h"
#include "simpleQtLogger.h"
#include "constants.h"
#include "processor.h" // for the compare function
#include "crosssection.h"

Crosssection::Crosssection()
{
    m_dirty = false;
    m_truncated = false; //geeft aan of het profiel gestript is van overbodige punten

    m_processors.append(new IPointParser(this));
    m_processors.append(new IToeParser(this));
    m_processors.append(new Truncator(this));
    m_processors.append(new RDPparser(this));
    m_processors.append(new RubbleProcessor(this));
    m_processors.append(new ZettingParser(this));
    m_processors.append(new NegativeSlopeCorrector(this));
    m_processors.append(new TooShallowCorrector(this));
    m_processors.append(new TooSteepCorrector(this));
    m_validator = new Validator(this);
    m_processors.append(m_validator);
    m_processors.append(new RiskeerProcessor(this));
    m_allProcessorState = Qt::PartiallyChecked;

    m_zetting = -1.0;
    m_epsilon = -1.0;
    m_skip = -1;
    m_regression = -1;
    m_highlightPoint = nullptr;
}

Crosssection::~Crosssection()
{
    clear();

    // Delete all processors.
    foreach (Processor* processor, m_processors) {
        delete processor;
    }
    m_processors.clear();
}

// Clear all data
void Crosssection::clear()
{
    // Delete all original points.
    foreach(Point* p, m_original_points) {
        delete p;
    }
    m_original_points.clear();

    // Delete all surface points.
    foreach(Point* p, m_surface_points) {
        delete p;
    }
    m_surface_points.clear();
}

QList<Point *> Crosssection::getOriginalSurfacePoints() const
{
    return m_original_points.values();
}

QList<Point *> Crosssection::getSurfacePoints(bool all) const
{
    QList<Point*> result;
    foreach(Point *p, m_surface_points){
        if(all || !p->truncated()) result.append(p);
    }
    return result;
}

bool Crosssection::isInvalid() const
{
    for (Processor* processor : m_processors) {
        if (!processor->getOk()) return true;
    }
    return false;
}

void Crosssection::setName(const QString name)
{
    if (name != m_name) {
        if (m_original_name.isEmpty()) m_original_name = name;
        m_name = name;
    }
}

void Crosssection::addSurfacePoint(Point *const p)
{
    long key;
    if (m_surface_points.size()) {
        key = p->lmm(getReferencePoint());
    } else {
        key = 0;
    }
    m_surface_points.insert(key, p);
}

// Move existing point to a new location based on L,Z.
// In fact, remove the old point and create a new one.
Point *Crosssection::moveSurfacePoint(Point* p, double newL, double newZ)
{
    // If point already exist at this position, shift a mm to the right.
    while (getSurfacePoint(Point::toLong(newL))) newL += 0.001;

    long oldkey = p->lmm(getReferencePoint());
    Point* newpoint = new Point(*p);
    Point newpos = LZtoRDW(QPointF(newL, newZ));
    newpoint->setX(newpos.x());
    newpoint->setY(newpos.y());
    newpoint->setZ(newpos.z());

    // Move point
    removeSurfacePoint(oldkey);
    delete (p);
    addSurfacePoint(newpoint);

    return newpoint;
}

void Crosssection::setImaginaryPoint(const double l, const double z, Point::CPID type, Point::CPID realPointCode)
{
    Point* realPoint = getCharacteristicPoint(realPointCode);
    double r = 1.0;
    if (realPoint != nullptr) {
        r = realPoint->roughness();
    }
    Point ip = LZtoRDW(QPointF(l,z));
    Point *current_ip = getCharacteristicPoint(type);

    if(current_ip != nullptr) {
        removeSurfacePoint(current_ip->lmm(getReferencePoint()));
        delete current_ip;
    }
    Point *p = new Point(ip.x(), ip.y(), ip.z());
    p->setRoughness(r);
    p->setId(type);
    addSurfacePoint(p);
}

double Crosssection::lmin()
{
    QList<Point*> points = getSurfacePoints();
    if (points.size()) return points.at(0)->l(m_surface_points.first());
    else return 1e9;
}

double Crosssection::lmax()
{
    QList<Point*> points = getSurfacePoints();
    if (points.size()){
        return points.at(points.count()-1)->l(m_surface_points.first());
    }
    return -1e9;
}

double Crosssection::zmin()
{
    double result = 1e9;
    foreach(Point* p, m_surface_points.values()) {
        if (!p->truncated() && p->z() < result) result = p->z();
    }
    return result;
}

double Crosssection::zmax()
{
    double result = -1e9;
    foreach (Point* p, m_surface_points.values()) {
        if (!p->truncated() && p->z() > result) result = p->z();
    }
    return result;
}

bool Crosssection::addRoughness(const Point pstart, const Point pend, const double roughness)
{
    bool inArea = false;
    for (Point* p : getSurfacePoints()) {
        if (pstart.isSameLocation(p)) {
            if (p->getId() == Point::CPID::NONE) p->setId(Point::CPID::OVERGANG_RUWHEID);
            inArea = true;
        } else if (inArea && pend.isSameLocation(p)) {
            if (p->getId() == Point::CPID::NONE) p->setId(Point::CPID::OVERGANG_RUWHEID);
            inArea = false;
            return true;    // Return true, because all points have been found.
        }
        if (inArea) p->setRoughness(roughness);
    }
    return false; // Return false because not all two points have been found.
}

// Get the first point of the cross section. All distances are calculated from here.
Point *Crosssection::getReferencePoint() const
{
    if (m_original_points.size()) {
        return m_original_points.first();
    } else if (m_surface_points.size()) {
        return m_surface_points.first();
    }
    return nullptr;
}

Point *Crosssection::getSurfacePoint(long lmm)
{
    return m_surface_points.value(lmm, nullptr);
}

Point *Crosssection::getCharacteristicPoint(Point::CPID id) const
{
    for (Point* p : m_surface_points.values()) {
        if (p->getId() == id) return p;
    }
    for (Point* p : m_original_points.values()) {
        if (p->getId() == id) return p;
    }
    return nullptr;
}

QList<Point *> Crosssection::getCharacteristicPoints()
{
    QList<Point*> list;
    for (Point* p : m_surface_points.values()) {
        if (p->getId() != Point::NONE) list.append(p);
    }
    return list;
}

// Calculate the height at a specific position of the cross section.
// Use interpolation.
double Crosssection::getZAt(const double l)
{
    if (m_surface_points.count() < 2) return 0.0;
    long lmm = Point::toLong(l);
    long k1 = -1;
    foreach (long k2, m_surface_points.keys()) {
        if (k1 >= 0) {
            if ((lmm >= k1) && (lmm <= k2)) {
                double z1 = m_surface_points.find(k1).value()->z();
                double z2 = m_surface_points.find(k2).value()->z();
                return z1 + (l - k1) / (k2 - k1) * (z2 - z1);
            }
        }
        k1 = k2;
    }
    return 0.0;
}

double Crosssection::getZSlope(Point *p1, Point *p2)
{
    double dl = p2->l(p1);
    double dz = p2->z() - p1->z();
    if((dl > 0.001) || (dl < -0.001)) return dz / dl;
    return 1e9;
}

Point *Crosssection::getPointLeftOf(Point *point)
{
    double dlmin = 1e9;
    double lref = point->l(getReferencePoint());
    Point *result = nullptr;

    foreach(Point *p, getSurfacePoints()){

        double l = p->l(getReferencePoint());
        double dl = lref - l;
        if(dl > 0 && dl < dlmin){
            result = p;
            dlmin = dl;
        }
    }
    return result;
}

QList<Point *> Crosssection::getPointsLeftOf(Point *edge, int num_skip, int num_points)
{
    long ledge = edge->lmm(getReferencePoint());

    for(int i=0; i<getSurfacePoints().count(); i++){
        Point *p = getSurfacePoints().at(i);
        if(p->lmm(getReferencePoint()) == ledge){
            int istart = i - num_skip - num_points + 1;
            if(istart < 0){
                //L_DEBUG("Het startpunt van de regressielijn wordt voor het eerste punt gezocht, gecorrigeerd door eerste punt te gebruiken");
                istart = 0;
            }
            return getSurfacePoints().mid(istart, num_points);
        }
    }
    //L_DEBUG("Het opgegeven punt is niet gevonden.");
    QList<Point *> empty;
    return empty;
}

QList<Point *> Crosssection::getPointsRightOf(Point *edge, int num_skip, int num_points)
{
    long ledge = edge->lmm(getReferencePoint());

    for(int i=0; i < getSurfacePoints().count(); i++){
        Point *p = getSurfacePoints().at(i);
        if(p->lmm(getReferencePoint()) == ledge){
            int istart = i + num_skip;
            if  ((istart + num_points) > getSurfacePoints().count()) {
                //L_DEBUG("Het startpunt van de regressielijn ligt teveel naar rechts.");
                num_points = getSurfacePoints().count() - istart;
            }
            return getSurfacePoints().mid(istart, num_points);
        }
    }
    //L_DEBUG("Het opgegeven punt is niet gevonden.");
    QList<Point *> empty;
    return empty;
}

double Crosssection::getZMax(Point *pleft, Point *pright)
{
    double zmax = -1e9;
    double lleft = pleft ? pleft->l(getReferencePoint()) : m_surface_points.first()->l(getReferencePoint());
    double lright = pright ? pright->l(getReferencePoint()) : m_surface_points.last()->l(getReferencePoint());
    foreach (Point *p, m_surface_points) {
        double l = p->l(getReferencePoint());
        if((lleft <= l) && (l<=lright)){
            if(p->z() > zmax){
                zmax = p->z();
            }
        }
    }
    return zmax;
}

void Crosssection::applyProcessors(QMap<QString, int> *processororder)
{
    //create a sorted map with the order of processors
    QMap<int, QString> ordered;
    QMap<QString, int>::const_iterator i = processororder->constBegin();
    while (i != processororder->constEnd()) {
        ordered[i.value()] = i.key();
        ++i;
    }

    //execute processors
    QMap<int, QString>::const_iterator it = ordered.constBegin();
    while (it != ordered.constEnd()) {
        foreach (Processor *processor, m_processors) {
            if(processor->getName() == it.value()){
                //L_DEBUG("Executing processor " + processor->getName() + " order " + QString::number(it.key()));

                processor->run();
                if(!processor->getOk()){
                    //todo, netjes opvangen
                    //L_DEBUG(processor->getStatus());
                }
                setDirty(true);
            }
        }
        ++it;
    }

    emit layoutChanged();
}

int Crosssection::getNumExecutedProcessors()
{
    int ic = 0;
    foreach (Processor *processor, m_processors) {
        if(processor->getHasrun()) ic++;
    }
    return ic;
}


QList<Processor *> Crosssection::getProcessors() const
{
    return m_processors;
}

Processor *Crosssection::getProcessor(const QString name) const
{
    for (Processor* p : m_processors) {
        if (p->getName() == name) return p;
    }
    return nullptr;
}

Point Crosssection::LZtoRDW(QPointF LZ)
{
    Point p;

    double l = LZ.x();
    Point *left = m_original_points.first();
    Point *right = m_original_points.last();
    if (left && right) {
        double diff = right->l(left);
        if (diff > 0) {
            p.setX(left->x() + (right->x() - left->x()) * (l / diff) );
            p.setY(left->y() + (right->y() - left->y()) * (l / diff) );
        } else {
            p.setX(left->x());
            p.setY(left->y());
        }
        p.setZ(LZ.y());
    }
    return p;
}

void Crosssection::removeSurfacePoint(long lmm)
{
    if (!m_surface_points.remove(lmm)) {
        L_ERROR("Internal error. Unable to remove point with key: " + QString::number(lmm));
    }
}

void Crosssection::validate()
{
    if (m_validator){
        QString oldstatus = m_validator->getStatus();
        m_validator->run();
        if(m_validator->getStatus() != oldstatus){
            emit validationStateChanged(m_validator->getStatus());
        }
    }
}

bool Crosssection::isDirty() const
{
    return m_dirty;
}

void Crosssection::setDirty(bool dirty)
{
    m_dirty = dirty;
}


double Crosssection::getDirection() const
{    
    double angle = -1.0;
    if (m_surface_points.size() > 1) {
        Point *p1 = getCharacteristicPoint(Point::KRUIN_BUITENTALUD);
        Point *p2 = getCharacteristicPoint(Point::KRUIN_BINNENTALUD);

        if(p1 == nullptr || p2 == nullptr){
            QGOException ex(QGOException::ExceptionType::WARNING, "Crosssection error", "Het dwarsprofiel mist binnen- en/of buitenkruinpunt en kan daarmee de dijknormaal niet berekenen.");
            ErrorHandler::exception(ex);
            return false;
        }

        double dx = p2->x() - p1->x();
        double dy = p2->y() - p1->y();
        double radials = qAtan2(dx, dy);
        angle = qRadiansToDegrees(radials);
        if (angle < 0.0) angle += 360.0;
    }
    return angle;
}

long Crosssection::voorland() const
{
    long voorland = 0L;
    Point* p = getCharacteristicPoint(Point::CPID::IMAGINAIR_TEENPUNT);
    if (!p) {
        p = getCharacteristicPoint(Point::CPID::TEEN_DIJK_BUITENWAARTS);
    }
    if (p) {
        voorland = p->lmm(getReferencePoint());
    }
    return voorland;
}

void Crosssection::addRemark(const QString remark)
{
    if (!m_remarks.isEmpty()) m_remarks += "\n";
    m_remarks += remark;
}

int Crosssection::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    else return getSurfacePoints().count() - 1;     // Number of segments is one less than number of points.
}

int Crosssection::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    else return 8;
}

QVariant Crosssection::data(const QModelIndex &index, int role) const
{
    if (role == Qt::BackgroundRole) {
        if (index.row() > getSurfacePoints().count()) {
            L_ERROR("Table model is messed up. Row doesn't exist anymore. " + QString::number(index.row()));
            return QVariant();
        }
        Point* p2 = getSurfacePoints().at(index.row() + 1);
        Point* base = getReferencePoint();

        if (p2 == m_highlightPoint) return QVariant(QColor(COLOR_SELECTED_POINT));
        else if (p2->lmm(base) <= voorland()) return QVariant(QColor(COLOR_VOORLAND_TABLE));
        else return QVariant();
    }
    if (role == Qt::ForegroundRole) {
        if (index.row() > getSurfacePoints().count()) {
            L_ERROR("Table model is messed up. Row doesn't exist anymore. " + QString::number(index.row()));
            return QVariant();
        }
        Point* p = getSurfacePoints().at(index.row() + 1);
        if (p->getId() != Point::CPID::NONE) return QVariant(QColor(COLOR_CHARACTERISTIC_POINT));
        else return QVariant();
    }
    if ((role == Qt::DisplayRole) || (role == Qt::EditRole)) {
        if (index.row() > getSurfacePoints().count()) {
            L_ERROR("Table model is messed up. Row doesn't exist anymore. " + QString::number(index.row()));
            return QVariant();
        }
        Point* p1 = getSurfacePoints().at(index.row());
        Point* p2 = getSurfacePoints().at(index.row() + 1);
        Point* base = getReferencePoint();

        double lstart = p1->l(base);
        double zstart = p1->z();
        double l = p2->l(base);
        double z = p2->z();
        double dl = l-lstart;
        double dz = z - zstart;
        double r = p1->roughness();
        double angle = 1e6; //avoid too large angles if the line is horizontal
        if(qAbs(z-zstart)>=1e-5){
            angle = (l - lstart) / (z - zstart);
        }
        double value;
        switch (index.column()) {
        case 0:
            value = lstart;
            break;
        case 1:
            value = zstart;
            break;
        case 2:
            value = l;
            break;
        case 3:
            value = z;
            break;
        case 4:
            value = dl;
            break;
        case 5:
            value = dz;
            break;
        case 6:
            value = r;
            break;
        case 7:
            value = angle;
            break;
        default:
            L_ERROR("Internal error. Table dimensions are messed up.");
            value = 0.0;
            break;
        }
        return QVariant(QString::number(value, 'f', 3).replace('.', ','));
    }
    return QVariant();
}

QVariant Crosssection::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ((role == Qt::DisplayRole) && (orientation == Qt::Orientation::Horizontal)) {
        QString header;
        switch (section) {
        case 0:
            header = "L-start";
            break;
        case 1:
            header = "Z-start";
            break;
        case 2:
            header = "L";
            break;
        case 3:
            header = "Z";
            break;
        case 4:
            header = "dL";
            break;
        case 5:
            header = "dZ";
            break;
        case 6:
            header = "r";
            break;
        case 7:
            header = "1:n";
            break;
        default:
            L_ERROR("Internal error. Table dimensions are messed up.");
            break;
        }
        return QVariant(header);
    }
    return QVariant();
}

Qt::ItemFlags Crosssection::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = Qt::ItemIsSelectable;
    if ((index.column() == 0) || (index.column() == 1) || (index.column() == 6)) flags |= Qt::ItemIsEditable | Qt::ItemIsEnabled;
    return flags;
}

bool Crosssection::setData(const QModelIndex &index, const QVariant &value, int role)
{       
    if (role != Qt::EditRole) return false;

    if (index.row() > getSurfacePoints().count()) {
        L_ERROR("Table model is messed up. Row doesn't exist anymore. " + QString::number(index.row()));
        return false;
    }
    Point* p1 = getSurfacePoints().at(index.row());
    switch(index.column()) {
    case 0:
        moveSurfacePoint(p1, value.toDouble(), p1->z());
        break;
    case 1:
        p1->setZ(value.toDouble());
        break;
    case 6:
        p1->setRoughness(value.toDouble());
        break;
    default:
        L_ERROR("Internal error. Editing an uneditable cell.");
        return false;
    }
    validate();
    dataChanged(index, index);
    return true;
}

void Crosssection::deleteAllSurfacePoints()
{
    //delete all pointers
    foreach(Point *p, m_surface_points){
        delete p;
    }
    m_surface_points.clear();
}

void Crosssection::reset()
{
    deleteAllSurfacePoints();

    //and build again based on the original points
    Point *base = getReferencePoint();
    foreach(Point *p, m_original_points){
        Point *np = new Point(p);
        m_surface_points.insert(np->lmm(base), np);
    }
    m_truncated = false;
    m_dirty = false;

    // reset name
    m_name = m_original_name;

    //reset processors
    for (Processor* p: m_processors) {
        p->reset();
    }
}

// Make a deep copy of all points.
void Crosssection::copySurfaceToOriginalPoints()
{
    m_original_points.clear();
    for (long key: m_surface_points.keys()) {
        Point* surface = m_surface_points.value(key);
        Point* original = new Point(surface);
        m_original_points.insert(key, original);
    }
}

bool Crosssection::read(const QJsonObject &json)
{
    clear();

    if (json.contains("zetting")) {
        m_zetting = json["zetting"].toDouble();
    }
    if (    json.contains("name") &&
            json.contains("dirty") &&
            json.contains("epsilon") &&
            json.contains("skip") &&
            json.contains("regression") &&
            json.contains("remarks") &&
            json.contains("original_points") &&
            json.contains("surface_points") &&
            json.contains("processors") &&
            json.contains("truncated")) {
        m_name = json["name"].toString();
        m_original_name = json["original_name"].toString();
        m_dirty = json["dirty"].toBool();
        m_truncated = json["truncated"].toBool();
        m_epsilon = json["epsilon"].toDouble();
        m_skip = json["skip"].toInt();
        m_regression = json["regression"].toInt();
        m_remarks = json["remarks"].toString();

        bool ok = true;

        // Read original points
        QJsonArray originals = json["original_points"].toArray();
        for (int i = 0; ok && (i < originals.size()); i++) {
            QJsonObject entry = originals[i].toObject();
            Point* p = new Point();
            ok = p->read(entry);
            long key = entry["lmm"].toString().toLong();
            m_original_points[key] = p;
        }

        // Read surface points
        QJsonArray surface = json["surface_points"].toArray();
        for (int i = 0; ok && (i < surface.size()); i++) {
            QJsonObject entry = surface[i].toObject();
            Point* p = new Point();
            ok = p->read(entry);
            long key = entry["lmm"].toString().toLong();
            m_surface_points[key] = p;
        }

        // Read processors
        for (QJsonValue record : json["processors"].toArray()) {
            if (!ok) break;
            QJsonObject entry = record.toObject();
            Processor* processor = getProcessor(entry["name"].toString());
            if (processor) {
                ok = processor->read(entry);
            }
        }

        // Read dam data
        if (ok && json.contains("dam") && json.contains("damwand")) {
            QJsonObject dam = json["dam"].toObject();
            m_dam.read(dam);
            QJsonObject damwand = json["damwand"].toObject();
            m_damwand.read(damwand);
        }

        return ok;
    } else {
        ErrorHandler::info("Fout in bestand");
        return false;
    }
}

bool Crosssection::write(QJsonObject &json) const
{
    json["name"] = m_name;
    json["original_name"] = m_original_name;
    json["dirty"] = m_dirty;
    json["truncated"] = m_truncated;
    json["epsilon"] = m_epsilon;
    json["skip"] = m_skip;
    json["regression"] = m_regression;
    json["remarks"] = m_remarks;
    json["zetting"] = m_zetting;

    // Write original points
    QJsonArray originals;
    for (long key: m_original_points.keys()) {
        Point* p = m_original_points[key];
        QJsonObject entry;
        entry["lmm"] = QString::number(key);
        p->write(entry);
        originals.append(entry);
    }
    json["original_points"] = originals;

    // Write surface points
    QJsonArray surface;
    for (long key: m_surface_points.keys()) {
        Point* p = m_surface_points[key];
        QJsonObject entry;
        entry["lmm"] = QString::number(key);
        p->write(entry);
        surface.append(entry);
    }
    json["surface_points"] = surface;

    // Write processor data
    QJsonArray processors;
    for (Processor* processor : m_processors) {
        QJsonObject entry;
        processor->write(entry);
        processors.append(entry);
    }
    json["processors"] = processors;

    // Write dam data
    QJsonObject dam;
    m_dam.write(dam);
    json["dam"] = dam;
    QJsonObject damwand;
    m_damwand.write(damwand);
    json["damwand"] = damwand;

    return true;
}

double Crosssection::getZetting() const
{
    return m_zetting;
}

void Crosssection::setZetting(double zetting)
{
    m_zetting = zetting;
}

void Crosssection::setActivePoint(Point *value)
{
    if (m_highlightPoint != value) {
        m_highlightPoint = value;

    }
}

Point *Crosssection::getActivePoint() const
{
    return m_highlightPoint;
}

Qt::CheckState Crosssection::getAllProcessorState() const
{
    return m_allProcessorState;
}

void Crosssection::setAllProcessorState(const Qt::CheckState &allProcessorState)
{
    m_allProcessorState = allProcessorState;
}
