/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "doublespinitemdelegate.h"
#include <QDoubleSpinBox>

//#include "simpleQtLogger.h"

DoubleSpinItemDelegate::DoubleSpinItemDelegate(QObject *parent) : QItemDelegate(parent)
{

}

QWidget *DoubleSpinItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);

    double lmin = DEFAULT_LMIN;
    double lmax = DEFAULT_LMAX;
    //try to set the min and max according to the neighbouring rows
    if(index.column()==0 && index.row()>0 && index.row()){
        QModelIndex idxmin = index.sibling(index.row()-1, index.column());
        QModelIndex idxmax = index.sibling(index.row()+1, index.column());

        if(index.row() == index.model()->rowCount()-1){
            idxmax = index.sibling(index.row(), INDEX_COLUMN_L);
        }
        lmin = idxmin.model()->data(idxmin, Qt::EditRole).toString().replace(',','.').toDouble();
        lmax = idxmax.model()->data(idxmax, Qt::EditRole).toString().replace(',','.').toDouble();

        //L_DEBUG(QString("Setting editor delegate to lmin: %1 lmax: %2").arg(lmin,5,'f',3).arg(lmax,5,'f', 3));
    }

    QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
    editor->setMinimum(lmin);
    editor->setMaximum(lmax);
    editor->setDecimals(3);
    editor->setSingleStep(0.1);
    return editor;
}

void DoubleSpinItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    double value = index.model()->data(index, Qt::EditRole).toString().replace(',','.').toDouble();

    QDoubleSpinBox *dspinBox = static_cast<QDoubleSpinBox*>(editor);
    dspinBox->setValue(value);
}

void DoubleSpinItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QDoubleSpinBox *dspinBox = static_cast<QDoubleSpinBox*>(editor);
    dspinBox->interpretText();
    double value = dspinBox->value();
    model->setData(index, value, Qt::EditRole);
}

void DoubleSpinItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);

    editor->setGeometry(option.rect);
}

