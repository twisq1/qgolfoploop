/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>

namespace Ui {
class ExportDialog;
}

class ExportDialog : public QDialog
{
    Q_OBJECT

public:
    enum ExportWhat {
        NOTHING,
        SINGLE_PROFILE,
        ALL_PROFILES,
        SINGLE_IMAGE,
        ALL_IMAGES,
        SHAPEFILE,
        XML_FILE
    };

    explicit ExportDialog(QWidget *parent = 0);
    ~ExportDialog();

    ExportWhat getExportWhat() const;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::ExportDialog *ui;

    ExportWhat m_what;
};

#endif // EXPORTDIALOG_H
