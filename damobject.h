/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef DAMOBJECT_H
#define DAMOBJECT_H

#include <QString>
#include "storable.h"

class DamObject : public Storable
{
public:
    DamObject();
    bool isValid() const;

    void set(int type, const QString &name, double height);

    int type() const;
    QString name() const;
    double height() const;

    virtual bool read(const QJsonObject &json) override;
    virtual bool write(QJsonObject &json) const override;

private:
    bool m_valid;
    int m_type;
    QString m_name;
    double m_height;
};

#endif // DAMOBJECT_H
