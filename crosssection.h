/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef CROSSSECTION_H
#define CROSSSECTION_H

#include <QList>
#include <QMap>
#include <QString>
#include <QObject>
#include <QAbstractTableModel>

#include "point.h"
#include "damobject.h"
#include "processor.h"
#include "storable.h"

// A cross section contains all the information of a profile or cross section.
// i.e. surface line points, characteristic points, name and comments.

class Crosssection : public QAbstractTableModel, Storable
{
    Q_OBJECT

public:
    Crosssection();
    // The destructor frees all the memory associated to this cross section.
    virtual ~Crosssection() override;

    QList<Point*> getOriginalSurfacePoints() const;
    QList<Point*> getSurfacePoints(bool all = false) const;

    int numSurfacePoints() const {return m_surface_points.count();}

    bool isInvalid() const;
    bool isTrunacted() {return m_truncated;}
    void setTruncated(const bool t) {m_truncated=t;}

    void setName(const QString name);
    QString getName() {return m_name;}

    void addSurfacePoint(Point *const p);
    Point* moveSurfacePoint(Point* p, double newL, double newZ);
    void setImaginaryPoint(const double l, const double z, Point::CPID type, Point::CPID realPointCode);

    double lmin();
    double lmax();
    double zmin();
    double zmax();

    bool addRoughness(const Point pstart, const Point pend, const double roughness);

    // Get the first point of the cross section. All distances are calculated from here.
    Point *getReferencePoint() const;

    // Return the surfacepoint at a specific l-distance.
    Point* getSurfacePoint(long lmm);

    // Get a specific characteristic point.
    Point* getCharacteristicPoint(Point::CPID id) const;
    QList<Point*> getCharacteristicPoints();

    // Calculate the height at a specific position of the cross section.
    double getZAt(const double l);

    // Get slope between two points
    double getZSlope(Point *p1, Point *p2);

    // Helper function
    Point* getPointLeftOf(Point *p);
    QList<Point *> getPointsLeftOf(Point *p, int num_skip, int num_points);
    QList<Point *> getPointsRightOf(Point *p, int num_skip, int num_points);
    double getZMax(Point *pleft = nullptr, Point *pright = nullptr);

    // Apply the active processors
    void applyProcessors(QMap<QString, int> *processororder);
    //return the number of processor with has_run state = true
    int getNumExecutedProcessors();

    // Get all processors
    QList<Processor *> getProcessors() const;

    // Get processor with name
    Processor* getProcessor(const QString name) const;

    // Convert LZ to RDW coordinates.
    Point LZtoRDW(QPointF LZ);

    // Remove a point from the map.
    // The actual point will not be deleted!
    void removeSurfacePoint(long lmm);

    // Validate the profile.
    void validate();

    bool isDirty() const;
    void setDirty(bool dirty);

    // Return the direction of the profile in degrees 0=N, 90=E, 180=S, 270-W.
    double getDirection() const;

    // Getters and setters for RDP and IKP parameters.
    bool hasRDPepsilon() const { return m_epsilon >= 0; }
    bool hasIKPnumskip() const { return m_skip >= 0; }
    bool hasIKPnumregression() const { return m_regression >= 0; }
    bool hasZetting() const { return m_zetting >= 0.0; }
    double getRDPepsilon() const { return m_epsilon; }
    int getIKPnumskip() const { return m_skip; }
    int getIKPnumregression() const { return m_regression; }
    void setRDPepsilon(double epsilon) { m_epsilon = epsilon; }
    void setIKPnumskip(int skip) { m_skip = skip; }
    void setIKPnumregression(int regression) { m_regression = regression; }

    long voorland() const;

    QString getRemark() {return m_remarks;}
    void setRemark(const QString remark) {m_remarks = remark;}
    void addRemark(const QString remark);

    // Overrides to implement Table Model.
    int rowCount(const QModelIndex & parent) const override;
    int columnCount(const QModelIndex & parent) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    Qt::ItemFlags flags(const QModelIndex & index) const override;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;

    void deleteAllSurfacePoints();
    void reset();
    void copySurfaceToOriginalPoints();

    DamObject* getDam() { return &m_dam; }
    DamObject* getDamwand() { return &m_damwand; }

    virtual bool read(const QJsonObject &json) override;
    virtual bool write(QJsonObject &json) const override;

    double getZetting() const;
    void setZetting(double zetting);

    void setActivePoint(Point *value);
    Point *getActivePoint() const;

    Qt::CheckState getAllProcessorState() const;
    void setAllProcessorState(const Qt::CheckState &allProcessorState);

signals:
    void validationStateChanged(QString status);

private:
    bool m_truncated;
    bool m_dirty;
    QString m_name;
    QString m_original_name;
    double m_epsilon;
    int m_skip;
    int m_regression;
    QString m_remarks;
    double m_zetting;
    Qt::CheckState m_allProcessorState;

    QMap<long, Point*> m_original_points;   // Key is distance to reference point in millimeters.
    QMap<long, Point*> m_surface_points;    // Key is distance to reference point in millimeters.
    QList<Processor*> m_processors;
    Processor* m_validator;
    DamObject m_dam;
    DamObject m_damwand;
    Point* m_highlightPoint;

    void clear();
};

#endif // CROSSSECTION_H
