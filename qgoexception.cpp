/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "qgoexception.h"

QGOException::QGOException()
{
}

QGOException::QGOException(QGOException::ExceptionType type, QString message, QString description)
{
    setType(type);
    setMessage(message);
    setDescription(description);
}

QGOException::ExceptionType QGOException::getType() const
{
    return type;
}

void QGOException::setType(const ExceptionType &value)
{
    type = value;
}

QString QGOException::getMessage() const
{
    return message;
}

void QGOException::setMessage(const QString &value)
{
    message = value;
}

QString QGOException::getDescription() const
{
    return description;
}

void QGOException::setDescription(const QString &value)
{
    description = value;
}
