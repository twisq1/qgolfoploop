/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "simpleQtLogger.h"
#include "crosssection.h"
#include "negativeslopecorrector.h"
#include "constants.h"

NegativeSlopeCorrector::NegativeSlopeCorrector(Crosssection *crs)
    : Corrector(crs)
{
}

QString NegativeSlopeCorrector::getName() const
{
    return PROCESSOR_COR_NEG;
}

void NegativeSlopeCorrector::doProcess()
{
    bool correction = false;

    // Check voorland.
    Point* ref = crs()->getReferencePoint();
    long voorland_lmm = crs()->voorland();
    if (voorland_lmm == 0) {
        L_WARN("Profiel heeft geen voorland.");
        setOk(false);
        setStatus("Profiel heeft geen voorland.");
    }

    // Get parameters.
    double almost_horizontal = 1.0 / getGlobalAlmostHorizontal();

    Point* p1 = nullptr;
    QList<Point*> pointsBefore = crs()->getSurfacePoints();

    // Walk through profile, segment by segment. Each time the segment between p1 and p2 is processed.
    for (int n = 0; n < pointsBefore.length(); n++) {
        Point* p2 = pointsBefore.at(n);
        if (p1) {

            // check only dam part.
            if (p2->lmm(ref) > voorland_lmm) {

                // Calculate slope of current segment and do the checks.
                double slope = crs()->getZSlope(p1, p2);

                if (slope < 0.0) {
                    correction = true;

                    // First create a point to the right that has the same Z as p1.
                    Line secant(0, p1->z());   // A line with rc=0 (horizontal) and y0=z.
                    Point* q1 = nullptr;
                    Point* q2 = nullptr;
                    Point* new_left = nullptr;
                    Point* new_right = nullptr;
                    int ix = -1;
                    bool hit = false;
                    QPointF right_intersection;
                    QPointF left_intersection;

                    // Find the first segment to the right, that crosses Z of p1.
                    for (int i = 0; !hit && (i < pointsBefore.length()); i++) {
                        q2 = pointsBefore.at(i);
                        if (ix < 0) {
                            if (q1 && (q1->lmm(ref) >= p2->lmm(ref))) ix = i;   // We have found the first segment right to p2.
                        }
                        if (ix >= 0) {
                            QPointF qf1(q1->l(ref), q1->z());
                            QPointF qf2(q2->l(ref), q2->z());
                            right_intersection = getIntersection(qf1, qf2, secant, &hit);
                            if (hit) {
                                // We have a hit within the current segment.
                                Point candidate = crs()->LZtoRDW(right_intersection);
                                new_right = crs()->getSurfacePoint(candidate.lmm(ref));
                                if (new_right) {
                                    new_right->setTruncated(false);
                                } else {
                                    new_right = new Point(candidate);
                                    crs()->addSurfacePoint(new_right);
                                }
                            }
                        }
                        q1 = q2;
                    }
                    if (hit) {

                        // Now, a point to the right has been added at 'right intersection'.
                        // Draw a line 'almost horizontal' to the left and
                        // Add a point at the left intersection between the line and the profile

                        Line helperLine = getLine(right_intersection, almost_horizontal);
                        q1 = nullptr;
                        q2 = nullptr;
                        hit = false;

                        // Walk through the line segments to the left.
                        for (int i = ix - 1; !hit && (i >= 0); i--) {
                            q1 = pointsBefore.at(i);

                            // Check if we have found a segment left of p1.
                            if (q2 && (q2->lmm(ref) <= p1->lmm(ref))) {

                                // Now check if we have an intersection with this line segment.
                                QPointF qf1(q1->l(ref), q1->z());
                                QPointF qf2(q2->l(ref), q2->z());
                                left_intersection = getIntersection(qf1, qf2, helperLine, &hit);
                                if (hit) {

                                    // We have a hit within the current segment.
                                    Point candidate = crs()->LZtoRDW(left_intersection);
                                    new_left = crs()->getSurfacePoint(candidate.lmm(ref));
                                    if (new_left) {
                                        new_left->setTruncated(false);
                                    } else {
                                        new_left = new Point(candidate);
                                        crs()->addSurfacePoint(new_left);
                                    }
                                }
                            }
                            q2 = q1;
                        }
                        if (hit) {

                            // Both intersection points have been added.
                            // Now truncate the points in between them.
                            for (int i = 0; i < pointsBefore.length(); i++) {
                                Point* p = pointsBefore.at(i);

                                if ((p->lmm(ref) > Point::toLong(left_intersection.x())) && (p->lmm(ref) < Point::toLong(right_intersection.x()))) {
                                    // Point has to be deleted.
                                    crs()->removeSurfacePoint(p->lmm(ref));
                                    delete p;
                                }
                            }

                            // Check if the main loop can continue without problems.
                            pointsBefore = crs()->getSurfacePoints();
                            n = 0;
                            do {
                                p2 = pointsBefore.at(n);
                                n++;
                            }
                            while ((n < pointsBefore.length()) && (p2->lmm(ref) < Point::toLong(right_intersection.x())));
                            n--;
                        }
                    }
                }
            }
        }
        p1 = p2;
    }
    if (correction) {
        crs()->addRemark("Profiel automatisch aangepast: Negatieve helling verwijderd.");
    }
}
