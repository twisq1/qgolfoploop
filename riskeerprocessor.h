/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef RISKEERPROCESSOR_H
#define RISKEERPROCESSOR_H

#include "processor.h"

class RiskeerProcessor : public Processor
{
    static constexpr double PRECISION = 0.1;

public:
    RiskeerProcessor(Crosssection* crs);

    // Processor interface
public:
    virtual QString getName() const override;

private:
    virtual void doProcess() override;};

#endif // RISKEERPROCESSOR_H
