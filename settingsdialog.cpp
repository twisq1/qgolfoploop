/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "rdpparser.h"
#include "ipointparser.h"
#include "itoeparser.h"
#include "rubbleprocessor.h"
#include "zettingparser.h"
#include "corrector.h"
#include "validator.h"
#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "constants.h"
#include <QSettings>
#include <QMessageBox>

SettingsDialog::SettingsDialog(QWidget *parent, QMap<QString,int> *processor_order) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    m_processor_order = processor_order;
    ui->dspEpsilon->setValue(RDPparser::getGlobalEpsilon());
    ui->spNumIgnore_2->setValue(IPointParser::getGlobalNumIgnore());
    ui->spNumRegression_2->setValue(IPointParser::getGlobalNumRegression());
    ui->dspZetting_2->setValue(ZettingParser::getGlobalZetting());
    ui->cbTruncateIKP->setChecked(IPointParser::getGlobalIkbTruncate());
    ui->cb_negative->setChecked(Corrector::getGlobalCheckNegativeKey());
    ui->cb_flat->setChecked(Corrector::getGlobalCheckTooFlat());
    ui->cb_steep->setChecked(Corrector::getGlobalCheckTooSteep());
    ui->dspAlmostHorizontal->setValue(Corrector::getGlobalAlmostHorizontal());
    ui->dspMaxBank->setValue(Corrector::getGlobalMaxBank());
    ui->dspMinDam->setValue(Corrector::getGlobalMinDam());
    ui->dspMaxDam->setValue(Corrector::getGlobalMaxDam());
    ui->dspRubbleRoughness->setValue(RubbleProcessor::getGlobalRubbleRoughness());
    ui->spNumIgnore_12->setValue(IToeParser::getGlobalIgnore());
    ui->spNumRegression_12->setValue(IToeParser::getGlobalRegression());
    ui->spFixedToeHeight->setValue(IToeParser::getGlobalHeight());
    ui->cbFixedToe->setChecked(IToeParser::getGlobalFixed());
    ui->cbTruncateITP->setChecked(IToeParser::getGlobalTruncate());
    ui->keepRoughness->setChecked(RubbleProcessor::getGlobalRubbleKeepRoughness());
    ui->cb_start->setChecked(Validator::getGlobalCheckStart());
    ui->cb_end->setChecked(Validator::getGlobalCheckEnd());
    ui->cb_maxberm->setChecked(Validator::getGlobalCheckBerm());
    ui->sb_maxberm->setValue(Validator::getGlobalMaxBerm());
    //ui->spRubbleIgnore->setValue(RubbleProcessor::getGlobalRubbleIgnore());
    //ui->spRubbleRegression->setValue(RubbleProcessor::getGlobalRubbleRegression());
    QSettings settings;
    PRFLColumnSeperator sep = (PRFLColumnSeperator) settings.value("prfl_column_seperator", PRFLColumnSeperator::ColumnSpaces).toInt();
    ui->rbColumnSeperatorSpace->setChecked(sep==PRFLColumnSeperator::ColumnSpaces);
    ui->rbColumnSeperatorTab->setChecked(sep==PRFLColumnSeperator::ColumnTabs);

    // Load global processor box.
    crs = new Crosssection();

    updateProcessorListbox();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
    delete crs;
}

void SettingsDialog::on_buttonBox_accepted()
{
    RDPparser::setGlobalEpsilon(ui->dspEpsilon->value());
    IPointParser::setGlobalNumIgnore(ui->spNumIgnore_2->value());
    IPointParser::setGlobalNumRegression(ui->spNumRegression_2->value());
    IPointParser::setGobalIkbTruncate(ui->cbTruncateIKP->isChecked());
    ZettingParser::setGlobalZetting(ui->dspZetting_2->value());
    Corrector::setGlobalCheckNegativeKey(ui->cb_negative->isChecked());
    Corrector::setGlobalCheckTooFlat(ui->cb_flat->isChecked());
    Corrector::setGlobalCheckTooSteep(ui->cb_steep->isChecked());
    Corrector::setGlobalAlmostHorizontal(ui->dspAlmostHorizontal->value());
    Corrector::setGlobalMaxBank(ui->dspMaxBank->value());
    Corrector::setGlobalMinDam(ui->dspMinDam->value());
    Corrector::setGlobalMaxDam(ui->dspMaxDam->value());
    RubbleProcessor::setGlobalRubbleRoughness(ui->dspRubbleRoughness->value());
    IToeParser::setGlobalIgnore(ui->spNumIgnore_12->value());
    IToeParser::setGlobalRegression(ui->spNumRegression_12->value());
    IToeParser::setGlobalHeight(ui->spFixedToeHeight->value());
    IToeParser::setGlobalFixed(ui->cbFixedToe->isChecked());
    IToeParser::setGlobalTruncate(ui->cbTruncateITP->isChecked());
    RubbleProcessor::setGlobalRubbleKeepRoughness(ui->keepRoughness->isChecked());
    Validator::setGlobalCheckStart(ui->cb_start->isChecked());
    Validator::setGlobalCheckEnd(ui->cb_end->isChecked());
    Validator::setGlobalCheckBerm(ui->cb_maxberm->isChecked());
    Validator::setGlobalMaxBerm(ui->sb_maxberm->value());
    //RubbleProcessor::setGlobalRubbleIgnore(ui->spRubbleIgnore->value());
    //RubbleProcessor::setGlobalRubbleRegression(ui->spRubbleRegression->value());

    QSettings settings;
    if(ui->rbColumnSeperatorSpace->isChecked()){
        settings.setValue("prfl_column_seperator", (int) PRFLColumnSeperator::ColumnSpaces);
    }else{
        settings.setValue("prfl_column_seperator", (int) PRFLColumnSeperator::ColumnTabs);
    }


    // TODO >> Save settings from global processor box.
    m_processor_order->clear();
    for (int i = 0; i < ui->lbGlobalProcessor->count(); i++) {
        QListWidgetItem* item = ui->lbGlobalProcessor->item(i);
        Processor* processor = reinterpret_cast<Processor*>(item->data(Qt::UserRole).toLongLong());
        bool enable = item->checkState() == Qt::Checked;
        processor->setGlobalEnable(enable);
        m_processor_order->insert(processor->getName(), i);
    }
}

void SettingsDialog::on_pbMoveUp_clicked()
{
    int row = ui->lbGlobalProcessor->currentRow();
    if(row > 0){
        QListWidgetItem *item = ui->lbGlobalProcessor->takeItem(row);
        ui->lbGlobalProcessor->insertItem(row-1, item);
        ui->lbGlobalProcessor->setCurrentRow(row-1);
    }
}

void SettingsDialog::on_pbMoveDown_clicked()
{
    int row = ui->lbGlobalProcessor->currentRow();
    if(row < ui->lbGlobalProcessor->count()-1){
        QListWidgetItem *item = ui->lbGlobalProcessor->takeItem(row);
        ui->lbGlobalProcessor->insertItem(row+1, item);
        ui->lbGlobalProcessor->setCurrentRow(row+1);
    }
}

void SettingsDialog::on_pbReset_clicked()
{
    if(QMessageBox::warning(this, "QGolfOploop", "Weet u zeker dat u terug wilt naar de standaard instellingen?", QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Ok){
        m_processor_order->clear();
        m_processor_order->insert(PROCESSOR_IKP, 0);
        m_processor_order->insert(PROCESSOR_ITP, 1);
        m_processor_order->insert(PROCESSOR_TRUNC, 2);
        m_processor_order->insert(PROCESSOR_RDP, 3);
        m_processor_order->insert(PROCESSOR_BEKLEDING, 4);
        m_processor_order->insert(PROCESSOR_BREUKSTEEN, 5);
        m_processor_order->insert(PROCESSOR_ZETTING, 6);
        m_processor_order->insert(PROCESSOR_COR_NEG, 7);
        m_processor_order->insert(PROCESSOR_COR_GENTLESLOPE, 8);
        m_processor_order->insert(PROCESSOR_COR_STEEP, 9);
        m_processor_order->insert(PROCESSOR_VALIDATOR, 10);
        m_processor_order->insert(PROCESSOR_RISKEER, 11);

        updateProcessorListbox();
    }
}

void SettingsDialog::updateProcessorListbox()
{
    ui->lbGlobalProcessor->clear();
    //create a sorted map with the order of processors
    QMap<int, QString> ordered;
    QMap<QString, int>::const_iterator i = m_processor_order->constBegin();
    while (i != m_processor_order->constEnd()) {
        ordered[i.value()] = i.key();
        ++i;
    }

    QMap<int, QString>::const_iterator it = ordered.constBegin();
    while (it != ordered.constEnd()) {
        for (Processor* processor : crs->getProcessors()) {
            if(processor->getName() == it.value()){
                QListWidgetItem *item = new QListWidgetItem(processor->getName(), ui->lbGlobalProcessor);
                item->setData(Qt::UserRole, (qlonglong)processor);
                item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
                item->setCheckState(processor->getGlobalEnable() ? Qt::Checked : Qt::Unchecked);
                ui->lbGlobalProcessor->addItem(item);
            }
        }
        ++it;
    }
}
