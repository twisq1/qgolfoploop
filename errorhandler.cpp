/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QMessageBox>
#include <QCoreApplication>
#include "simpleQtLogger.h"
#include "errorhandler.h"

void ErrorHandler::exception(QGOException &ex)
{
    bool terminate = false;
    QString type;

    switch (ex.getType()) {
    case QGOException::FATAL:
        L_FATAL(ex.getMessage());
        terminate = true;
        type = "Fatal error";
        break;
    case QGOException::ERROR:
        L_ERROR(ex.getMessage());
        type = "Error";
        break;
    case QGOException::WARNING:
        L_WARN(ex.getMessage());
        type = "Warning";
        break;
    case QGOException::INFO:
        L_INFO(ex.getMessage());
        type = "Melding";
        break;
    }

    QString html = "<html><body><h1>" + type + "</h1>";
    html += "<p>" + ex.getMessage() + "</p>";
    html += "<p>" + ex.getDescription() + "</p>";
    html += "</body></html>";
    QMessageBox::information(nullptr, "qGolfoploop", html);

    if (terminate) {
        // Terminate program
        QCoreApplication::exit(-1);
    }
}

void ErrorHandler::info(const QString msg)
{
    QGOException ex(QGOException::INFO, msg, "");
    exception(ex);
}

void ErrorHandler::warn(const QString msg)
{
    QGOException ex(QGOException::WARNING, msg, "");
    exception(ex);
}

void ErrorHandler::error(const QString msg)
{
    QGOException ex(QGOException::ERROR, msg, "");
    exception(ex);
}

void ErrorHandler::fatal(const QString msg)
{
    QGOException ex(QGOException::FATAL, msg, "");
    exception(ex);
}
