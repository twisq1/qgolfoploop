/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "rdpparser.h"

#include "constants.h"

#include <QtMath>
#include <QSettings>
#include "simpleQtLogger.h"
#include "point.h"
#include "crosssection.h"

double PerpendicularDistance(const Point* base, const Point *pt, const Point *lineStart, const Point *lineEnd)
{
    double dx = lineEnd->l(base) - lineStart->l(base);
    double dy = lineEnd->z() - lineStart->z();

    //Normalise
    double mag = qPow(qPow(dx,2.0)+qPow(dy,2.0),0.5);
    if(mag > 0.0)
    {
        dx /= mag; dy /= mag;
    }

    double pvx = pt->l(base) - lineStart->l(base);
    double pvy = pt->z()  - lineStart->z();

    //Get dot product (project pv onto normalized direction)
    double pvdot = dx * pvx + dy * pvy;

    //Scale line direction vector
    double dsx = pvdot * dx;
    double dsy = pvdot * dy;

    //Subtract this from pv
    double ax = pvx - dsx;
    double ay = pvy - dsy;

    return qPow(qPow(ax,2.0)+qPow(ay,2.0),0.5);
}

RDPparser::RDPparser(Crosssection* crs) : Processor(crs)
{
}

QString RDPparser::getName() const
{
    return PROCESSOR_RDP;
}

void RDPparser::setGlobalEpsilon(double epsilon)
{
    QSettings settings;
    settings.setValue(GLOBAL_EPSILON_KEY, epsilon);
}

double RDPparser::getGlobalEpsilon()
{
    QSettings settings;
    return settings.value(GLOBAL_EPSILON_KEY, DEFAULT_EPSILON).toDouble();
}

void RDPparser::doProcess()
{
    if(crs()->numSurfacePoints() < 2){
        error("Het dwarsprofiel bevat minder dan 2 punten.");
        return;
    }

    double epsilon = crs()->hasRDPepsilon() ? crs()->getRDPepsilon() : getGlobalEpsilon();

    QList<Point*> points;

    //we maken lijnstukken die `ge-rdpt` moeten worden die lopen van begin tot eind maar ook onderbroken worden
    //door de karakteristieke punten zodat de karakteristieke punten nooit verwijderd worden
    QList<Point*> spoints = crs()->getSurfacePoints();
    for(int i=0; i<spoints.count(); i++){
        Point *p = spoints.at(i);
        points.append(p);

        if ((p->getId() != Point::NONE) || i==spoints.count()-1){
            if(points.count()> 2){
                apply(points, epsilon);
            }
            points.clear();
            points.append(p);
        }
    }
}


void RDPparser::apply(const QList<Point *> &pointList, double epsilon)
{
    Point* base = pointList.at(0);

    // Find the point with the maximum distance from line between start and end
    double dmax = 0.0;
    int index = 0;
    int end = pointList.length()-1;
    for(int i = 1; i < pointList.length()-1; i++)
    {
        double d = PerpendicularDistance(base, pointList[i], pointList[0], pointList[end]);
        if (d > dmax)
        {
            index = i;
            dmax = d;
        }
    }

    if(dmax > epsilon)
    {
        QList<Point*> firstLine;
        for(int i=0; i<(index+1); i++){if(!pointList.at(i)->truncated()) firstLine.append(pointList.at(i));}
        QList<Point*> lastLine;
        for(int i=index; i<pointList.length(); i++){if(!pointList.at(i)->truncated()) lastLine.append(pointList.at(i));}
        apply(firstLine, epsilon);
        apply(lastLine, epsilon);
    }else{ //alles tussen begin en eind valt binnen epsilon dus deze punten op truncated zetten
        for(int i=1; i<pointList.count()-1; i++){
            pointList.at(i)->setTruncated();
        }
    }
}

