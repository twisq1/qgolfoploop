#include "exportdialog.h"
#include "ui_exportdialog.h"

ExportDialog::ExportDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExportDialog)
{
    m_what = NOTHING;
    ui->setupUi(this);
}

ExportDialog::~ExportDialog()
{
    delete ui;
}

ExportDialog::ExportWhat ExportDialog::getExportWhat() const
{
    return m_what;
}

void ExportDialog::on_buttonBox_accepted()
{
    if (ui->radioSingleProfile->isChecked()) m_what = SINGLE_PROFILE;
    if (ui->radioAllProfiles->isChecked()) m_what = ALL_PROFILES;
    if (ui->radioSingleImage->isChecked()) m_what = SINGLE_IMAGE;
    if (ui->radioAllImages->isChecked()) m_what = ALL_IMAGES;
    if (ui->radioShapefile->isChecked()) m_what = SHAPEFILE;
    if (ui->radioXml->isChecked()) m_what = XML_FILE;
}
