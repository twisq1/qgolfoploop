/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QDir>
#include <QCoreApplication>
#include <QMessageBox>
#include <QCheckBox>
#include <QSettings>
#include <QProgressDialog>
#include "point.h"
#include "simpleQtLogger.h"
#include "errorhandler.h"
#include "xmlitem.h"
#include "project.h"

bool Project::show_processor_warning = true;

Project::Project()
{
    clearData();
}

Project::~Project()
{
    clearData();
}

bool Project::read(const QJsonObject &json)
{
    clearData();

    QString application = json["application"].toString();
    if (application == "QGolfOploop") {

        bool ok = true;

        // Read roughness map
        QJsonArray map = json["materials"].toArray();
        for (int i = 0; i < map.size(); i++) {
            QJsonObject entry = map[i].toObject();
            if (entry.contains("r") && entry.contains("material")) {
                int key = entry["r"].toInt();
                QString material = entry["material"].toString();
                m_roughnessmap[key] = material;
            } else {
                ErrorHandler::info("Fout in bestand.");
                return false;
            }
        }

        // Read crosssections
        QJsonArray list = json["crosssections"].toArray();
        for (int i = 0; ok && (i < list.size()); i++) {
            QJsonObject entry = list[i].toObject();
            Crosssection* crs = new Crosssection();
            ok = crs->read(entry);
            if (ok) m_crosssections.append(crs);
        }
        m_current_crosssection_index = json["crosssection_index"].toInt();

        return ok;
    }

    QGOException ex(QGOException::INFO, "Ongeldig bestand", "Het bestand is geen golfoploopbestand en kan niet gelezen worden.");
    ErrorHandler::exception(ex);
    return false;
}

bool Project::write(QJsonObject &json) const
{
    json["application"] = "QGolfOploop";
    json["version"] = QCoreApplication::applicationVersion();
    json["url"] = "https://www.twisq.nl";

    // Write roughness map
    QJsonArray map;
    for (int key: m_roughnessmap.keys()) {
        QString roughness = m_roughnessmap.value(key);
        QJsonObject entry;
        entry["r"] = key;
        entry["material"] = roughness;
        map.append(entry);
    }
    json["materials"] = map;

    // TODO: crosssections
    QJsonArray list;
    for (Crosssection* crs: m_crosssections) {
        QJsonObject entry;
        crs->write(entry);
        list.append(entry);
    }
    json["crosssections"] = list;
    json["crosssection_index"] = m_current_crosssection_index;

    // TODO: settings

    return true;
}

// Delete all data, for example before reading a file.
void Project::clearData()
{
    foreach (Crosssection *crs, m_crosssections) {
        delete crs;
    }
    m_crosssections.clear();
    m_current_crosssection_index = -1;

    m_roughnessmap.clear();
    m_roughnessmap[1000] = "standaard materiaal";
}

Crosssection *Project::getCrosssectionByProfileCode(const QString &profilecode)
{
    foreach (Crosssection *crs, m_crosssections) {
        if(crs->getName() == profilecode)
            return crs;
    }
    return nullptr;
}


bool Project::isDirty() const
{
    bool dirty = false;
    foreach (Crosssection* crs, m_crosssections) {
        dirty |= crs->isDirty();
    }
    return dirty;
}

bool Project::readFromPath(const QString &path)
{
    //L_DEBUG("Reading from path: " + path);
    if (path.isEmpty()) return false;

    // First delete all existing old data.
    clearData();

    // Open directory and look for files.
    QStringList surfaceFiles;
    QStringList characteristicFiles;
    QStringList xmlFiles;

    QDir dir = QDir(path);
    QStringList filemasks;
    filemasks << "*.csv";
    filemasks << "*.xml";
    QStringList allFiles = dir.entryList(filemasks, QDir::Files | QDir::NoSymLinks);
    for (QString filename : allFiles) {

        QString fullFilename = dir.absoluteFilePath(filename);
        //L_DEBUG(fullFilename);

        if (fullFilename.right(3).toLower() == "csv") {

            // CSV file, so read first line to determine type.
            QFile file(fullFilename);
            if(!file.open(QIODevice::ReadOnly)) {
                QGOException ex(QGOException::ExceptionType::WARNING, "File Read Error", "Bestand kan niet gelezen worden: " + fullFilename);
                ErrorHandler::exception(ex);
                return false;
            }
            QTextStream stream(&file);
            QString header = stream.readLine();
            file.close();

            if (header.startsWith("LOCATIONID;X1;Y1;Z1;.....;Xn;Yn;Zn;(Profiel)")) surfaceFiles.append(fullFilename);
            if (header.startsWith("CODE;SUBCODE;X;Y;Z;LOCATIONID")) surfaceFiles.append(fullFilename);
            if (header.startsWith("CODE;SUBCODE;X_WAARDE;Y_WAARDE;Z_WAARDE;PROFIELNAAM")) surfaceFiles.append(fullFilename);
            if (header.startsWith("LOCATIONID;X_")) characteristicFiles.append(fullFilename);
        }

        if (fullFilename.right(3).toLower() == "xml") {
            xmlFiles.append(fullFilename);
        }
    }

    // We should have exactly one surfacelines file and one characteristic points file.
    if (surfaceFiles.size() < 1) {
        QGOException ex(QGOException::ExceptionType::WARNING, "File Read Error", "Folder bevat geen geldig surfacelines bestand.");
        ErrorHandler::exception(ex);
        return false;
    }
    if (surfaceFiles.size() > 1) {
        QGOException ex(QGOException::ExceptionType::WARNING, "File Read Error", "Folder bevat meerdere surfacelines bestanden. Verwijder de overbodige bestanden.");
        ErrorHandler::exception(ex);
        return false;
    }
    if (characteristicFiles.size() < 1) {
        QGOException ex(QGOException::ExceptionType::WARNING, "File Read Error", "Folder bevat geen geldig characteristicPoints bestand.");
        ErrorHandler::exception(ex);
    }
    if (characteristicFiles.size() > 1) {
        QGOException ex(QGOException::ExceptionType::WARNING, "File Read Error", "Folder bevat meerdere characteristicPoints bestanden. Verwijder de overbodige bestanden.");
        ErrorHandler::exception(ex);
        return false;
    }

    // Read files.
    bool success = true;
    if (success) success &= readSurfacelines(surfaceFiles.first());
    if (success && characteristicFiles.size()) success &= readCharacteristicPoints(characteristicFiles.first());
    for (QString xmlFilename : xmlFiles) {
        if (success) success &= readXmlFile(xmlFilename);
    }
    if (success) {
        for (Crosssection* crs : m_crosssections) {
            crs->copySurfaceToOriginalPoints();
        }
    } else {
        QGOException ex(QGOException::ExceptionType::ERROR, "File Read Error", "Interne fout in file reader.");
        ErrorHandler::exception(ex);
    }
    return success;
}

bool Project::readCharacteristicPoints(const QString &filename)
{
    //L_DEBUG("Reading characteristic file: " + filename);
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        // This should not happen, since the file has been opened just before.
        return false;
    }
    QTextStream stream(&file);

    bool header = true;
    bool missing_points = false;
    int index_teen_buitenwaarts = -1;
    int index_kruin_binnenwaarts = -1;
    int index_kruin_buitenwaarts = -1;

    while(!stream.atEnd()) {
        /*
         * read the header and find the index of the mandatory points
         */
        if(header){            
            QString line = stream.readLine();
            QStringList args = line.split(';');

            if(args.length()>0){
                index_kruin_binnenwaarts = args.indexOf("X_Kruin binnentalud");
                index_kruin_buitenwaarts = args.indexOf("X_Kruin buitentalud");
                index_teen_buitenwaarts = args.indexOf("X_Teen dijk buitenwaarts");

                if(index_kruin_binnenwaarts==1 || index_kruin_buitenwaarts==-1 || index_teen_buitenwaarts == -1){
                    QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand mist de benodigde characteristic points.");
                    ErrorHandler::exception(ex);
                    return false;
                }
            }
            header = false;
        }

        /*
         * read the locations of the mandatory points and use them to remove
         * unnecessary points from the crosssection
         */
        QString line = stream.readLine();
        QStringList args = line.split(';');
        if(args.length()>index_teen_buitenwaarts){ // this index is always > the other indices
            QString profielcode = args[0];

            Crosssection *crs = getCrosssectionByProfileCode(profielcode);
            if(crs == nullptr){
                QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points en Surfaceline bestanden komen niet overeen.");
                ErrorHandler::exception(ex);
                return false;
            }
            Point* base = crs->getReferencePoint();

            /* Kruin binnentalud */
            bool okx, oky, okz;
            double x = args[index_kruin_binnenwaarts].trimmed().toDouble(&okx);
            double y = args[index_kruin_binnenwaarts+1].trimmed().toDouble(&oky);
            double z = args[index_kruin_binnenwaarts+2].trimmed().toDouble(&okz);
            if(!(okx && oky && okz)){
                QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand bevat ongeldige coordinaten.");
                ErrorHandler::exception(ex);
                return false;
            }
            if ((x < -0.9) && (x > -1.1) && (y < -0.9) && (y > -1.1)) {
                // Characteristic point is niet ingevuld.
                missing_points = true;
            } else {
                Point* kruin_binnentalud = crs->getSurfacePoint(Point(x, y, z).lmm(base));
                if (!kruin_binnentalud) {
                    QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand bevat een punt 'kruin binnentalud' wat niet in de Surfacelines voorkomt. Profiel " +
                                    crs->getName() + " X=" + QString::number(x, 'f', 3) + " Y=" + QString::number(y, 'f', 3) + " Z=" + QString::number(z, 'f', 3));
                    ErrorHandler::exception(ex);
                    return false;
                } else {
                    kruin_binnentalud->setId(Point::CPID::KRUIN_BINNENTALUD);
                }
            }

            /* Kruin buitentalud */
            x = args[index_kruin_buitenwaarts].trimmed().toDouble(&okx);
            y = args[index_kruin_buitenwaarts+1].trimmed().toDouble(&oky);
            z = args[index_kruin_buitenwaarts+2].trimmed().toDouble(&okz);
            if(!(okx && oky && okz)){
                QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand bevat ongeldige coordinaten.");
                ErrorHandler::exception(ex);
                return false;
            }
            if ((x < -0.9) && (x > -1.1) && (y < -0.9) && (y > -1.1)) {
                // Characteristic point is niet ingevuld.
                missing_points = true;
            } else {
                Point* kruin_buitentalud = crs->getSurfacePoint(Point(x, y, z).lmm(base));
                if (!kruin_buitentalud) {
                    QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand bevat een punt 'kruin buitentalud' wat niet in de Surfacelines voorkomt. Profiel " +
                                    crs->getName() + " X=" + QString::number(x, 'f', 3) + " Y=" + QString::number(y, 'f', 3) + " Z=" + QString::number(z, 'f', 3));
                    ErrorHandler::exception(ex);
                    return false;
                } else {
                    kruin_buitentalud->setId(Point::CPID::KRUIN_BUITENTALUD);
                }
            }

            /* Teen dijk buitenwaarts */
            x = args[index_teen_buitenwaarts].trimmed().toDouble(&okx);
            y = args[index_teen_buitenwaarts+1].trimmed().toDouble(&oky);
            z = args[index_teen_buitenwaarts+2].trimmed().toDouble(&okz);
            if(!(okx && oky && okz)){
                QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand bevat ongeldige coordinaten.");
                ErrorHandler::exception(ex);
                return false;
            }
            if ((x < -0.9) && (x > -1.1) && (y < -0.9) && (y > -1.1)) {
                // Characteristic point is niet ingevuld.
                missing_points = true;
            } else {
                Point* teen_buitenwaarts = crs->getSurfacePoint(Point(x, y, z).lmm(base));
                if (!teen_buitenwaarts) {
                    QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand bevat een punt 'teen buitenwaarts' wat niet in de Surfacelines voorkomt. Profiel " +
                                    crs->getName() + " X=" + QString::number(x, 'f', 3) + " Y=" + QString::number(y, 'f', 3) + " Z=" + QString::number(z, 'f', 3));
                    ErrorHandler::exception(ex);
                    return false;
                } else {
                    teen_buitenwaarts->setId(Point::CPID::TEEN_DIJK_BUITENWAARTS);
                }
            }

        } else {
            QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Characteristic Points bestand bevat ongeldige inhoud.");
            ErrorHandler::exception(ex);
            return false;
        }
    }
    if (missing_points) {
        QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Niet alle benodigde characteristic point zijn ingevuld in het bestand. Hierdoor kunnen niet alle bewerkingen worden uitgevoerd.");
        ErrorHandler::exception(ex);
    }
    return true;
}

bool Project::readSurfacelines(const QString &filename)
{
    //L_DEBUG("Read surfacelines: " + filename);
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        // This should not happen since the file has just been opened before.
        return false;
    }
    QTextStream stream(&file);

    bool header = true;
    while(!stream.atEnd()) {
        if(header){
            QString hdr = stream.readLine(); // Read header.
            if (!hdr.startsWith("LOCATIONID;X1;Y1;Z1;.....;Xn;Yn;Zn;(Profiel)")) {
                if (hdr.startsWith("CODE;SUBCODE;X;Y;Z;LOCATIONID") ||
                    hdr.startsWith("CODE;SUBCODE;X_WAARDE;Y_WAARDE;Z_WAARDE;PROFIELNAAM")) {
                    return readAlternativeSurfacelines(stream);
                } else {
                    QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Dit type surfacelines file wordt niet ondersteund.");
                    ErrorHandler::exception(ex);
                    return false;
                }
            }
            header = false;
        }
        QString line = stream.readLine();
        QStringList args = line.split(";");
        if (args.count() < 1) {
            clearData();
            QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Ongeldige of lege regel in surface bestand.");
            ErrorHandler::exception(ex);
            return false;
        }

        Crosssection *crs = new Crosssection();
        crs->setName(args[0]);
        for(int i = 1; i < args.count(); i+=3) {

            if ((i+2) >= args.count()) {
                clearData();
                QGOException ex(QGOException::ExceptionType::WARNING, "Illegal file", "Surface bestand bevat minstens één onvolledig punt en kan niet worden ingelezen.");
                ErrorHandler::exception(ex);
                return false;
            }

            // Read coordinates.
            bool okx, oky, okz;
            double x = args[i].trimmed().toDouble(&okx);
            double y = args[i+1].trimmed().toDouble(&oky);
            double z = args[i+2].trimmed().toDouble(&okz);

            if (okx && oky && okz) {
                Point* p = new Point(x, y, z);
                crs->addSurfacePoint(p);
            }
        }
        m_crosssections.append(crs);
    }
    return true;
}

bool Project::readAlternativeSurfacelines(QTextStream &stream)
{
    Crosssection* crs = nullptr;
    while(!stream.atEnd()) {
        QString line = stream.readLine();
        QStringList args = line.split(";");
        if (args.count() < 6) {
            clearData();
            QGOException ex(QGOException::ExceptionType::WARNING, "File error", "Ongeldige regel in surfacelines bestand.");
            ErrorHandler::exception(ex);
            return false;
        }

        // Check if we are starting a new cross section.
        if (!crs) {
            crs = new Crosssection();
            crs->setName(args[5]);
        } else if (crs->getName() != args[5]) {
            m_crosssections.append(crs);
            crs = new Crosssection();
            crs->setName(args[5]);
        }

        // Read coordinates.
        bool okx, oky, okz;
        double x = args[2].trimmed().replace(",", ".").toDouble(&okx);
        double y = args[3].trimmed().replace(",", ".").toDouble(&oky);
        double z = args[4].trimmed().replace(",", ".").toDouble(&okz);

        if (!(okx && oky && okz)) {
            clearData();
            QGOException ex(QGOException::ExceptionType::WARNING, "File error", "Bestand bevat ongeldig coördinaat.");
            ErrorHandler::exception(ex);
            return false;
        }

        crs->addSurfacePoint(new Point(x, y, z));
    }
    if (crs) m_crosssections.append(crs);
    return true;
}

void Project::parseXmlSturingsParameters(QDomDocument& doc)
{
    XMLItem xml(doc.documentElement());
    for (XMLItem x : xml.getChildren()) {
        QString id;
        double epsilon;
        int skip;
        int segments;
        if (x.getValueAsString("id", &id) &&
            x.getValueAsDouble("peuckerEpsilon", &epsilon) &&
            x.getValueAsInt("segmentenOverslaanInLineaireregressieLijn", &skip) &&
            x.getValueAsInt("segmentenInLineaireregressieLijn", &segments)) {
            //L_DEBUG(id + " : " + QString::number(epsilon) + " : " + QString::number(skip) + " : " + QString::number(segments));
            Crosssection* crs = getCrosssectionByProfileCode(id);
            if (crs) {
                crs->setRDPepsilon(epsilon);
                crs->setIKPnumskip(skip);
                crs->setIKPnumregression(segments);
            } else {
                L_WARN(QString("XML bestand bevat onbekende profielcode %1").arg(id));
            }
        } else {
            L_WARN("XML bestand bevat ongeldige inhoud.");
        }

    }
}

void Project::parseXmlRuwheden(QDomDocument &doc)
{
    XMLItem xml(doc.documentElement());
    for (XMLItem x : xml.getChildren()) {
        //L_DEBUG("Naam xml object: " + x.getName());
        QString id;
        if(x.getValueAsString("id", &id)){
            XMLItem item(doc.documentElement());
            if(x.getItemById("bekledingsvlakken", &item)){
                for(XMLItem bv : item.getChildren()){
                    double xstart = 0.0, ystart = 0.0, zstart = 0.0;
                    double xend = 0.0, yend = 0.0, zend = 0.0;
                    double roughness = 0.0;
                    QString material;
                    if (bv.getValueAsDouble("xstart_bekledingsvlak", &xstart) &&
                        bv.getValueAsDouble("ystart_bekledingsvlak", &ystart) &&
                        bv.getValueAsDouble("zstart_bekledingsvlak", &zstart) &&
                        bv.getValueAsDouble("xeind_bekledingsvlak", &xend) &&
                        bv.getValueAsDouble("yeind_bekledingsvlak", &yend) &&
                        bv.getValueAsDouble("zeind_bekledingsvlak", &zend) &&
                        bv.getValueAsString("bekledingsmateriaal", &material) &&
                        bv.getValueAsDouble("ruwheid", &roughness)) {
                            //voeg de ruwheid toe aan de map
                            m_roughnessmap[static_cast<int>(roughness * 1000)] = material;

                            Crosssection* crs = getCrosssectionByProfileCode(id);
                            if (crs) {
                                Point pstart(xstart, ystart, zstart);
                                Point pend(xend, yend, zend);
                                if (!crs->addRoughness(pstart, pend, roughness)) {
                                    L_WARN(QString("Punt uit ruwheid XML bestand niet gevonden in profiel: ").arg(id));
                                    crs->setRemark("Niet alle punten uit het ruwheid XML bestand zijn gevonden.");
                                }
                            } else {
                                L_WARN(QString("XML bestand bevat onbekende profielcode %1").arg(id));
                            }
                    }else{
                        L_WARN("XML bestand bevat ongeldige inhoud bij het inlezen van een bekledingsvlak.");
                    }
                }
            }
        }else{
            L_WARN("XML bestand bevat ongeldige inhoud bij het parsen van de ruwheden.");
        }
    }
}

void Project::parseXmlDam(QDomDocument &doc)
{
    XMLItem xml(doc.documentElement());
    for (XMLItem x : xml.getChildren()) {
        QString id;
        double height=0.0;
        int type=0;
        QString name;
        if (x.getValueAsString("id", &id) &&
            x.getValueAsDouble("damhoogte", &height) &&
            x.getValueAsInt("damType", &type) &&
            x.getValueAsString("damTypeNaam", &name)) {
            //L_DEBUG("Dam " + id + " : " + QString::number(type) + " : " + name + " : " + QString::number(height));
            Crosssection* crs = getCrosssectionByProfileCode(id);
            if (crs) {
                crs->getDam()->set(type, name, height);
            } else {
                L_WARN(QString("XML bestand bevat onbekende profielcode %1").arg(id));
            }
        } else {
            L_WARN("XML bestand bevat ongeldige inhoud.");
        }

    }
}

void Project::parseXmlDamwand(QDomDocument &doc)
{
    XMLItem xml(doc.documentElement());
    for (XMLItem x : xml.getChildren()) {
        QString id;
        double height;
        int type;
        QString name;
        if (x.getValueAsString("id", &id) &&
            x.getValueAsDouble("damwandhoogte", &height) &&
            x.getValueAsInt("damwandType", &type) &&
            x.getValueAsString("damwandTypeNaam", &name)) {
            //L_DEBUG("Damwand " + id + " : " + QString::number(type) + " : " + name + " : " + QString::number(height));
            Crosssection* crs = getCrosssectionByProfileCode(id);
            if (crs) {
                crs->getDamwand()->set(type, name, height);
            } else {
                L_WARN(QString("XML bestand bevat onbekende profielcode %1").arg(id));
            }
        } else {
            L_WARN("XML bestand bevat ongeldige inhoud.");
        }

    }
}

void Project::parseXmlZetting(QDomDocument &doc)
{
    XMLItem xml(doc.documentElement());
    for (XMLItem x : xml.getChildren()) {
        QString id;
        double zetting;
        if (x.getValueAsString("id", &id) &&
            x.getValueAsDouble("zetting", &zetting))
        {
            //L_DEBUG("Zetting " + id + " : " + QString::number(zetting));
            Crosssection* crs = getCrosssectionByProfileCode(id);
            if (crs) {
                crs->setZetting(zetting);
            } else {
                L_WARN(QString("XML bestand bevat onbekende profielcode %1").arg(id));
            }
        } else {
            L_WARN("XML bestand bevat ongeldige inhoud.");
        }
    }
}

bool Project::readXmlFile(const QString &filename)
{
    //L_DEBUG("Read XML file: " + filename);
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)){
        QGOException ex(QGOException::ExceptionType::WARNING, "File Read Error", "XML bestand kan niet worden geopend.");
        ErrorHandler::exception(ex);
        return false;
    }

    bool success;
    QDomDocument doc;
    success = doc.setContent(&file);
    file.close();

    if (!success) {
        QGOException ex(QGOException::ExceptionType::WARNING, "XML error", "XML bestand is ongeldig: " + filename);
        ErrorHandler::exception(ex);
    } else {
        QString taste = doc.documentElement().nodeName();
        //L_DEBUG(taste);

        if (taste == "sturingsparametersGolfoploopProfielen") {
            parseXmlSturingsParameters(doc);
        } else if (taste=="ruwheidGolfoploopProfielen"){
            parseXmlRuwheden(doc);
        } else if (taste=="damGolfoploopProfielen"){
            parseXmlDam(doc);
        } else if (taste=="damwandGolfoploopProfielen"){
            parseXmlDamwand(doc);
        } else if (taste=="zettingGolfoploopProfielen"){
            parseXmlZetting(doc);
        } else {
            //L_DEBUG("No parser for " + taste);
        }
    }

    return success;
}

// Write all profiles in *.prfl format to path.
void Project::writePrlf(QTextStream& stream, Crosssection* crs)
{
    Q_ASSERT(crs);
    char odoa[3] = {13, 10, 0};

    int dam = crs->getDam()->type();
    if (dam < 0) dam = 0;
    double dam_height = crs->getDam()->height();

    stream << "VERSIE       4.0" << odoa;
    stream << "ID           " << crs->getName() << odoa;
    stream << "RICHTING     " << QString::number((int)(crs->getDirection() + 0.5)) << odoa;
    stream << "DAM          " << QString::number(dam) << odoa;
    stream << "DAMHOOGTE    " << QString::number(dam_height, 'f', 3) << odoa;
    stream << odoa;

    // Bepaal waar de overgang is tussen voorland en dijk.
    long teenpos = 0;
    Point* teen = crs->getCharacteristicPoint(Point::CPID::IMAGINAIR_TEENPUNT);
    if (!teen) teen = crs->getCharacteristicPoint(Point::CPID::TEEN_DIJK_BUITENWAARTS);
    if (teen) {
        teenpos = teen->lmm(crs->getReferencePoint());
    } else {
        L_WARN("Profiel bevat geen teen");
    }

    QStringList voorland;
    QStringList dijk;
    double offset = 0.0;

    QSettings settings;
    PRFLColumnSeperator sep = (PRFLColumnSeperator) settings.value("prfl_column_seperator", PRFLColumnSeperator::ColumnSpaces).toInt();

    for (auto p : crs->getSurfacePoints()) {
        QString s;
        if(sep==PRFLColumnSeperator::ColumnSpaces){
            s = QString::number(p->l(crs->getReferencePoint()) - offset, 'f', 3).leftJustified(13, ' ') +
                QString::number(p->z(), 'f', 3).leftJustified(10, ' ') +
                QString::number(p->roughness(), 'f', 3);
        }else{
            s = QString::number(p->l(crs->getReferencePoint()) - offset, 'f', 3) + "\t" + QString::number(p->z(), 'f', 3) + "\t" + QString::number(p->roughness(), 'f', 3);
        }

        if (p->lmm(crs->getReferencePoint()) < teenpos) {
            voorland.append(s);
        } else {
            dijk.append(s);
        }

    }

    // Voorland removed in version 1.30
    stream << "VOORLAND     0" << odoa;
    //stream << "VOORLAND     " << voorland.size() << odoa;
    //for (QString& s : voorland) {
    //    stream << s << odoa;
    //}
    //stream << odoa;

    int dam_wall = crs->getDamwand()->type();
    if (dam_wall < 0) dam_wall = 0;
    stream << "DAMWAND      " << QString::number(dam_wall) << odoa;
    stream << "KRUINHOOGTE  " << QString::number(crs->getZMax(), 'f', 3) << odoa;
    stream << "DIJK         " << dijk.size() << odoa;
    for (QString& s : dijk) {
        stream << s << odoa;
    }

    stream << odoa;
    stream << "MEMO" << odoa;
    stream << crs->getRemark() << odoa;
}


void Project::exportSingleProfile(const QString filename, Crosssection* crs)
{
    if (!crs) crs = getCurrentCrosssection();

    QFile file(filename);
    if (file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        writePrlf(stream, crs);
        file.close();
    } else {
        QGOException ex(QGOException::ERROR, "Schrijffout", "Fout bij schrijven naar bestand " + filename);
        ErrorHandler::exception(ex);
    }
}

void Project::addProcessorMapping(const QString key, const int value)
{
    m_processormapping[key] = value;
}

int Project::getProcessorOrder(const QString key)
{
    return m_processormapping[key]; //TODO > fout afhandeling of juist laten crashen omdat het goed moet zitten in de code?
}

QList<QString> Project::getOrderedProcessorNames()
{
    //create a sorted map with the order of processors
    QMap<int, QString> ordered;
    QMap<QString, int>::const_iterator i = m_processormapping.constBegin();
    while (i != m_processormapping.constEnd()) {
        ordered[i.value()] = i.key();
        ++i;
    }
    return ordered.values();
}

void Project::exportAllProfiles(const QString &path)
{
    // Does path exist?
    //L_DEBUG("Writing PRFLs to path: " + path);
    QDir dir = QDir(path);
    if (!dir.exists()) {
        QGOException ex(QGOException::WARNING, "Folder niet gevonden", "De gekozen folder om PRFL bestanden in op te slaan bestaat niet.");
        ErrorHandler::exception(ex);
        return;
    }

    // Do (some) files already exist?
    bool oldfiles = false;
    for (int i = 0; i < numCrosssections(); i++) {
        Crosssection* crs = getCrosssection(i);
        if (!crs) {
            QGOException ex(QGOException::ERROR, "Interne fout", "Data beschadigd. Sluit programma af en probeer opnieuw.");
            ErrorHandler::exception(ex);
            return;
        }
        QString filename = dir.filePath(crs->getName() + ".prfl");
        QFile file(filename);
        oldfiles |= file.exists();
    }

    if (oldfiles) {
        auto reply = QMessageBox::question(nullptr, "Vraag", "Bestaande *prfl bestanden overschrijven?", QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::No) return;
    }

    // Write files.
    QProgressDialog progress("Bezig met het exporteren van profieles", "Afbreken", 0, numCrosssections());
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumDuration(10);

    for (int i = 0; i < numCrosssections(); i++) {
        progress.setValue(i);
        if (progress.wasCanceled()) break;

        Crosssection* crs = getCrosssection(i);
        if (!crs) {
            QGOException ex(QGOException::ERROR, "Interne fout", "Data beschadigd. Sluit programma af en probeer opnieuw.");
            ErrorHandler::exception(ex);
            return;
        }
        QString filename = dir.filePath(crs->getName() + ".prfl");
        exportSingleProfile(filename, crs);
    }

    progress.setValue(numCrosssections());

    QMessageBox::information(nullptr, "Klaar", "Alle *.prfl bestanden zijn opgeslagen.");
}

Crosssection *Project::getCurrentCrosssection() const
{
    if ((m_current_crosssection_index >= 0) && (m_crosssections.count() > m_current_crosssection_index)) return m_crosssections.at(m_current_crosssection_index);
    else return nullptr;
}

int Project::getCurrent_crosssection_index() const
{
    return m_current_crosssection_index;
}

void Project::setCurrent_crosssection_index(int current_crosssection_index)
{
    m_current_crosssection_index = current_crosssection_index;
}

void Project::runProcessorsOnAllCrosssections()
{
    QSettings settings;
    //pre-check if processors have already been run
    bool ask = false;
    bool proceed = true;
    foreach (Crosssection *crs, m_crosssections) {
        ask |= crs->getNumExecutedProcessors() > 0;
    }

    if(ask && show_processor_warning){
        QCheckBox *cb = new QCheckBox("Toon deze waarschuwing niet meer");
        QMessageBox msgbox;

        msgbox.setText("(De profielen bevatten reeds uitgevoerde processoren.\nDit kan onverwachte resultaten geven. Over het algemeen is het beter om het profiel eerst te resetten.\nWilt u toch doorgaan?");
        msgbox.setIcon(QMessageBox::Icon::Warning);
        msgbox.addButton(QMessageBox::Ok);
        msgbox.addButton(QMessageBox::Cancel);
        msgbox.setDefaultButton(QMessageBox::Cancel);
        msgbox.setCheckBox(cb);

        proceed = msgbox.exec() == QMessageBox::Ok;
        show_processor_warning = !cb->isChecked();
        delete(cb);
    }

    if(proceed){
        QProgressDialog progress("Regels toepassen...", "Onderbreken", 0, m_crosssections.count(), nullptr);
        progress.setWindowModality(Qt::WindowModal);
        progress.setWindowTitle("QGolfoploop - Regels toepassen");
        int iprogress = 0;

        foreach (Crosssection *crs, m_crosssections) {
            progress.setValue(iprogress++);
            if(progress.wasCanceled())
                break;
            crs->applyProcessors(&m_processormapping);
        }
        progress.setValue(m_crosssections.count());
    }
}

void Project::runProcessorsOnCurrentCrosssection()
{    
    QSettings settings;
    Crosssection* crs = getCurrentCrosssection();

    if (crs){
        if(crs->getNumExecutedProcessors()>0 && show_processor_warning){
            QCheckBox *cb = new QCheckBox("Toon deze waarschuwing niet meer");
            QMessageBox msgbox;

            msgbox.setText("(De profielen bevatten reeds uitgevoerde processoren.\nDit kan onverwachte resultaten geven. Over het algemeen is het beter om het profiel eerst te resetten.\nWilt u toch doorgaan?");
            msgbox.setIcon(QMessageBox::Icon::Warning);
            msgbox.addButton(QMessageBox::Ok);
            msgbox.addButton(QMessageBox::Cancel);
            msgbox.setDefaultButton(QMessageBox::Cancel);
            msgbox.setCheckBox(cb);

            bool proceed = msgbox.exec() == QMessageBox::Ok;

            show_processor_warning = !cb->isChecked();

            if(proceed){
                crs->applyProcessors(&m_processormapping);
            }
            delete(cb);
        }else{
            crs->applyProcessors(&m_processormapping);
        }
    }
}

QString Project::getRoughnessName(const double r)
{
    int key = (int)(r * 1000);
    if (m_roughnessmap.contains(key)) return m_roughnessmap.value(key);
    return QString("<onbekend materiaal, r=%1>").arg(r, 4, 'f', 2);
}

Crosssection *Project::getCrosssection(const int index)
{
    if(index >= 0 && index < m_crosssections.count())
        return m_crosssections[index];
    return NULL;
}

Crosssection *Project::getCrosssection(const QString label)
{
    foreach (Crosssection* cs, m_crosssections) {
        if (cs->getName() == label) return cs;
    }
    return nullptr;
}

bool Project::containsDirtyCrosssections()
{
    foreach(Crosssection *crs, m_crosssections){
        if(crs->isDirty()){
            return true;
        }
    }
    return false;
}
