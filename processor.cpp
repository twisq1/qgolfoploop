/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QSettings>
#include "crosssection.h"
#include "simpleQtLogger.h"
#include "errorhandler.h"
#include "processor.h"

Processor::Processor(Crosssection* crs)
    : m_crs(crs)
{
    m_hasrun = false;
    m_enabled = Tristate::GLOBAL;
    m_ok = true;
    m_status = "Ok";        
}

Processor::~Processor()
{
}

void Processor::run()
{
    if (isEnabled()) {
        doProcess();
        m_hasrun = true;
    }
}

void Processor::reset()
{
    m_hasrun = false;
    m_ok = true;
    m_status = "";
}

bool Processor::isEnabled() const
{
    switch (m_enabled) {
    case Tristate::ON:
        return true;
    case Tristate::OFF:
        return false;
    case Tristate::GLOBAL:
        return getGlobalEnable();
    }
    L_ERROR("Illegal value of m_enable.");
    return false;
}

Processor::Tristate Processor::getLocalEnable() const
{
    return m_enabled;
}

void Processor::setLocalEnable(const Tristate enabled)
{
    if (enabled != m_enabled) {
        m_enabled = enabled;
        crs()->setDirty(true);
    }
}

bool Processor::getHasrun() const
{
    return m_hasrun;
}

bool Processor::getOk() const
{
    return m_ok;
}

void Processor::setOk(bool ok)
{
    m_ok = ok;
}

QString Processor::getStatus() const
{
    return m_status;
}

Crosssection *Processor::getCrs() const
{
    return m_crs;
}

bool Processor::getGlobalEnable() const
{
    QSettings settings;
    QString key = KEY_PREFIX + getName();
    return settings.value(key).toBool();
}

void Processor::setGlobalEnable(bool enable)
{
    QSettings settings;
    QString key = KEY_PREFIX + getName();
    settings.setValue(key, enable);
}

void Processor::setStatus(const QString &status)
{
    m_status = status;
}

void Processor::error(const QString &status)
{
    setStatus(status);
    setOk(false);
}

bool Processor::read(const QJsonObject &json)
{
    if (json.contains("status") && json.contains("ok") && json.contains("hasrun") && json.contains("enabled")) {
        m_status = json["status"].toString();
        m_ok = json["ok"].toBool();
        m_hasrun = json["hasrun"].toBool();
        m_enabled = static_cast<Tristate>(json["enabled"].toInt());
        return true;
    } else {
        ErrorHandler::info("Bestand bevat ongeldige inhoud betreffende een processor.");
        return false;
    }
}

bool Processor::write(QJsonObject &json) const
{
    json["name"] = getName();
    json["status"] = m_status;
    json["ok"] = m_ok;
    json["hasrun"] = m_hasrun;
    json["enabled"] = static_cast<int>(m_enabled);
    return true;
}

Processor::SlopeLine Processor::slope(const QList<Point *>& points){
    double n = points.count();

    QVector<double> x;
    QVector<double> y;

    foreach(Point *p, points){
        x.append(p->l(points.at(0)));
        y.append(p->z());
    }

    // de regressie lijn gaat door x_avg, y_avg
    double avgX = std::accumulate(x.begin(), x.end(), .0) / x.size();
    double avgY = std::accumulate(y.begin(), y.end(), .0) / y.size();

    double numerator = 0.0;
    double denominator = 0.0;

    for(int i=0; i<n; ++i){
        numerator += (x[i] - avgX) * (y[i] - avgY);
        denominator += (x[i] - avgX) * (x[i] - avgX);
    }

    if ((denominator < 1e-9) && (denominator > -1e-9)) {
        return Processor::SlopeLine(0., 0., 1e9);
    }

    return Processor::SlopeLine(avgX, avgY, numerator / denominator);
}
