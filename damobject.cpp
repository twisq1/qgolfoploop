/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "damobject.h"

DamObject::DamObject()
{
    m_valid = false;
    set(-1,"",0);
}

void DamObject::set(int type, const QString &name, double height)
{
    m_type = type;
    m_name = name;
    m_height = height;
}

int DamObject::type() const
{
    return m_type;
}

QString DamObject::name() const
{
    return m_name;
}

double DamObject::height() const
{
    return m_height;
}

bool DamObject::isValid() const
{
    return m_valid;
}

bool DamObject::read(const QJsonObject &json)
{
    if (json.contains("name") && json.contains("valid") && json.contains("type") && json.contains("height")) {
        m_name = json["name"].toString();
        m_valid = json["valid"].toBool();
        m_type = json["type"].toInt();
        m_height = json["height"].toDouble();
        return true;
    }
    return false;
}

bool DamObject::write(QJsonObject &json) const
{
    json["name"] = m_name;
    json["valid"] = m_valid;
    json["type"] = m_type;
    json["height"] = m_height;
    return true;
}
