/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H

#include "qgoexception.h"

/*
 * When an error occurs, you can call this function to show an error message to the user.
 * The message will be showed in a pop-up window.
 * You should give the properties of the error message in the QGOEXception object.
 * This object hold the message and a description of the error as well as the type of the error message.
 * If the type is FATAL, the application will terminate immediate after the user has clicked OK on the message box.
 * In all other cases, the application continues then.
 */

class ErrorHandler
{
public:
    static void exception(QGOException& ex);

    static void info(const QString msg);
    static void warn(const QString msg);
    static void error(const QString msg);
    static void fatal(const QString msg);
};

#endif // ERRORHANDLER_H
