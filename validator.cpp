/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QSettings>
#include "simpleQtLogger.h"
#include "crosssection.h"
#include "validator.h"
#include "constants.h"

Validator::Validator(Crosssection* crs)
    : Corrector(crs)
{

}

void Validator::setGlobalCheckStart(const bool check)
{
    QSettings settings;
    settings.setValue(GLOBAL_CHECK_START, check);
}

void Validator::setGlobalCheckEnd(const bool check)
{
    QSettings settings;
    settings.setValue(GLOBAL_CHECK_END, check);
}

void Validator::setGlobalCheckBerm(const bool check)
{
    QSettings settings;
    settings.setValue(GLOBAL_CHECK_BERM, check);
}

void Validator::setGlobalMaxBerm(const int x)
{
    QSettings settings;
    settings.setValue(GLOBAL_MAX_BERM, x);
}

bool Validator::getGlobalCheckStart()
{
    QSettings settings;
    return settings.value(GLOBAL_CHECK_START, DEFAULT_CHECK_START).toBool();
}

bool Validator::getGlobalCheckEnd()
{
    QSettings settings;
    return settings.value(GLOBAL_CHECK_END, DEFAULT_CHECK_END).toBool();
}

bool Validator::getGlobalCheckBerm()
{
    QSettings settings;
    return settings.value(GLOBAL_CHECK_BERM, DEFAULT_CHECK_BERM).toBool();
}

int Validator::getGlobalMaxBerm()
{
    QSettings settings;
    return settings.value(GLOBAL_MAX_BERM, DEFAULT_MAX_BERM).toInt();
}

QString Validator::getName() const
{
    return PROCESSOR_VALIDATOR;
}

void Validator::doProcess()
{
    bool no_voorland = false;
    bool negative_slope = false;
    bool too_shallow = false;
    bool too_steep = false;

    // Check voorland.
    Point* ref = crs()->getReferencePoint();
    long voorland_lmm = crs()->voorland();
    if (voorland_lmm == 0) {
        no_voorland = true;
    }

    // Get parameters.
    bool checkNegative = getGlobalCheckNegativeKey();
    bool checkTooFlat = getGlobalCheckTooFlat();
    bool checkTooSteep = getGlobalCheckTooSteep();
    double max_slope_bank  = (1.0 / getGlobalMaxBank());
    double min_slope_slope = (1.0 / getGlobalMinDam());
    double max_slope_slope = (1.0 / getGlobalMaxDam());

    bool first_bank_checked = false;
    bool first_bank = false;
    bool last_bank = false;
    int  nrof_banks = 0;
    QList<Point*> banks;
    Point* p1 = nullptr;
    // Walk through profile, segment by segment. Each time the segment between p1 and p2 is processed.
    for (Point* p2 : crs()->getSurfacePoints()) {
        if (p1) {

            // check only dam part.
            if (p2->lmm(ref) > voorland_lmm) {

                // Calculate slope of current segment and do the checks.
                double slope = crs()->getZSlope(p1, p2);

                // Check banks
                bool this_is_a_bank = (slope < max_slope_bank);
                if (this_is_a_bank) banks.append(p2);
                if (!first_bank_checked) {
                    first_bank = this_is_a_bank;
                    first_bank_checked = true;
                    if (first_bank && getGlobalCheckStart()) {
                        p2->setInvalid(true);
                    }
                }
                if (this_is_a_bank && !last_bank) nrof_banks++;
                last_bank = this_is_a_bank;

                if (checkNegative && (slope < 0.0)) {
                    negative_slope = true;
                    p2->setInvalid(true);

                } else if (checkTooFlat && (slope > max_slope_bank) && (slope < min_slope_slope)) {
                    too_shallow = true;
                    p2->setInvalid(true);

                } else if (checkTooSteep && (slope > max_slope_slope)) {
                    too_steep = true;
                    p2->setInvalid(true);
                } else {
                    p2->setInvalid(false);
                }
            }
        }
        p1 = p2;
    }

    QString status;
    if (no_voorland) status = "Profiel heeft geen voorland.";
    if (negative_slope) {
        if (!status.isEmpty()) status += " / ";
        status += "Profiel bevat een negatieve helling.";
    }
    if (too_shallow) {
        if (!status.isEmpty()) status += " / ";
        status += "Profiel bevat een segment dat te flauw is voor een taluddeel, maar te steil voor een bermdeel.";
    }
    if (too_steep) {
        if (!status.isEmpty()) status += " / ";
        status += "Profiel bevat een segment dat te steil is voor een taluddeel.";
    }
    bool wrong_start = false;
    if (getGlobalCheckStart() && first_bank) {
        if (!status.isEmpty()) status += " / ";
        status += "Profiel begint met een bermdeel.";
        wrong_start = true;
    }
    bool wrong_end = false;
    if (getGlobalCheckEnd() && last_bank) {
        p1->setInvalid(true);
        if (!status.isEmpty()) status += " / ";
        status += "Profiel eindigt met een bermdeel.";
        wrong_end = true;
    }
    bool too_many_banks = false;
    if (getGlobalCheckBerm() && nrof_banks > getGlobalMaxBerm()) {
        if (!status.isEmpty()) status += " / ";
        status += "Profiel bevat teveel bermdelen.";
        too_many_banks = true;
        for (Point* p : banks) {
            p->setInvalid(true);
        }
    }
    setOk(!(no_voorland || negative_slope || too_shallow || too_steep || wrong_start || wrong_end || too_many_banks));
    setStatus(status);
}
