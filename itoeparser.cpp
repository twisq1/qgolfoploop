/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "itoeparser.h"
#include "constants.h"
#include "simpleQtLogger.h"
#include "crosssection.h"
#include <QList>
#include <QSettings>

IToeParser::IToeParser(Crosssection* crs) : Processor(crs)
{
}

QString IToeParser::getName() const
{
    return PROCESSOR_ITP;
}

void IToeParser::setGlobalIgnore(const int ignore)
{
    QSettings settings;
    settings.setValue(GLOBAL_ITP_IGNORE_KEY, ignore);
}

void IToeParser::setGlobalRegression(const int regression)
{
    QSettings settings;
    settings.setValue(GLOBAL_ITP_REGRESSION_KEY, regression);
}

void IToeParser::setGlobalHeight(const double height)
{
    QSettings settings;
    settings.setValue(GLOBAL_ITP_HEIGHT_KEY, height);
}

void IToeParser::setGlobalTruncate(const bool truncate)
{
    QSettings settings;
    settings.setValue(GLOBAL_ITP_TRUNCATE_KEY, truncate);
}

void IToeParser::setGlobalFixed(const bool fixed)
{
    QSettings settings;
    settings.setValue(GLOBAL_ITP_FIXED_KEY, fixed);
}

int IToeParser::getGlobalIgnore()
{
    QSettings settings;
    return settings.value(GLOBAL_ITP_IGNORE_KEY, DEFAULT_ITP_IGNORE).toInt();
}

int IToeParser::getGlobalRegression()
{
    QSettings settings;
    return settings.value(GLOBAL_ITP_REGRESSION_KEY, DEFAULT_ITP_REGRESSION).toInt();
}

double IToeParser::getGlobalHeight()
{
    QSettings settings;
    return settings.value(GLOBAL_ITP_HEIGHT_KEY, DEFAULT_ITP_HEIGHT).toDouble();
}

bool IToeParser::getGlobalTruncate()
{
    QSettings settings;
    return settings.value(GLOBAL_ITP_TRUNCATE_KEY, DEFAULT_ITP_TRUNCATE).toBool();
}

bool IToeParser::getGlobalFixed()
{
    QSettings settings;
    return settings.value(GLOBAL_ITP_FIXED_KEY, DEFAULT_ITP_FIXED).toBool();
}

void IToeParser::doProcess()
{
    // First look for relevant points.
    Point* base = crs()->getReferencePoint();

    Point *realToe = crs()->getCharacteristicPoint(Point::CPID::TEEN_DIJK_BUITENWAARTS);
    if(!realToe){
        setStatus("Kan geen buitenteenpunt vinden.");
        setOk(false);
        return;
    }

    // Determine height of imaginairy toe.
    double ztoe = realToe->z();
    if (getGlobalFixed()) {
        ztoe = getGlobalHeight();
    }

    // Get points for regression line.
    int num_skip_points = getGlobalIgnore();
    int num_regression_points = getGlobalRegression();

    QList<Point*> regression_points = crs()->getPointsRightOf(realToe, num_skip_points, num_regression_points);
    if(regression_points.count() < 2){
        setStatus("Er zijn minder dan 2 regressielijn punten gevonden, de regressie kan niet plaatsvinden.");
        setOk(false);
        return;
    }

    double maxright = regression_points.first()->l(base);

    //stap 3 - bepaal de helling
    //we geven de punten voor de regressielijn door aan een functie die op basis van
    //deze punten een slopeline teruggeeft. De slopelijn staat in een lokaal assenstelsel
    //waarbij regression_points[0] het nulpunt voor de lengte is.
    SlopeLine slopeline = slope(regression_points);
    if(slopeline.slope<0.00001){
        setStatus("De regressielijn heeft de verkeerde richting.");
        setOk(false);
        return;
    } else {
        // Niet vergeten om de lengte van regression_points[0] bij het uiteindelijke punt op te tellen.
        double l = regression_points.at(0)->l(base) + slopeline.x + (ztoe - slopeline.y) / slopeline.slope;

        // Check of IKP niet teveel naar links of naar rechts komt te liggen.
        if ((l < 0.0) || (l > maxright))
        {
            setStatus("Geen snijpunt van regressielijn en profiel.");
            setOk(false);
        } else {

            // Add imaginairy point.
            crs()->setImaginaryPoint(l, ztoe, Point::CPID::IMAGINAIR_TEENPUNT, Point::CPID::TEEN_DIJK_BUITENWAARTS);

            // Remove all the points between regression line and the ITP.
            for (Point* p : crs()->getSurfacePoints()) {
                if ((p->l(base) >= (l + 0.001)) && (p->l(base) <= (maxright - 0.001))) p->setTruncated();
            }

            // Remove original kruinpunt.
            realToe->setId(Point::NONE);
            realToe->setTruncated();

            // Truncate all points left of ITP when needed.
            if (getGlobalTruncate()) {
                for (Point* p : crs()->getSurfacePoints()) {
                    if ((p->l(base) < (l - 0.001))) p->setTruncated();
                }
            }
        }
    }
}
