# Handleiding qGolfoploop

* Versie 1.0.0
* Datum 27-06-2018

Ontwikkeld door:
![](https://bitbucket.org/twisq1/qgolfoploop/raw/9c05c9de04e4600d9ae160378c1ad3cb37572b73/handleiding/img/twisq.png)
In samenwerking met:

![](https://bytebucket.org/twisq1/qgolfoploop/raw/70bb639a94900bd2ef28e9dc012d4a562c8f6069/handleiding/img/breinbaas.png)
In opdracht van:

![](https://bitbucket.org/twisq1/qgolfoploop/raw/49494147abe26628024789c3809fe436a318a7a2/handleiding/img/opdrachtgevers.png)

## Inleiding

qGolfoploop is software om de invoer van de toetsing voor de golfoploop te vereenvoudigen. De software accepteert invoer vanuit XML en CSV bestanden die de dwarsprofielen en de randvoorwaarden beschrijven waarna de software de gegevens grafisch weergeeft en automatisch kan wijzigen om te voldoen aan de gestelde regelgeving volgens het WBI. De export van qGolfloop is een prfl bestand dat benodigd is voor de uiteindelijke toetsing in de WBI software.

## Invoerbestanden

De invoer voor qGolfoploop bestaat uit een cvs bestand met dwarsprofiel informatie en diverse XML bestanden die randvoorwaarden en parameters voor de berekening bevatten.

### CSV bestand

Het invoer bestand is een csv file met per regel alle punten van het gehele dwarsprofiel.

Voorbeeld;

```
LOCATIONID;X1;Y1;Z1;.....;Xn;Yn;Zn;(Profiel)
12_2_00100;131597.040;548326.090;0.440;...;131621.190;548272.260;-0.050
12_2_00200;131677.370;548387.380;-0.100;...;131681.400;548379.330;-0.212
```

De eerste regel is de header. Deze dient in de invoerbestanden aanwezig te zijn. De opbouw is;

symbool | betekenis
------------- | -------------
LOCATIONID | locatie id bijvoorbeeld de dijkring en de metrering
X1;Y1;Z1 | het eerste punt van het profiel overeenkomend met het eerste punt aan de buitendijkse zijde (rivier, boezem, etc)
... | alle achtereenvolgende punten richting binnendijks maaiveld
Xn;Yn;Zn | het laatste punt overeenkomend met de binnendijkse zijde (polder, achterland, etc)

### XML bestanden

#### DAM gegevens

De aanwezigheid van een dam in een profiel wordt in het bestand damGolfoploopProfielen.xml meegegeven.

keyword | betekenis | voorbeeld
--- | --- | ---
id | verwijzing naar de profielnamen als gespecificeerd in het csv bestand | 12_2_00100
damType | het type dam volgens ...| 0
damTypeNaam | de naam van het dam type | caisson
damhoogte | de hoogte van de dam tov het referentievlak | 6.50

Hieronder is een voorbeeld van het XML bestand gegeven.


```
<?xml version="1.0" encoding="UTF-8"?>
<damGolfoploopProfielen xmlns="http://WBI/damGolfoploopProfielen/1">
	<damGolfoploopProfiel>
		<id>6-4_0002</id>
		<damType>1</damType>
		<damTypeNaam>Caisson</damTypeNaam>
		<damhoogte>333</damhoogte>
	</damGolfoploopProfiel>
</damGolfoploopProfielen>
```

#### Damwand gegevens

De aanwezigheid van een damwand in het profiel wordt in het bestand damwandGolfoploopProfielen.xml meegegeven met de volgende keywords.

keyword | betekenis | voorbeeld
--- | --- | ---
id | verwijzing naar de profielnamen als gespecificeerd in het csv bestand | 12_2_00100
damwandType | het type damwand volgens ... | 0
damwandTypeNaam | de naam van het type damwand | ...
damwandhoogte | de hoogte van de damwand tov het referentievlak | 6.50

Hieronder is een voorbeeld van het XML bestand gegeven.


```
<?xml version="1.0" encoding="UTF-8"?>
<damwandGolfoploopProfielen xmlns="http://WBI/damwandGolfoploopProfielen/1">
	<damwandGolfoploopProfiel>
		<id>6-4_0002</id>
		<damwandType>5</damwandType>
		<damwandTypeNaam>Test</damwandTypeNaam>
		<damwandhoogte>88</damwandhoogte>
	</damwandGolfoploopProfiel>
</damwandGolfoploopProfielen>
```

#### Ruwheidsgegevens

Het bestand ruwheidGolfoploopProfielen.xml beschrijft de overgangspunten van bekledingsvlakken.

keyword | betekenis | voorbeeld
--- | --- | ---
id | verwijzing naar de profielnamen als gespecificeerd in het csv bestand | 12_2_00100
xstart_bekledingsvlak | x-coordinaat van de start van het bekledingsvlak | 123456.789
ystart_bekledingsvlak | x-coordinaat van de start van het bekledingsvlak | 123456.789
zstart_bekledingsvlak | x-coordinaat van de start van het bekledingsvlak | 123456.789
xeind_bekledingsvlak | x-coordinaat van het eind van het bekledingsvlak | 123456.789
yeind_bekledingsvlak | y-coordinaat van het eind van het bekledingsvlak | 123456.789
zeind_bekledingsvlak | z-coordinaat van het eind van het bekledingsvlak | 123456.789

Hieronder is een voorbeeld van het XML bestand gegeven.


```
<?xml version="1.0" encoding="UTF-8"?>
<ruwheidGolfoploopProfielen xmlns="http://WBI/ruwheidGolfoploopProfielen/1">
	<ruwheidGolfoploopProfiel>
		<id>6-4_0001</id>
		<bekledingsvlakken>
			<bekledingsvlak>
				<xstart_bekledingsvlak>179489.130</xstart_bekledingsvlak>
				<ystart_bekledingsvlak>592683.593</ystart_bekledingsvlak>
				<zstart_bekledingsvlak>3.056</zstart_bekledingsvlak>
				<xeind_bekledingsvlak>179490.758</xeind_bekledingsvlak>
				<yeind_bekledingsvlak>592675.25</yeind_bekledingsvlak>
				<zeind_bekledingsvlak>4.323</zeind_bekledingsvlak>
				<bekledingsmateriaal>GVK-Grasvlak</bekledingsmateriaal>
				<ruwheid>0.9</ruwheid>
			</bekledingsvlak>			
		</bekledingsvlakken>
	</ruwheidGolfoploopProfiel>
</ruwheidGolfoploopProfielen>
```

#### Sturingsparameters

De sturingsparameters voor de profielen worden opgenomen in het bestand sturingsparametersGolfoploopProfielen.xml.

keyword | betekenis | voorbeeld
--- | --- | ---
id | verwijzing naar de profielnamen als gespecificeerd in het csv bestand | 12_2_00100
peuckerEpsilon | de toe te passen epsilon voor het Peucker algoritme | 0.1
SegmentenOverslaanInLineaireregressieLijn | het aantal segmenten dat niet meegenomen wordt voor het imaginaire kruinpunt | 4
SegmentenInLineaireregressieLijn | het aantal segmenten dat meegenomen wordt om het imaginaire kruinpunt te berekenen | 8

Hieronder is een voorbeeld van het XML bestand gegeven.


```
<?xml version="1.0" encoding="UTF-8"?>
<sturingsparametersGolfoploopProfielen xmlns="http://WBI/sturingsparametersGolfoploopProfielen/1">
	<sturingsparametersGolfoploopProfiel>
		<id>6-4_0002</id>
		<peuckerEpsilon>8.781</peuckerEpsilon>
		<segmentenOverslaanInLineaireregressieLijn>2</segmentenOverslaanInLineaireregressieLijn>
		<segmentenInLineaireregressieLijn>4</segmentenInLineaireregressieLijn>
	</sturingsparametersGolfoploopProfiel>
</sturingsparametersGolfoploopProfielen>
```

## User interface

De user interface van qDAMEdit bestaat uit een taakbalk en diverse informatieschermen. In de onderstaande figuur is een overzicht gegeven van de beschikbare knoppen en schermen.

![](https://bitbucket.org/twisq1/qgolfoploop/raw/49494147abe26628024789c3809fe436a318a7a2/handleiding/img/ui.png)

nummer | functie | toelichting
--- | --- | ---
1 | Taakbalk met diverse knoppen | In de grafische weergave wordt het profiel weergegeven. Met de schaalbalken onder en rechts van dt weergave kan op X of Y worden ingezoomd. Door de rechtermuisknop ingedrukt te houden kan het profiel verplaatst worden.
2 | Grafische weergave van de profielen en karakteristieke punten |  
3 | Lijst met weergave van de beschikbare profielen | De lijst met beschikbare profielen. Profielen die niet door de validatie komen worden in rode letters weergegeven.
4 | Opmerkingen | Een invoerveld om opmerkingen over het profiel op te slaan
5 | Checkbox lijst voor de toe te passen processoren | De opties zijn globaal aan, individueel aan en uit
6 | Tabel met punt informatie |
7 | Informatiebalk |

De grafische weergave is aan de onderzijde en rechter zijkant begrensd met een schaalbalk voor respectievelijk de x-, en y-as. De checkbox voor de processoren kent 3 toestanden; uit (grijze tekst), globaal aan (vinkje) en individueel aan (grijs vierkant). De kolommen L-start en Z-start in de tabel zijn aanpasbaar.

### Taakbalk

In de volgende afbeelding zijn de beschikbare taakbalkknoppen weergegeven. In de rest van de documentatie wordt naar deze knoppen verwezen.

![](https://bytebucket.org/twisq1/qgolfoploop/raw/bbbad183974c3cb647947c5bbd8dd8282c50509f/handleiding/img/menu.png)

knop | functie
--- | ---
1 | openen van de map met invoerbestanden map
2 | opslaan van de PRFL uitvoer
3 | importeer folder met profieldata
4 | exporteer profieldata naar folder
5 | reset profiel(en) naar uitgangspositie
6 | regels op huidig profiel toepassen
7 | toepassen van de processor regels op alle profielen
8 | verslepen van individuele punten
9 | punt aan profiel toevoegen
10 | geselecteerd punt verwijderen
11 | ga naar het eerste profiel
12 | ga naar het vorige profiel
13 | ga naar het volgende profiel
14 | ga naar het laatste profiel
15 | toon of verberg alle profielpunten
16 | toon of verberg raster
17 | reset de zoom naar het gehele profiel
18 | toon profiel informatie op het scherm
19 | herstel zoom naar gehele profiel
20 | ga naar de instellingen van qGolfoploop
21 | informatie over qGolfoploop

## Werkwijze qGolfoploop

### Invoer van gegevens
Bij de invoer van de gegevens dient de map met alle beschikbare bestanden opgegeven te worden. qGolfoploop bepaalt automatisch welke bestanden beschikbaar zijn en lees ze in. Foutieve bestanden worden gemeld.

### Weergave en aanpassen profielen
De profielen worden weergegeven waarbij weergave opties in te stellen zijn. Standaard worden alle punten getoond. Het voorland is een blauwe lijn en de karakteristieke punten (buitenteen, buiten- en binnenkruin) zijn rood omcirkeld. De Processoren kunnen per profiel individueel worden ingesteld indien dit gewenst is. De profielpunten kunnen met de muis versleept worden of de gebruiker kan de waarden voor L-start en Z-start in de tabel aanpassen.

### Uitvoeren processoren
qGolfoploop komt standaard met een 11-tal processoren. Dit zijn algoritmen die aanpassingen aan het profiel uitvoeren.

De processoren volgen standaard de instellingen die via Bestand | Instellingen | Processoren ingesteld staan. Indien een processor in deze instellingen aan of uit staat geldt deze waarde voor alle dwarsprofielen. Het is echter mogelijk om deze globale waarden per profiel aan te passen. Dit kan met de selectiebox in het processoren veld (5). Er zijn drie instellingen mogelijk;

- leeg, ongeacht de globale instelling voor dit profiel NIET deze processor uitvoeren
- vinkje, ongeacht de globale instelling voor dit profiel WEL deze processor uitvoeren
- gevuld vierkant, neem de globale instelling over

De volgende processoren zijn aanwezig;

#### Imaginair kruinpunt
Deze processor berekent het imaginaire kruinpunt.

![](https://bitbucket.org/twisq1/qgolfoploop/raw/45c46262c9102d712ab7242cb47478ea5c71cdd2/handleiding/img/ipparser.png)

De twee rode punten worden niet meegenomen in de berekening van de regressielijn. De vier punten links van de rode punten zijn de basis van de regressielijn. De menu instellingen zijn in het venster onder het profiel te zien.

#### Truncator
Deze processor knipt punten buiten de benodigde geometrie weg.

#### Ramer-Douglas-Peucker
Het Ramer-Douglas-Peucker algoritme zorgt ervoor dat overbodigde punten in lijnstukken verwijderd worden. Voor meer informatie zie de uitleg over de instellingen van QGolfoploop.

#### Bekleding overnemen uit XML
Gebruikt de waarden in het XML bestand om de bekleding over te nemen in het profiel.

#### Breuksteen verwijderen
Breuksteen punten worden automatisch verwijderd.

#### Zetting
De zetting wordt in het profiel meegenomen.

#### Corrigeer negatieve helling
Deze processor corrigeert hellingen die negatief zijn volgens de door de gebruiker opgegeven regels.

#### Corrigeer te flauw taluddeel
Deze processor corrigeert hellingen die te flauw zijn volgens de door de gebruiker opgegeven regels.

#### Corrigeer te stijl taluddeel
Deze processor corrigeert hellingen die te stijl zijn volgens de door de gebruiker opgegeven regels.

#### Validator
Deze processor controleert of aan alle regels voor de golfoploop toetsing wordt voldaan.

#### Aanpassen voor Riskeer
Pas de uitvoer aan aan het Riskeer protocol.

De processoren worden uitgevoerd door op knop 6 (individueel profiel) of 7 (alle profielen) in het menu te drukken.

![](https://bytebucket.org/twisq1/qgolfoploop/raw/711aad041de8be7590e002fa6502261000a31527/handleiding/img/error.png)
Indien de processoren meerdere keren uitgevoerd worden kunnen rare situaties in het profiel ontstaan. Gebruik in deze situatie de herstelknop (5) om weer in de oude situatie te komen.


## Uitvoer
De uitvoer kan gegenereerd worden door op knop 2 in het menu te drukken. De gebruiker kan een directory uitkiezen waar de uitvoerbestanden weggeschreven worden. Tevens bestaat de optie om de grafische weergave van een enkel of van alle profielen naar een map weg te schrijven.

## Instellingen
Via knop 20 in het menu kan het instellingen scherm worden weergegeven. De volgende instellingen zijn beschikbaar.

#### Corrigeren
Instellingen voor het automatisch corrigeren van profielen.

#### Validator
De validator controleert op 3 punten die individueel aan of uit te zetten zijn.

#### Kruin en zetting
Instellingen voor de zetting en het regressiemodel.

#### Breuksteen
Instellingen voor de standaard ruwheid van breuksteen.

#### RDP algoritme
Het RDP algoritme verwijdert punten uit polylijnen die niet echt bijdragen aan de richting van het gehele lijnstuk. Epsilon is een parameter die bepaalt hoeveel de afwijking van de punten mee moet wegen in het algoritme. Voor meer informatie zie [Wikipedia]("https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm")

De waarde voor epsilon kan ingesteld worden via het menu Bestand | Instellingen | RDP en geeft de waarde in meters aan wat een punt in hoogte t.o.v. een rechte lijn tussen de twee aangrenzende punten moet afwijken om onderdeel te zijn van de gehele lijn. Punten die onder de waarde van epsilon vallen worden uit de lijn gefilterd.

![](https://bitbucket.org/twisq1/qgolfoploop/raw/b67761d256e91f5654efca387b14b01b2f215887/handleiding/img/rdp.jpg)

#### Proccesoren
In dit menu kan aangegeven worden welke processoren standaard voor alle profielen aan (aangevinkt) of uit (geen vinkje) staan.

## Bronccode
De broncode van qGolfoploop is vrijgesteld volgens GNU GPLv3 en beschikbaar via: [bitbucket.org]("https://bitbucket.org/twisq1/qgolfoploop")

qGolfoploop is geschreven in de open source versie van Qt toolkit versie 5.10 en draait onder Windows 7 en hoger, MacOS en Linux. De applicatie is voor 64bits systemen geoptimaliseerd.

![](https://bitbucket.org/twisq1/qgolfoploop/raw/ac204dbce899a91344408b77546ff0bbfdd32542/handleiding/img/qt.jpg)
