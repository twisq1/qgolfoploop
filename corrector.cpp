/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include <QSettings>
#include "constants.h"
#include "simpleQtLogger.h"
#include "crosssection.h"
#include "corrector.h"

Corrector::Corrector(Crosssection *crs)
    : Processor(crs)
{
}

void Corrector::setGlobalCheckNegativeKey(const bool check)
{
    QSettings settings;
    settings.setValue(GLOBAL_CHECK_NEGATIVE_KEY, check);
}

void Corrector::setGlobalCheckTooFlat(const bool check)
{
    QSettings settings;
    settings.setValue(GLOBAL_CHECK_TOO_FLAT_KEY, check);
}

void Corrector::setGlobalCheckTooSteep(const bool check)
{
    QSettings settings;
    settings.setValue(GLOBAL_CHECK_TOO_STEEP_KEY, check);
}

void Corrector::setGlobalAlmostHorizontal(const double x)
{
    QSettings settings;
    settings.setValue(GLOBAL_ALMOST_HORIZONTAL, x);
}

void Corrector::setGlobalMaxBank(const double x)
{
    QSettings settings;
    settings.setValue(GLOBAL_MAX_BANK, x);
}

void Corrector::setGlobalMinDam(const double x)
{
    QSettings settings;
    settings.setValue(GLOBAL_MIN_DAM, x);
}

void Corrector::setGlobalMaxDam(const double x)
{
    QSettings settings;
    settings.setValue(GLOBAL_MAX_DAM, x);
}

bool Corrector::getGlobalCheckNegativeKey()
{
    QSettings settings;
    return settings.value(GLOBAL_CHECK_NEGATIVE_KEY, true).toBool();
}

bool Corrector::getGlobalCheckTooFlat()
{
    QSettings settings;
    return settings.value(GLOBAL_CHECK_TOO_FLAT_KEY, true).toBool();
}

bool Corrector::getGlobalCheckTooSteep()
{
    QSettings settings;
    return settings.value(GLOBAL_CHECK_TOO_STEEP_KEY, true).toBool();
}

double Corrector::getGlobalAlmostHorizontal()
{
    QSettings settings;
    return settings.value(GLOBAL_ALMOST_HORIZONTAL, DEFAULT_ALMOST_HORIZONTAL).toDouble();
}

double Corrector::getGlobalMaxBank()
{
    QSettings settings;
    return settings.value(GLOBAL_MAX_BANK, DEFAULT_MAX_BANK).toDouble();
}

double Corrector::getGlobalMinDam()
{
    QSettings settings;
    return settings.value(GLOBAL_MIN_DAM, DEFAULT_MIN_DAM).toDouble();
}

double Corrector::getGlobalMaxDam()
{
    QSettings settings;
    return settings.value(GLOBAL_MAX_DAM, DEFAULT_MAX_DAM).toDouble();
}

double Corrector::correctSlope(Point **pp1, Point **pp2, double &shift, double newslope)
{
    Point* p1 = *pp1;
    Point* p2 = *pp2;
    Point* ref = crs()->getReferencePoint();
    double dz0 = p2->z() - p1->z();
    double dl0 = p2->l(ref) - p1->l(ref);
    double dln = dz0 / newslope;
    double addshift = dln - dl0;
    shift += addshift;
    p2 = crs()->moveSurfacePoint(p2, p2->l(ref) + addshift, p2->z());
    *pp2 = p2;
    return addshift;
}

QPointF Corrector::getIntersection(QPointF q1, QPointF q2, Line secant, bool *hit)
{
    *hit = false;
    QPointF intersection;
    if (q1.x() != q2.x()) {

        double rc = (q2.y() - q1.y()) / (q2.x() - q1.x());
        Line line(rc, q1.y() - rc * q1.x());

        intersection = getIntersection(line, secant, hit);
        if (*hit) {
            if ((intersection.x() < q1.x()) || (intersection.x() > q2.x())) *hit = false;
        }
    } else {
        *hit = false;
    }
    return intersection;
}

QPointF Corrector::getIntersection(Line line1, Line line2, bool *hit)
{
    QPointF intersection;
    *hit = line1.first != line2.first;

    double x = (line2.second - line1.second) / (line1.first - line2.first);
    double y = line1.first * x + line1.second;
    intersection.setX(x);
    intersection.setY(y);
    return intersection;
}

// Return a line with slope=rc and which hits point p
Line Corrector::getLine(QPointF p, double rc) {
    double y0 = p.y() - rc * p.x();
    return Line(rc, y0);
}
