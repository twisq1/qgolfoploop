/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef ZETTINGPARSER_H
#define ZETTINGPARSER_H

#include "processor.h"

#define GLOBAL_ZETTING              "zet_zetting"

class ZettingParser : public Processor
{
    static constexpr double DEFAULT_ZETTING = 0.0;

public:
    ZettingParser(Crosssection* crs);

    virtual QString getName() const override;

    static void setGlobalZetting(const double zetting);
    static double getGlobalZetting();

private:
    virtual void doProcess() override;
};

#endif // ZETTINGPARSER_H
