/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef RUBBLEPROCESSOR_H
#define RUBBLEPROCESSOR_H

#include "processor.h"

#define GLOBAL_RUBBLE_IGNORE_KEY            "rub_ignore"
#define GLOBAL_RUBBLE_REGRESSION_KEY        "rub_regression"
#define GLOBAL_RUBBLE_ROUGHNESS_KEY         "rub_roughness"
#define GLOBAL_RUBBLE_KEEP_ROUGHNESS_KEY    "rub_keep_roughness"

class RubbleProcessor : public Processor
{
    static const int DEFAULT_RUBBLE_IGNORE = 2;
    static const int DEFAULT_RUBBLE_REGRESSION = 4;
    static constexpr double DEFAULT_RUBBLE_ROUGHNESS = 0.55;
    static const bool DEFAULT_RUBBLE_KEEP_ROUGHNESS = false;

public:
    RubbleProcessor(Crosssection* crs);

    virtual QString getName() const override;

    static void setGlobalRubbleIgnore(const int ignore);
    static void setGlobalRubbleRegression(const int regression);
    static void setGlobalRubbleRoughness(const double roughness);
    static void setGlobalRubbleKeepRoughness(const bool keep);
    static int getGlobalRubbleIgnore();
    static int getGlobalRubbleRegression();
    static double getGlobalRubbleRoughness();
    static bool getGlobalRubbleKeepRoughness();

private:
    virtual void doProcess() override;
};

#endif // RUBBLEPROCESSOR_H
