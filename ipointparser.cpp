/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "ipointparser.h"
#include "constants.h"

#include <QDebug>
#include <QList>
#include <QSettings>
#include "crosssection.h"

IPointParser::IPointParser(Crosssection* crs) : Processor(crs)
{

}

QString IPointParser::getName() const
{
    return PROCESSOR_IKP;
}

void IPointParser::setGlobalNumIgnore(const int ignore)
{
    QSettings settings;
    settings.setValue(GLOBAL_NUM_IGNORE_KEY, ignore);
}

void IPointParser::setGlobalNumRegression(const int regression)
{
    QSettings settings;
    settings.setValue(GLOBAL_NUM_REGRESSION_KEY, regression);
}

void IPointParser::setGobalIkbTruncate(const bool truncate)
{
    QSettings settings;
    settings.setValue(GLOBAL_IKB_TRUNCATE, truncate);
}

int IPointParser::getGlobalNumIgnore()
{
    QSettings settings;
    return settings.value(GLOBAL_NUM_IGNORE_KEY, DEFAULT_NUM_IGNORE).toInt();
}

int IPointParser::getGlobalNumRegression()
{
    QSettings settings;
    return settings.value(GLOBAL_NUM_REGRESSION_KEY, DEFAULT_NUM_REGRESSION).toInt();
}

bool IPointParser::getGlobalIkbTruncate()
{
    QSettings settings;
    return settings.value(GLOBAL_IKB_TRUNCATE, DEFAULT_IKB_TRUNCATE).toBool();
}

void IPointParser::doProcess()
{
    // voorwerk, zoek naar karakteristieke punten
    Point* base = crs()->getReferencePoint();

    Point *pbuk = crs()->getCharacteristicPoint(Point::CPID::KRUIN_BUITENTALUD);
    if(!pbuk){
        setStatus("Kan geen buitenkruinpunt vinden.");
        setOk(false);
        return;
    }

    Point *pbik = crs()->getCharacteristicPoint(Point::CPID::KRUIN_BINNENTALUD);
    if(!pbik){
        setStatus("Kan geen binnenkruinpunt vinden.");
        setOk(false);
        return;
    }
    // Het IKP mag niet rechts van het Kruin Binnentalud komen.
    double maxright = pbik->l(base);

    //stap 1 - bepaal hoogste punt op de kruin.
    double zmax = crs()->getZMax(pbuk, pbik);

    //stap 2 - haal de punten op voor de regressie lijn
    int num_skip_points = crs()->hasIKPnumskip() ? crs()->getIKPnumskip() : getGlobalNumIgnore();
    int num_regression_points = crs()->hasIKPnumregression() ? crs()->getIKPnumregression() : getGlobalNumRegression();

    QList<Point*> regression_points = crs()->getPointsLeftOf(pbuk, num_skip_points, num_regression_points);
    if(regression_points.count() < 2){
        setStatus("Er zijn minder dan 2 regressielijn punten gevonden, de regressie kan niet plaatsvinden.");
        setOk(false);
        return;
    }

    double maxleft = regression_points.last()->l(base);

    //stap 3 - bepaal de helling
    //we geven de punten voor de regressielijn door aan een functie die op basis van
    //deze punten een slopeline teruggeeft. De slopelijn staat in een lokaal assenstelsel
    //waarbij regression_points[0] het nulpunt voor de lengte is.
    SlopeLine slopeline = slope(regression_points);
    if(slopeline.slope>1e4){
        setStatus("De regressielijn is horizontaal.");
        setOk(false);
        return;
    } else {
        // Niet vergeten om de lengte van regression_points[0] bij het uiteindelijke punt op te tellen.
        double l = regression_points.at(0)->l(base) + slopeline.x + (zmax - slopeline.y) / slopeline.slope;

        // Check of IKP niet teveel naar links of naar rechts komt te liggen.
        if (l < maxleft) l = maxleft + 0.0001;
        if (l > maxright) l = maxright - 0.001;

        // Add imaginairy point.
        crs()->setImaginaryPoint(l, zmax, Point::CPID::IMAGINAIR_KRUINPUNT, Point::CPID::KRUIN_BUITENTALUD);

        // Remove all the points between regression line and the IKP.
        for (Point* p : crs()->getSurfacePoints()) {
            if ((p->l(base) >= (maxleft + 0.001)) && (p->l(base) <= (l - 0.001))) p->setTruncated();
        }

        // Remove original kruinpunt.
        pbuk->setId(Point::NONE);
        pbuk->setTruncated();

        // Truncate all points right of IKP when needed.
        if (getGlobalIkbTruncate()) {
            for (Point* p : crs()->getSurfacePoints()) {
                if ((p->l(base) >= (l + 0.001))) p->setTruncated();
            }
        }
    }
}
