/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "zettingparser.h"

#include <QSettings>
#include "crosssection.h"
#include "constants.h"


ZettingParser::ZettingParser(Crosssection* crs)
    : Processor(crs)
{
}

QString ZettingParser::getName() const
{
    return PROCESSOR_ZETTING;
}

void ZettingParser::setGlobalZetting(const double zetting)
{
    QSettings settings;
    settings.setValue(GLOBAL_ZETTING, zetting);
}

double ZettingParser::getGlobalZetting()
{
    QSettings settings;
    return settings.value(GLOBAL_ZETTING, DEFAULT_ZETTING).toDouble();
}

void ZettingParser::doProcess()
{
    if (!getHasrun()) {
        // Calculate new top.
        double zetting = crs()->hasZetting() ? crs()->getZetting() : getGlobalZetting();
        double top = crs()->zmax() - zetting;

        // Lower all points which are too high.
        for (Point* p : crs()->getSurfacePoints()) {
            if (p->z() > top) p->setZ(top);
        }
    } else {
        setStatus("Kan Zetting Processor niet meerdere keren achter elkaar uitvoeren.");
        setOk(false);
    }
}
