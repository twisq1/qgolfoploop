/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef XMLITEM_H
#define XMLITEM_H

#include <QDomNode>
#include <QList>

class XMLItem
{
private:
    QDomNode m_node;

public:
    XMLItem(QDomNode node);

    QList<XMLItem> getChildren();


    QString getName() const;

    bool getValueAsString(QString name, QString *value);
    bool getValueAsInt(QString name, int *value);
    bool getValueAsDouble(QString name, double *value);
    bool getItemById(const QString id, XMLItem *item);

};

#endif // XMLITEM_H
