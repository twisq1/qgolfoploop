/**********************************************************************
* This file is part of qGolfoploop.
*
* qGolfoploop is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qGolfoploop is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qGolfoploop.  If not, see <http://www.gnu.org/licenses/>.
*
* qGolfoploop is written in Qt version 5.10.
*
* Copyright 2018 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QListWidgetItem>
#include <QTableWidgetItem>
#include <QStandardItemModel>
#include "project.h"
#include "doublespinitemdelegate.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    // Override close event to check if changes have been saved.
    void closeEvent (QCloseEvent *event);

private slots:
    void gotoNextCrosssection();

    void on_actionInfo_triggered();
    void on_actionToStart_triggered();
    void on_actionBackward_triggered();
    void on_actionForward_triggered();
    void on_actionToEnd_triggered();
    void on_processorsListWidget_itemChanged(QListWidgetItem *item);
    void on_actionGo_triggered();
    void on_actionOne_triggered();
    void on_actionOpen_triggered();
    void on_actionShowPoints_toggled(bool arg1);
    void on_actionEdit_toggled(bool arg1);
    void on_actionAdd_toggled(bool arg1);
    void on_actionDelete_toggled(bool arg1);
    void on_actionReset_zoom_triggered();
    void on_actionShowLines_toggled(bool arg1);
    void on_actionShowGrid_toggled(bool arg1);

    // to connect to the projectview
    void on_mouseLocationChanged(QPointF p);

    void on_actionSettings_triggered();

    void on_profileListWidget_currentRowChanged(int currentRow);
    void on_actionSave_triggered();
    void exportSingleImage();
    void exportAllImages();
    void exportShapefile();
    void updateRules();
    void on_teRemark_textChanged();

    void updateTable();

    void on_processorsListWidget_itemDoubleClicked(QListWidgetItem *item);
    void on_actionReset_profiel_triggered();
    void on_actionImport_triggered();
    void on_actionExport_triggered();

    void on_actionactionShowCrosssectionInfo_triggered(bool checked);

    void on_projectviewScaleChanged(const QPointF new_scale);

private:
    Ui::MainWindow *ui;
    QLabel *m_lbl_mouse_location;
    QLabel *m_lbl_cross_section;
    DoubleSpinItemDelegate m_dspbox_delegate;

    Project m_project;
    bool updating_rules;

    int m_imk_num_ignore;
    int m_imk_num_regression;

    void showActiveCrossSection();
    void initProfileList();

    void loadProcessorOrderSettings();
    void writeProcessorOrderSettings();

    void loadInitialFolder();
    void updateProfileList();
    void exportAllProfiles();
    void exportSingleProfile();
    void exportXmlFile();
    void updateProcessorState(Qt::CheckState state, Processor* p);
};

#endif // MAINWINDOW_H
